<?php

require $_SERVER['DOCUMENT_ROOT'] . '/inc/defines.php';

error_reporting(E_WARNING);

spl_autoload_register(function(){
    $path = explode('\\',func_get_arg(0));
    $aPath = [];
    $ct = count($path);
    foreach ($path as $k => $p) {
        //strtolower - имя папок
        $aPath[] = $k + 1 != $ct ? strtolower($p) : $p;
    }
    $className = LIB_PATH . implode('/',$aPath).'.php';
    if (file_exists($className)){
        require_once $className;
    }
});

require LIB_PATH.'definesErrorCode.php';
require LIB_PATH.'classes/Session.php';
Session::newInstance()->session_start();

require LIB_PATH.'db.inc.php';
require LIB_PATH.'functions.inc.php';
require LIB_PATH.'monthName.php';

//classes
require LIB_PATH.'classes/db/DAO.php';
require LIB_PATH.'classes/db/DBCommandClass.php';
require LIB_PATH.'classes/db/DBConnectionClass.php';
require LIB_PATH.'classes/db/DBRecordsetClass.php';
require LIB_PATH.'classes/BaseModel.php';
require LIB_PATH.'classes/View.php';

//models
require LIB_PATH.'models/F1.php';
require LIB_PATH.'models/F1_year.php';
require LIB_PATH.'models/Params.php';
require LIB_PATH.'models/Rewrite.php';
require LIB_PATH.'models/AbonentActions.php';
require LIB_PATH.'models/Payment.php';
require LIB_PATH.'models/Tariff.php';
require LIB_PATH.'models/Street.php';
require LIB_PATH.'models/Controller.php';
require LIB_PATH.'models/User.php';
require LIB_PATH.'models/UserPayment.php';
require LIB_PATH.'models/ClientUserPayment.php';
require LIB_PATH.'models/UserRules.php';


//helpers
require LIB_PATH.'helpers/hDatabaseInfo.php';
require LIB_PATH.'helpers/hErrors.php';
require LIB_PATH.'helpers/hUrls.php';
require LIB_PATH.'helpers/hMessages.php';
require LIB_PATH.'helpers/hUsers.php';

//forms
require LIB_PATH.'frm/Form.php';

Params::init();
Rewrite::init();

//if (don_get_preference('lastId') == ''){
//    don_set_preference('lastId', 1);
//}

if ($_SERVER['REQUEST_URI'] === '/index.php') {
    osc_redirect_to('.');
}