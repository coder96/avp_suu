<?php

ob_start();

include 'app_load.php';

if (run_controller(Params::getParam('controller')) !== false){
    exit();
} else {
    Params::setParam('action','404');
    run_controller('main');
}
exit();