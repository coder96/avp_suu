<?php
$text_color = '#1f8d8f';
$border_color = '#67d4d6';
$bg_color = '#e8e8e8';
    
function osc_base_path() {
    return $_SERVER['DOCUMENT_ROOT'].'/';
}

/**
 * @param string $name
 * @param string $value
 * @return View
 */
function __export($name = '',$value = ''){
    if ($name == ''){
        return View::newInstance();
    }
    View::newInstance()->_exportVariableToView($name,$value);
}

/**
 * @param $name
 * @return string|View
 */
function __get($name = ''){
    if ($name == ''){
        return View::newInstance();
    }
    return View::newInstance()->_get($name);
}

function __getSess($name){
    return Session::newInstance()->_get($name);
}

function __setSess($name,$value){
    return Session::newInstance()->_set($name,$value);
}

function __setForm($name,$value){
    return Session::newInstance()->_setForm($name,$value);
}

function __getParam($name){
    return Params::getParam($name);
}

function __existParam($name){
    return Params::existParam($name);
}

function __exist($name){
    return View::newInstance()->_exists($name);
}

function __setParam($name,$value){
    Params::setParam($name,$value);
}

function __getReferer($fromSession = TRUE){
    if ($fromSession){
        return Session::newInstance()->_getReferer();
    } else {
        return ($_SERVER['HTTP_REFERER']);
    }
}

function __dropReferer(){
    Session::newInstance()->_dropReferer();
}

function __setReferer($url = '',$request_uri = false){
    if ($url == ''){
        $url = don_current_url($request_uri);
    }
    Session::newInstance()->_setReferer($url);
}

function dbContainUser($name, $password) {
    $row = User::newInstance()->dbContainUser($name, $password);
    $user = \Skeletion\UserSkeletion::build($row);
    if (!empty($row)) {
        $_SESSION['user_id'] = $row['pk_i_id'];
        $_SESSION['fio'] = $row['fio'];
        $_SESSION['name'] = $name;
        $_SESSION['user_tariff'] = $row['i_tariffPrice'];
        $_SESSION['password'] = $password;
        $_SESSION['loggedIn'] = TRUE;
        if ($row['s_db_name'] != ''){
            $_SESSION['db_name'] = $row['s_db_name'];
            $_SESSION['db_user'] = $row['s_db_user'];
        }
        $_SESSION['user_balance'] = $row['d_balance'];
        $_SESSION['iskakb'] = $row['s_iskakb'];
        if ($user->getRule() == 2){
            $_SESSION['main_account'] = 1;
        }
        $_SESSION['iskakb'] = $row['s_iskakb'];
        $_SESSION['logged_user_id'] = $row['pk_i_id'];
        return true;
    } else {
        return false;
    }
}

function  don_current_url($request_uri = false){
    if ($request_uri){
        return $_SERVER['REDIRECT_URL'];
    } else {
        return $_SERVER['REQUEST_URI'];
    }
}

//for cancel pay
function href($id,$show_cancel){
    $url = $show_cancel ? 'href="/payment/cancel/'.$id.'" class="cancel"' : '';
    return $url;
}

function don_format_price($price, $decimals = 2){
    if (!is_numeric($price)){
        return $price;
    }
    return number_format($price, $decimals, '.',' ');
}
/**
 * Gets the root url for your installation
 *
 * @param boolean $with_index true if index.php in the url is needed
 * @return string
 */
function osc_base_url($with_index = false) {
    $path = WEB_PATH;
    // add the index.php if it's true
    if($with_index) {
        $path .= "index.php";
    }

    return $path;
}

function osc_redirect_to($url, $code = null) {
    if(ob_get_length()>0) {
        ob_clean();
        ob_end_flush();
    }
    if($code!=null) {
        header("Location: ".$url, true, $code);
    } else {
        header("Location: ".$url);
    }
    exit;
}

function don_not_draw_header($css = true){
    __export('drawHeader', FALSE);
    if (!$css) {
        __export('skipCSS', true);
    }
}

function don_is_draw_header(){
    return __get('drawHeader') !== FALSE;
}

function don_logged_user_name(){
    return (string)$_SESSION['fio'];
}

function don_logged_user_iskakb(){
    return (string)get_preference('iskakb_name');
}
function don_user_logged_in(){
    if (isset($_SESSION['loggedIn']) && dbContainUser($_SESSION['name'], $_SESSION['password'])) {
        return TRUE;
    } elseif (\Classes\Cookie::newInstance()->get_value('oc_userSecret') && User::newInstance()->loginBySecret()){
        osc_redirect_to($_SERVER['REQUEST_URI']);
    } else {
        User::newInstance()->dropCookie();
        unset($_SESSION['loggedIn']);
        return false;
    }
}

function don_logged_user_id(){
    return get_current_user_id();
}

function run_controller($name){
    if ($name == ''){
        $name = 'main';
    }
    $filename = LIB_PATH.'controllers/'.$name.'.php';
    if (don_reqiure($filename)){
        $class = $name."Controller";
        $run = new $class();
    } else {
        return false;
    }
    
}

function osc_current_web_theme_path($file = '') {
    $filename = VIEW_PATH.$file;
    return don_reqiure($filename);
}

function don_reqiure($filename){
    if (file_exists($filename)){
        require $filename;
        return TRUE;
    } else {
        return false;
    }
}
function reestr_header($n, $kassir = '', $check = false){
	?>
        <h3>Реестр оплат пачка № <?=$n?> <?=$kassir != '' ? 'Кассир: ' . $kassir : ''?></h3>
	<table border='1' cellspacing='0'>
            <tr>
                <th>
                    №
                </th>
                <th>
                    л/с
                </th>
                <th>
                    Ф.И.О. абонента
                </th>
                <th>
                    Сумма
                </th>
                <th>
                    Дата
                </th>
                <th>
                    Улица
                </th>
                <?php
                if ($check){
                    echo "<th>Чек</th>";
                }
                ?>
            </tr>
	<?php
}

function don_get_insert_ls(){
    $sql = 'SELECT ls FROM abonent ORDER BY ls DESC LIMIT 1';
    $r = sql_in($sql);
    $ls['ls'] = 0;
    if ($r){
        $ls = $r->fetch();
    }
    return $ls['ls'] + 1;
}
function reestr_footer($count_dok,$summ, $check = false){
	?>
            <tr>
                <th>
                </th>
                <th colspan="2">
                    Итого
                </th>
                <th style="white-space: nowrap">
                    <?=number_format($summ,2,'.',' ')?>
                </th>
                <th>
                    
                </th>
                <th>
                    
                </th>
                <?php
                if ($check){
                    echo '<th></th>';
                }
                ?>
            </tr>
			</table>
			<br><br>
	<?php
}

function get_preference($name = '', $section = null){
    return don_get_preference($name, $section);
}

function  don_get_preference($name = '', $section = null, $renew = null){
    static $preferences;
    if ($name === ''){
        return false;
    }
    if (!is_array($preferences)) {
        $sql = "SELECT * FROM preference ";
        $r = sql_in($sql);
        $res = [];
        if ($r){
            $res = $r->fetchAll();
        }
        foreach ($res as $r) {
            $preferences[$r['s_name']] = $r['s_value'];
        }
    }

    if ($renew) {
        $preferences[$name] = $renew;
    } elseif (isset($preferences[$name])){
        return $preferences[$name];
    } else {
        return '';
    }
}
function don_clear_sql($str = ''){
    return preg_replace('/[`\'"#]/', '', $str);
}

function don_logError($text){
    $trace = debug_backtrace(false)[0];
    trigger_error("don_error: $text file: {$trace['file']} line: {$trace['line']}", E_USER_WARNING);
}

function don_json_header(){
    header("Content-type:application/json; charset=utf-8");
}

function don_userMenu(){
    $menu = [
        ['text' => 'Оплата', 'url' => don_url_payment(), 'rule' => RULE_KASSIR, 'page' => 'payment', 'action' => '','icon' => 'fa fa-money'],
        ['text' => 'Корректировка', 'url' => don_url_payment().'korrekt', 'rule' => RULE_KASSIR, 'page' => 'payment', 'action' => 'korrekt','icon' => 'fa fa-calculator'],
        ['text' => 'Орошение', 'url' => don_url_orosheniye(), 'rule' => RULE_KASSIR, 'page' => 'orosheniye', 'action' => '','icon' => 'glyphicon glyphicon-tint'],
        ['text' => 'База', 'url' => '#','icon' =>'glyphicon glyphicon-book', 'submenu' => [
            ['text' => 'Абоненты', 'url' => don_url_abonent(),     'rule' => RULE_ABONENT,    'page' => 'abonent',     'action' => '', 'icon' => 'glyphicon glyphicon-user'],
            ['text' => 'Мирабы',   'url' => don_url_controller(),  'rule' => RULE_KONTROLLER, 'page' => 'controller',  'action' => '', 'icon' => 'glyphicon glyphicon-list-alt'],
            ['text' => 'Тарифы',   'url' => don_url_tariff(),      'rule' => RULE_TARIFF,     'page' => 'tariff',      'action' => '', 'icon' => 'fa fa-dollar'],
//            ['text' =>'Тарификация','url' => '#',                  'rule' => RULE_TARIFF,     'page' => 'tariff',      'action' => 'e', 'icon' => 'fa fa-dollar'],
            ['text' => 'Каналы',   'url' => don_url_street(),      'rule' => RULE_STREET,     'page' => 'street',      'action' => '', 'icon' => 'fa fa-road'],
            ['text' => 'Растения', 'url' => don_url_rasteniye(),   'rule' => RULE_GLAVNIY,    'page' => 'rasteniye',   'action' => '', 'icon' => 'glyphicon glyphicon-grain']
        ]],
        ['text' => 'Отчеты', 'url' => '#','icon' =>'fa fa-file-text-o', 'submenu' => [
            ['text' => 'Реестр оплат',            'url' => don_url_payment().'reestr',    'rule' => RULE_KASSIR,    'page' => 'payment',     'action' => 'reestr', 'icon' => 'fa fa-money'],
            ['text' => 'Абоненты',                'url' => don_url_report().'form-1',     'rule' => 0,    'page' => 'report',     'action' => 'form-1', 'icon' => 'glyphicon glyphicon-user'],
            ['text' => 'Орошении',                'url' => don_url_report().'oroshenii',  'rule' => 0,    'page' => 'report',   'action' => 'oroshenii', 'icon' => 'fa fa-balance-scale'],
            ['text' => 'Мирабы',                  'url' => don_url_report().'mirab',      'rule' => 0,    'page' => 'report',     'action' => 'mirab', 'icon' => 'glyphicon glyphicon-list-alt'],
            ['text' => 'Каналы',                  'url' => don_url_report().'kanal',      'rule' => 0,    'page' => 'report',     'action' => 'kanal', 'icon' => 'fa fa-road'],
            ['text' => 'Печать извещении',      'url' => don_url_report().'izvesheniye',  'rule' => 0,    'page' => 'report',     'action' => 'izvesheniye', 'icon' => 'fa fa-bullhorn'],
            ['text' => 'История абонента',        'url' => don_url_report().'history',    'rule' => 0,    'page' => 'report',     'action' => 'history', 'icon' => 'fa fa-history'],
            ['text' => 'История орошений',        'url' => don_url_report().'orosheniye', 'rule' => 0, 'page' => 'report',     'action' => 'orosheniye', 'icon' => 'glyphicon glyphicon-tint'],
            ['text' => 'Закрыть отчет',           'url' => don_url_report().'close-report',   'rule' => RULE_GLAVNIY,    'page' => 'report',     'action' => 'close-report', 'icon' => 'fa fa-lock'],
        ]],
        ['text' => 'Настройки', 'url' => don_url_setting(), 'rule' => RULE_SETTINGS, 'page' => 'setting', 'action' => '','icon' => 'glyphicon glyphicon-cog'],
        ['text' => 'Выход', 'url' => don_url_login().'logout', 'page' => 'login', 'action' => 'logout','icon' => 'glyphicon glyphicon-log-out'],
    ];

    return $menu;
}

function don_drawMenu($menuItems, $submenu = false){
    $hasActive = false;
    $menu = '';
    foreach ($menuItems as $item) {
        $active = __get()->controller == $item['page'] && ( __get()->action == $item['action']) ? 'class="active"' : '';
        if ($item['rule'] && !user_has_rule($item['rule'])){
            continue;
        }
        if ($active){
            $hasActive = true;
        }

        $icon = $item['icon'] ? '<i class="'.$item['icon'].'"></i> ' : '';
        $subMenuUl = '';
        if ($item['submenu']){
            $subMenu = don_drawMenu($item['submenu'], true);
            $active = 'class="treeview'.($subMenu['active'] ? ' menu-open active' : '').'"';
            $subMenuUl =
                '<ul class="treeview-menu" style="display: '.($subMenu['active'] ? 'block' : 'none').'">'
                . $subMenu['menu']
                . '</ul>';
        }
        $menu .= "<li $active><a href='{$item['url']}'>$icon<span>{$item['text']}</span> "
            . ($subMenuUl ? '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>' : '')
            ."</a>$subMenuUl</li>";
    }
    if ($submenu){
        return ['menu' => $menu, 'active' => $hasActive];
    } else {
        echo $menu;
    }
}

function  don_set_preference($name = '',$value = '', $section = null){
    static $dao;
    if (!$dao instanceof DAO){
        $dao = new DAO();
    }
    if ($name === ''){
        return false;
    }
    $fields = [
        's_name' => $name,
        's_value' => $value,
        's_section' => $section
    ];
    $r = $dao->dao->replace('preference',$fields);
    if ($r){
        don_get_preference($name, $section, $value);
        return TRUE;
    } else {
        return '';
    }
}

function f1_header($ul_name,$c_name,$year,$month,$show_balance_2,$balance_2_name,$show_counter,$date1,$date2){
    ?>
        <h3>Сведения по учету абонентской платы за <?php echo  $year. 'г - ';monthName($month)?>. Форма №1. Ул.: <?=$ul_name?>, мираб: <?=$c_name?></h3>
          <table border="1" cellspacing="0">
            <tr>
                <th rowspan="2">
                    Л/с
                </th>
                <th rowspan="2">
                    ФИО
                </th>
<!--                <th rowspan="2">
                    Дом №
                </th>-->
                <?php if(!$show_counter){ ?>
                <th rowspan="2" style="writing-mode: vertical-rl">
                    кол-во<br>чел
                </th>
                <?php } ?>
                <th rowspan="2" >
                    Тариф
                </th>
                <?php if($show_counter){ ?>
                <th colspan="3">
                    показание
                </th>
                <?php } ?>
                <th rowspan="2">
                    Остаток    на<br>начало месяца    
                </th>
                <?php if ($show_balance_2){ ?>
                    <th rowspan="2">
                        <?=$balance_2_name?>
                    </th>
                <?php } ?>
                <th rowspan="2">
                    Начислено <br>по тарифу
                </th>
                <th rowspan="2">
                    Пеня
                </th>
                <th rowspan="2">
                    Всего начислено
                </th>
                <th rowspan="2">
                    Оплачено
                </th>
                <th rowspan="2">
                    Коррек-тировка
                </th>
                <th rowspan="2">
                    К оплате<br>(Остаток на <br>конец месяца)
                </th>
            </tr>
            <tr>
                <?php if($show_counter){ ?>
                    <td>Н</td>
                    <td>П</td>
                    <td>Р</td>
                <?php } ?>
            </tr>
    <?php
}

function f1_footer($ab,$kol,$ostNaNach,$nachislPoTarifu,$penya,$nachislVsego,$oplacheno,$korrekt,$ostNaKonec,$show_balance_2,$balance_2,$show_counter,$count_all){ ?>
            <tr>
                <th>
                    <?php echo $ab ?>
                </th>
                <th colspan="1">
                    Итого
                </th>
                <th>
                    <?php echo $kol ?>
                </th>
                <th <?=($show_counter ? 'colspan="2"' : '')?>>

                </th>
                <?php if ($show_counter)  {
                    echo "<th>$count_all</th>";
                } ?>
                <th>
                    <?php echo $ostNaNach ?>
                </th>
                <?php if ($show_balance_2){ ?>
                <th>
                    <?=$balance_2?>
                </th>
                <?php } ?>
                <th>
                    <?php echo $nachislPoTarifu ?>
                </th>
                <th>
                    <?php echo $penya ?>
                </th>
                <th>
                    <?php echo $nachislVsego ?>
                </th>
                <th>
                    <?php echo $oplacheno ?>
                </th>
                <th>
                    <?php echo $korrekt ?>
                </th>
                <th>
                    <?php echo $ostNaKonec ?>
                </th>
            </tr>
        </table>
    <?php
}

/**
 * @param $sql
 * @return bool|false|PDOStatement
 * @deprecated
 */
function sql_in($sql) {
    static $pdo;
    try {
        if (!isset($pdo) && defined('DB_NAME') && DB_NAME != ''){
            $pdo = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME,DB_USER,DB_PASSWORD);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->exec('SET NAMeS "UTF8"');
        }
        if ($pdo == null) {
            return false;
        }
        $result = $pdo->query($sql);
		return $result;
    } catch (PDOException $e) {
        unset($_SESSION['db_name']);
        unset($_SESSION['loggedIn']);
        osc_redirect_to('/login');
//        $error = 'Ошибка при извлечении записей: ' . $e->getmessage();
//        include 'error.html.php';
       exit();
    }
}

function htmlout($str) {
    echo htmlspecialchars($str, ENT_QUOTES, 'utf-8');
}

function sql_del($sql, $value) {
    include $_SERVER['DOCUMENT_ROOT'] . '/inc/db.inc.php';
    try {
        $s = $pdo->prepare($sql);
        $s->bindValue(':id', $value);
        $s->execute();
        $message = 'Успешно удалено!';
    } catch (PDOException $e) {
        $error = 'Ошибка при удалении записи: ' . $e->getmessage();
        include 'error.html.php';
        exit();
    }
}

function format_date($datetime, $date, $time) {
    $rus = array(
        'Января',
        'Февраля',
        'Марта',
        'Апреля',
        'Мая',
        'Июня',
        'Июля',
        'Августа',
        'Сентябра',
        'Октября',
        'Ноября',
        'Декабря'
    );
    $en = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );
    $today = date('Y-m-d');
    $yesterday = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
    global $date, $time;
    $date = date_create($datetime)->Format('Y-m-d');
    if ($today == $date) {
        $date = 'Сегодня';
    } else
    if ($yesterday == $date) {
        $date = 'Вчера';
    } else {
        $date = date_create($datetime)->Format('j F');
        $date = str_replace($en, $rus, $date);
    }

    $time = date_create($datetime)->Format('H:i');
    return TRUE;
}

function don_ls_fio_autocompleteJs($focus = 'summa', $callback = ''){ ?>
    <script type="text/javascript">
        var aLs = [];
        $('#ls').on('input',function(){
            var val = $(this).val();
            if (val.length < 3){
                aLs = [];
                return;
            }
            if(aLs.length < 1) {
                $.get("/ajax/abonentAutocomplete/?unlimit=1&term="+val, function(aLs){
                    $( "#fio" ).val('');
                    $( "#ls" ).autocomplete({
                        source: aLs,
                        minLength: 3,
                        select: function( event, ui ) {
                            var fio = ui.item.fio;
                            var ls = ui.item.ls;
                            $( "#fio" ).val(ui.item.fio);
                            set_values(ui.item);
                            <?=$callback?>
                        }
                    });
                })
            } else {
                var selected = false;
                $.each(aLs, function(i,v){
                    if (val == v['ls']){
                        $('#<?=$focus?>').focus();
                        $( "#ls" ).autocomplete( "disable" );
                        var fio = v['fio'];
                        var ls = v['ls'];
                        <?=$callback?>
                        selected = true;
                    }
                });
                if (!selected){
                    $( "#ls" ).autocomplete( "enable" );
                }
            }
        });
        $('#fio').keyup(function(){
//                $("#ls").val('');
            var val = $(this).val();
            $( "#fio" ).autocomplete({
                source: "/ajax/abonentAutocomplete/?term="+val,
                minLength: 2,
                select: function( event, ui ) {
                    $( "#ls" ).val(ui.item.ls).change();
                    set_values(ui.item);
                    var fio = ui.item['fio'];
                    var ls = ui.item['ls'];
                    <?=$callback?>
                }
            });
        });
        $('#ls').change(function(){
            var val = $(this).val();
            $.get('/ajax/abonentAutocomplete/?equals&term='+val,{},function(d){
                if (d.length > 5){
                    $( "#fio" ).val(d[0].fio);
                    set_values(d[0]);
                } else {
                    $(this).css('border','1 px solid red');
                }
            });
        })
        function set_values(data){
            $('#current_counter').val(data.i_counter)
            $('#current_balance_2').text(data.balance_2);
            $('#current_balance').text(data.balance);
            $('#<?=$focus?>').focus();
        }
        function clear_values(){
            $('#current_balance_2').text();
            $('#current_balance').text('');
        }
    </script>
        <?php
}

function back_url($href = ''){
    if ($href == ''){
        $ref = __getReferer();
        $href = $ref != '' ? $ref : '..';
        if ($href == $_SERVER['REQUEST_URI'] || $href == '..'){
            $href = substr($_SERVER['REQUEST_URI'],-1,1) == '/' ? '..' : '.';
        }
    }
    echo "<a href='$href' class='back_url'>Назад</a>";
}

function don_head_links($title = 'Система'){ ?>
    
        <title><?=$title?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="/assets/js/jquery-min.js"></script>
        <script type="text/javascript" src="/assets/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="/assets/js/jquery-ui-1.12.1/jquery-ui.min.css" />
        <link rel="stylesheet" href="/assets/js/jquery-ui-1.12.1/jquery-ui.theme.min.css" />
        <link rel="stylesheet" href="/assets/js/jquery-ui-1.12.1/jquery-ui.structure.min.css" />
        <?php
}

function don_do_403() {
    osc_add_flash_error_message('У вас не достаточно прав!');
    osc_redirect_to(osc_base_url());
}

function __saveFrom($params = []) {
    $aParams = Params::getParamsAsArray();
    foreach ($aParams as $k => $v) {
        if (isset($params[$k])){
            Session::newInstance()->_setForm($k,$params[$k]);
        } else {
            Session::newInstance()->_setForm($k,$v);
        }
    }
}

function __getSavedFrom() {
    return Session::newInstance()->_getForm();
}

function don_get_md5_password($password = null) {
    if ($password == null){
        $password = __getParam('password');
    }
    $salt = "hayotbahshdd";
    $pass = md5(md5($password) . $salt);
    return $pass;
}

function check_rule($rule) {
    if (!user_has_rule($rule)){
        don_do_403();
    }
}

function cc($var){
    foreach (func_get_args() as $arg) {
        echo '<pre>';
        var_dump($arg);
        echo '</pre>';
    }
}

/***********************
 * CSRFGUARD functions *
 ***********************/
function osc_csrfguard_generate_token() {
    $token_name = Session::newInstance()->_get('token_name');
    if($token_name!='' && Session::newInstance()->_get($token_name)!='') {
        return array($token_name, Session::newInstance()->_get($token_name));
    }
    $unique_token_name = 'DCRSRF' . "_".mt_rand(0,mt_getrandmax());
    if(function_exists("hash_algos") and in_array("sha512",hash_algos())) {
        $token = hash("sha512",mt_rand(0,mt_getrandmax()));
    } else {
        $token = '';
        for ($i=0;$i<128;++$i) {
            $r=mt_rand(0,35);
            if($r<26) {
                $c=chr(ord('a')+$r);
            } else {
                $c=chr(ord('0')+$r-26);
            }
            $token.=$c;
        }
        $token .= mt_rand(0,mt_getrandmax());
    }
    Session::newInstance()->_set('token_name', $unique_token_name);
    Session::newInstance()->_set($unique_token_name, $token);
    return array($unique_token_name, $token);
}


/**
 * Create a CSRF token to be placed in a form
 *
 * @since 3.1
 * @return string
 */
function osc_csrf_token_form() {
    list($name, $token) = osc_csrfguard_generate_token();
    return "<input type='hidden' name='CSRFName' value='".$name."' />
        <input type='hidden' name='CSRFToken' value='".$token."' />";
}


/**
 * Create a CSRF token to be placed in a url
 *
 * @since 3.1
 * @return string
 */
function osc_csrf_token_url() {
    list($name, $token) = osc_csrfguard_generate_token();
    return "CSRFName=".$name."&CSRFToken=".$token;
}


/**
 * Check if CSRF token is valid, die in other case
 *
 * @since 3.1
 */
function osc_csrf_check() {
    $error      = false;
    $str_error  = '';
    if(Params::getParam('CSRFName')=='' || Params::getParam('CSRFToken')=='') {
        $str_error = 'Probable invalid request';
        $error = true;
    } else {
        $name   = Params::getParam('CSRFName');
        $token  = Params::getParam('CSRFToken');
        if (!osc_csrfguard_validate_token($name, $token)) {
            $str_error = 'Invalid CSRF token';
            $error = true;
        }
    }

    if( defined('IS_AJAX') ) {
        if($error && IS_AJAX === true ) {
            echo json_encode(array(
                'error' => 1,
                'msg'   => $str_error
            ));
            exit;
        }
    }

    // check ajax request
    if($error) {
        osc_add_flash_error_message($str_error);

        $url = __getReferer();
        // drop session referer
        Session::newInstance()->_dropReferer();
        if($url!='') {
            osc_redirect_to($url);
        }

        osc_redirect_to( osc_base_url() );
    }
}


function osc_csrfguard_validate_token($unique_form_name, $token_value) {
    $name = Session::newInstance()->_get('token_name');
    $token = Session::newInstance()->_get($unique_form_name);
    if($name===$unique_form_name && $token===$token_value) {
        return true;
    } else {
        return false;
    }
    return $result;
}

/**
 * Escape html
 *
 * Formats text so that it can be safely placed in a form field in the event it has HTML tags.
 *
 * @access  public
 * @version 2.4
 * @param   string
 * @return  string
 */
function osc_esc_html($str = '') {
    if ($str === '') {
        return '';
    }

    $temp = '__TEMP_AMPERSANDS__';

    // Replace entities to temporary markers so that
    // htmlspecialchars won't mess them up
    $str = preg_replace("/&#(\d+);/", "$temp\\1;", $str);
    $str = preg_replace("/&(\w+);/",  "$temp\\1;", $str);

    $str = htmlspecialchars($str);

    // In case htmlspecialchars misses these.
    $str = str_replace(array("'", '"'), array("&#39;", "&quot;"), $str);

    // Decode the temp markers back to entities
    $str = preg_replace("/$temp(\d+);/","&#\\1;",$str);
    $str = preg_replace("/$temp(\w+);/","&\\1;",$str);

    return $str;
}

function don_icon($class){
    echo "<i class='$class'></i>";
}

function don_set_current_report_id($id)
{
    return don_set_preference('lastId', $id);
}

function don_current_report_id(){
    return don_get_preference('lastId');
}

function don_set_prev_report_id($id)
{
    return don_set_preference('prevId', $id);
}

function don_prev_report_id(){
    return don_get_preference('prevId');
}

function don_current_report_date(){
    return \Models\Report::newInstance()->currentReportYear();
}

function don_handle_exception (Exception $ex) {
    if (OSC_DEBUG){
        echo '<b style="color: red; font-weight: bold;">ERROR in '.$ex->getFile().' at line '.$ex->getLine().'</b><br>';
        cc($ex->getMessage(), $ex->getTrace());
        exit;
    } else {
        osc_add_flash_error_message($ex->getMessage());
    }
}

/**
 * Creates a random password.
 * @param int password $length. Default to 8.
 * @return string
 */
function osc_genRandomPassword($length = 15) {
    $dict = array_merge(range('a', 'z'), range('0', '9'), range('A', 'Z'));
    shuffle($dict);

    $pass = '';
    for($i = 0; $i < $length; $i++)
        $pass .= $dict[rand(0, count($dict) - 1)];

    return $pass;
}

/**
 * @return string price для тарифа 2 и 3 четверти
 */
function don_get_tarif_23() {
    return don_get_preference('tarif_ii-iii') ?: '7.20';
}
/**
 * @return string price для тарифа 4 и 1 четверти
 */
function don_get_tarif_41() {
    return don_get_preference('tarif_iv-i') ?: '5.20';
}