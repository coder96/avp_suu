ALTER TABLE `orosheniye` CHANGE `fk_report_id` `fk_report_id` INT(11) UNSIGNED NOT NULL;

UPDATE orosheniye SET fk_report_id = 1 WHERE 1;

ALTER TABLE orosheniye DROP FOREIGN KEY orosheniye_ibfk_2;

ALTER TABLE `orosheniye` ADD FOREIGN KEY (`fk_report_id`) REFERENCES `report`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;