INSERT INTO report set year = 2019;

INSERT INTO abonent_history SELECT null, 1, ls, fio, s_phone, s_passport FROM abonent;

ALTER TABLE `abonent_ekin` ADD `fk_report_id` INT UNSIGNED NOT NULL AFTER `id`;

UPDATE `abonent_ekin` SET `fk_report_id` = 1 where 1;

ALTER TABLE `abonent_ekin` ADD FOREIGN KEY (`fk_report_id`) REFERENCES `report`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

REPLACE INTO preference (s_name, s_value, s_section) VALUES ('prevId', '1', NULL);

REPLACE INTO preference (s_name, s_value, s_section) VALUES ('lastId', '1', NULL);

ALTER TABLE `abonent_history` ADD FOREIGN KEY (`ls`) REFERENCES `abonent`(`ls`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `abonent_history` ADD FOREIGN KEY (`report_id`) REFERENCES `report`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `abonent_history` ADD UNIQUE( `report_id`, `ls`);

