CREATE TABLE `report` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT , `year` YEAR NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `abonent_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_id` int(10) UNSIGNED NOT NULL,
  `ls` int(10) UNSIGNED NOT NULL,
  `fio` varchar(40) NOT NULL,
  `s_phone` varchar(20) NOT NULL,
  `s_passport` varchar(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `abonent_history`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `abonent_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
