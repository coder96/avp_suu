ALTER ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW
    `v_abonent_ekin` AS select `e`.`id`            AS `id`,
                               `e`.`fk_report_id`  AS `fk_report_id`,
                               `e`.`fk_abonent_id` AS `fk_abonent_id`,
                               `e`.`fk_ekin_id`    AS `fk_ekin_id`,
                               `e`.`i_ga`          AS `i_ga`,
                               `e`.`i_num`         AS `i_num`,
                               `ek`.`name`         AS `name`,
                               `a`.`fio`           AS `fio`
                        from ((`suunetk_avp`.`abonent_ekin` `e` join `suunetk_avp`.`abonent` `a` on ((`a`.`ls` = `e`.`fk_abonent_id`)))
                                 join `suunetk_avp`.`ekin` `ek` on ((`ek`.`id` = `e`.`fk_ekin_id`)))