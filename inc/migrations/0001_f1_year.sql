CREATE TABLE `f1_year`
(
    `pk_i_id`         int(11) UNSIGNED NOT NULL,
    `id`              int(11)          NOT NULL,
    `ls`              int(11) UNSIGNED NOT NULL,
    `ostNaNach`       decimal(10, 2)   NOT NULL,
    `i_litr`          int(11) UNSIGNED NOT NULL DEFAULT '0',
    `penya`           decimal(10, 2)   NOT NULL,
    `nachislPoTarifu` decimal(10, 2)   NOT NULL,
    `oplacheno`       decimal(10, 2)   NOT NULL,
    `korrekt`         decimal(10, 2)   NOT NULL,
    `ostNaKonec`      decimal(10, 2)   NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

ALTER TABLE `f1_year`
    ADD UNIQUE KEY `pk_i_id` (`pk_i_id`),
    ADD KEY `id` (`id`),
    ADD KEY `ls` (`ls`);

ALTER TABLE `f1_year`
    MODIFY `pk_i_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `f1_year`
    ADD CONSTRAINT `f1_year_ibfk_1` FOREIGN KEY (`id`) REFERENCES `f1` (`id`),
    ADD CONSTRAINT `f1_year_ibfk_2` FOREIGN KEY (`ls`) REFERENCES `abonent` (`ls`) ON UPDATE CASCADE;

INSERT INTO `f1_year`(`pk_i_id`, `id`, `ls`, `ostNaNach`, `i_litr`, `penya`, `nachislPoTarifu`, `oplacheno`, `korrekt`,
                      `ostNaKonec`)
SELECT null,
       1,
       ls,
       0,
       max(schet_tek),
       0,
       sum(nachislPoTarifu),
       sum(oplacheno),
       sum(korrekt),
       0
from f1_det
GROUP BY ls;

update f1_year set ostNaKonec = nachislPoTarifu - oplacheno + korrekt WHERE 1;
