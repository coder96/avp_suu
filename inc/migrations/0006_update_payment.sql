update payment set id_otcheta = 1 where 1;

ALTER TABLE payment DROP FOREIGN KEY payment_ibfk_1;

ALTER TABLE `payment` CHANGE `id_otcheta` `id_otcheta` INT(11) UNSIGNED NOT NULL;

ALTER TABLE `payment` ADD FOREIGN KEY (`id_otcheta`) REFERENCES `report`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;