CREATE TABLE `migration`
(
    `id`      INT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name`    VARCHAR(150) NOT NULL,
    `dt_date` DATETIME     NOT NULL,
    `status`  VARCHAR(150) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;