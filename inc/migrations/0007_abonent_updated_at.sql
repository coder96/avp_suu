ALTER TABLE abonent ADD COLUMN updated_at DATETIME default null;

CREATE TRIGGER `updated_at` BEFORE UPDATE ON `abonent` FOR EACH ROW
    SET NEW.updated_at = now();