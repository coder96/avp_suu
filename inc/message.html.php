<?php
session_start();
if ($_SESSION['loggedIn']) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/access.inc.php';
} else {
    header("Location: http://" . $_SERVER['HTTP_HOST']);
    exit();
}
global $message, $title;
$nazad = $_SESSION['referer'] != '' ? $_SESSION['referer'] : '.';
$_SESSION['referer'] = '';
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    </head>
    <body>

        <h3><?php echo $message ?></h3>
        <a href="<?=$nazad?>">Назад</a>
    </body>
</html>
