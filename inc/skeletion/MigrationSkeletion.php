<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\Migration;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class MigrationSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var MigrationSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var Migration
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    private $excluded_fields = [];

    
    private $id;
    private $name;
    private $dt_date;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of Migration
     * @return MigrationSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Migration){
            self::$model = Migration::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        return $this->array;
    }

    /**
     * @param $array array fields of Migration
     * @return bool|MigrationSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        $next = $skeletion->next();
        return  $next ? $next : self::$instance;
    }

    /**
     * @param $exclude
     * @return array
     */
    public function toArray($exclude = [])
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                if (!in_array($field, $exclude)){
                    $return[$field] = $this->$field;
                }
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray($this->excluded_fields));
        if ($res){
            $primary = self::$primary;
            $this->$primary = Migration::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray($this->excluded_fields),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    /**
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->id = $value;
        return $this;    
    }
    public function getName() {
        return $this->name;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setName ($value) {
        $this->name = $value;
        return $this;    
    }
    public function getDate() {
        return $this->dt_date;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDate ($value) {
        $this->dt_date = $value;
        return $this;    
    }
}