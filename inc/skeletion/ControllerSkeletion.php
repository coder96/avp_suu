<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\Controller;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class ControllerSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var ControllerSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var Controller
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    //не трогай!!! тут вставится динамические свойства
    
    private $id;
    private $s_name;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of Controller
     * @return ControllerSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Controller){
            self::$model = Controller::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    /**
     * @param $array array fields of Controller
     * @return bool|ControllerSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        return $skeletion->next();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                $return[$field] = $this->$field;
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray());
        if ($res){
            $primary = self::$primary;
            $this->$primary = Controller::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray(),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->id = $value;
        return $this;    
    }
    public function getName() {
        return $this->s_name;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setName ($value) {
        $this->s_name = $value;
        return $this;    
    }
}