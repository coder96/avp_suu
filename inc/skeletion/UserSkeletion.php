<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use \User;

//если найдешь ошибку чини ее в файле Models-TemplateSkeletion в папке skeletion и перегенерируй модель
class UserSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var UserSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var User
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    //не трогай!!! тут вставится динамические свойства
    
    private $pk_i_id;
    private $fk_i_parent_id;
    private $fio;
    private $s_name;
    private $s_password;
    private $s_db_name;
    private $s_db_user;
    private $s_iskakb;
    private $s_domain;
    private $d_balance;
    private $i_tariffPrice;
    private $i_rule;
    private $i_block;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of User
     * @return UserSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof User){
            self::$model = User::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    /**
     * @param $array array fields of User
     * @return bool|UserSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        return $skeletion->next();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                $return[$field] = $this->$field;
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray());
        if ($res){
            $primary = self::$primary;
            $this->$primary = User::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray(),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->pk_i_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->pk_i_id = $value;
        return $this;    
    }
    public function getIParentId() {
        return $this->fk_i_parent_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setIParentId ($value) {
        $this->fk_i_parent_id = $value;
        return $this;    
    }
    public function getFio() {
        return $this->fio;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setFio ($value) {
        $this->fio = $value;
        return $this;    
    }
    public function getName() {
        return $this->s_name;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setName ($value) {
        $this->s_name = $value;
        return $this;    
    }
    public function getPassword() {
        return $this->s_password;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPassword ($value) {
        $this->s_password = $value;
        return $this;    
    }
    public function getDbName() {
        return $this->s_db_name;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDbName ($value) {
        $this->s_db_name = $value;
        return $this;    
    }
    public function getDbUser() {
        return $this->s_db_user;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDbUser ($value) {
        $this->s_db_user = $value;
        return $this;    
    }
    public function getIskakb() {
        return $this->s_iskakb;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setIskakb ($value) {
        $this->s_iskakb = $value;
        return $this;    
    }
    public function getDomain() {
        return $this->s_domain;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDomain ($value) {
        $this->s_domain = $value;
        return $this;    
    }
    public function getBalance() {
        return $this->d_balance;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setBalance ($value) {
        $this->d_balance = $value;
        return $this;    
    }
    public function getTariffprice() {
        return $this->i_tariffPrice;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setTariffprice ($value) {
        $this->i_tariffPrice = $value;
        return $this;    
    }
    public function getRule() {
        return $this->i_rule;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setRule ($value) {
        $this->i_rule = $value;
        return $this;    
    }
    public function getBlock() {
        return $this->i_block;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setBlock ($value) {
        $this->i_block = $value;
        return $this;    
    }
}