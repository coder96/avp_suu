<?php
/** Created by don console **/

namespace Skeletion\View;

use \Exception;
use Models\View\Orosheniye;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class OrosheniyeSkeletion extends \Skeletion\OrosheniyeSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var OrosheniyeSkeletion
     */
    protected static $instance;
    protected static $fields = [];

    /**
     * @var Orosheniye
     */
    protected static $model;

    /**
     * @var string primary key
     */
    protected static $primary;

    protected $excluded_fields = [];

    protected $i_ga;
    protected $name;
    protected $fio;
    protected $ls;

    /**
     * @access protected
     * @var array
     */
    protected $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of Orosheniye
     * @return OrosheniyeSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Orosheniye){
            self::$model = Orosheniye::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    public function getGa() {
        return $this->i_ga;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setGa ($value) {
        $this->i_ga = $value;
        return $this;    
    }
    public function getName() {
        return $this->name;
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setName ($value) {
        $this->name = $value;
        return $this;    
    }
    public function getFio() {
        return $this->fio;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setFio ($value) {
        $this->fio = $value;
        return $this;    
    }
    public function getAbonentLs() {
        return $this->ls;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setAbonentLs ($value) {
        $this->ls = $value;
        return $this;    
    }

    public function getAll()
    {
        return $this->array;
    }
}