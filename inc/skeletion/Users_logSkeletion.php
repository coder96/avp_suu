<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\Users_log;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class Users_logSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var Users_logSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var Users_log
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    //не трогай!!! тут вставится динамические свойства
    
    private $id;
    private $fk_user_id;
    private $ip;
    private $user_agent;
    private $date;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of Users_log
     * @return Users_logSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Users_log){
            self::$model = Users_log::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    /**
     * @param $array array fields of Users_log
     * @return bool|Users_logSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        return $skeletion->next();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                $return[$field] = $this->$field;
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray());
        if ($res){
            $primary = self::$primary;
            $this->$primary = Users_log::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray(),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->id = $value;
        return $this;    
    }
    public function getUserId() {
        return $this->fk_user_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setUserId ($value) {
        $this->fk_user_id = $value;
        return $this;    
    }
    public function getIp() {
        return $this->ip;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setIp ($value) {
        $this->ip = $value;
        return $this;    
    }
    public function getUserAgent() {
        return $this->user_agent;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setUserAgent ($value) {
        $this->user_agent = $value;
        return $this;    
    }
    public function getDate() {
        return $this->date;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDate ($value) {
        $this->date = $value;
        return $this;    
    }
}