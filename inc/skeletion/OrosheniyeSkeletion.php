<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\Orosheniye;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class OrosheniyeSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var OrosheniyeSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var Orosheniye
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    private $excluded_fields = [];

    
    protected $id;
    protected $fk_report_id;
    protected $fk_abonent_ekin_id;
    protected $d_tariff_price;
    protected $i_ls;
    protected $i_summa;
    protected $d_date;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of Orosheniye
     * @return OrosheniyeSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Orosheniye){
            self::$model = Orosheniye::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        return $this->array;
    }

    /**
     * @param $array array fields of Orosheniye
     * @return $this
     */
    public static function build($array = [])
    {
        $skeletion = static::fromArray([$array]);
        $next = $skeletion->next();
        return  $next ? $next : self::$instance;
    }

    /**
     * @param $exclude
     * @return array
     */
    public function toArray($exclude = [])
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                if (!in_array($field, $exclude)){
                    $return[$field] = $this->$field;
                }
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray($this->excluded_fields));
        if ($res){
            $primary = self::$primary;
            $this->$primary = Orosheniye::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray($this->excluded_fields),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    /**
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->id = $value;
        return $this;    
    }

    public function getReportId()
    {
        return $this->fk_report_id;
    }

    public function setReportId($value)
    {
        $this->fk_report_id = $value;
        return $this;
    }

    public function getSumma() {
        return $this->i_summa;
    }

    public function setSumma($value) {
        $this->i_summa = $value;
        return $this;
    }


    public function getAbonentEkinId() {
        return $this->fk_abonent_ekin_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setAbonentEkinId ($value) {
        $this->fk_abonent_ekin_id = $value;
        return $this;    
    }
    public function getTariffPrice() {
        return $this->d_tariff_price;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setTariffPrice ($value) {
        $this->d_tariff_price = $value;
        return $this;    
    }
    public function getLs() {
        return $this->i_ls;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setLs ($value) {
        $this->i_ls = $value;
        return $this;    
    }
    public function getDate() {
        return $this->d_date;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDate ($value) {
        $this->d_date = $value;
        return $this;    
    }
}