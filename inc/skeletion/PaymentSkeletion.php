<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\Payment;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class PaymentSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var PaymentSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var Payment
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    //не трогай!!! тут вставится динамические свойства
    
    private $id;
    private $fk_abonent_id;
    private $i_summa;
    private $dt_date;
    private $i_status;
    private $dt_success;
    private $fk_service_id;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of Payment
     * @return PaymentSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Payment){
            self::$model = Payment::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    /**
     * @param $array array fields of Payment
     * @return bool|PaymentSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        return $skeletion->next();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                $return[$field] = $this->$field;
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray());
        if ($res){
            $primary = self::$primary;
            $this->$primary = Payment::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray(),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->id = $value;
        return $this;    
    }
    public function getAbonentId() {
        return $this->fk_abonent_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setAbonentId ($value) {
        $this->fk_abonent_id = $value;
        return $this;    
    }
    public function getSumma() {
        return $this->i_summa;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setSumma ($value) {
        $this->i_summa = $value;
        return $this;    
    }
    public function getDate() {
        return $this->dt_date;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDate ($value) {
        $this->dt_date = $value;
        return $this;    
    }
    public function getStatus() {
        return $this->i_status;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setStatus ($value) {
        $this->i_status = $value;
        return $this;    
    }
    public function getSuccess() {
        return $this->dt_success;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setSuccess ($value) {
        $this->dt_success = $value;
        return $this;    
    }
    public function getServiceId() {
        return $this->fk_service_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setServiceId ($value) {
        $this->fk_service_id = $value;
        return $this;    
    }
}