<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\AbonentHistory;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class AbonentHistorySkeletion
{
    /**
     * @access private
     * @since unknown
     * @var AbonentHistorySkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var AbonentHistory
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    private $excluded_fields = [];

    
    private $id;
    private $report_id;
    private $ls;
    private $fio;
    private $s_phone;
    private $s_passport;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of AbonentHistory
     * @return AbonentHistorySkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof AbonentHistory){
            self::$model = AbonentHistory::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        return $this->array;
    }

    /**
     * @param $array array fields of AbonentHistory
     * @return bool|AbonentHistorySkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        $next = $skeletion->next();
        return  $next ? $next : self::$instance;
    }

    /**
     * @param $exclude
     * @return array
     */
    public function toArray($exclude = [])
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                if (!in_array($field, $exclude)){
                    $return[$field] = $this->$field;
                }
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray($this->excluded_fields));
        if ($res){
            $primary = self::$primary;
            $this->$primary = AbonentHistory::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray($this->excluded_fields),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    /**
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->id = $value;
        return $this;    
    }
    public function getReportId() {
        return $this->report_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setReportId ($value) {
        $this->report_id = $value;
        return $this;    
    }
    public function getLs() {
        return $this->ls;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setLs ($value) {
        $this->ls = $value;
        return $this;    
    }
    public function getFio() {
        return $this->fio;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setFio ($value) {
        $this->fio = $value;
        return $this;    
    }
    public function getPhone() {
        return $this->s_phone;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPhone ($value) {
        $this->s_phone = $value;
        return $this;    
    }
    public function getPassport() {
        return $this->s_passport;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPassport ($value) {
        $this->s_passport = $value;
        return $this;    
    }
}