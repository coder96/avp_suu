<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use Models\Abonent;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class AbonentSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var AbonentSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var Abonent
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    //не трогай!!! тут вставится динамические свойства
    
    private $ls;
    private $fio;
    private $ul;
    private $balance;
    private $i_counter;
    private $b_counter;
    private $d_penyaLastExec;
    private $b_notified;
    private $s_phone;
    private $s_passport;
    private $s_dogovor;
    private $ulname;
    private $cname;
    private $updated_at;

    private $excluded_fields = ['ulname', 'cname', 'updated_at'];
    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of v_abonent
     * @return AbonentSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof Abonent){
            self::$model = Abonent::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        return $this->array;
    }
    /**
     * @param $array array fields of Abonent
     * @return bool|AbonentSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        $next = $skeletion->next();
        return  $next ? $next : self::$instance;
    }

    /**
     * @param $exclude
     * @return array
     */
    public function toArray($exclude = [])
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                if (!is_null($this->$field) && !in_array($field, $exclude)){
                    $return[$field] = $this->$field;
                }
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function insert()
    {
        $res = self::$model->insert($this->toArray($this->excluded_fields));
        if ($res){
            $primary = self::$primary;
            $this->$primary = Abonent::newInstance()->insertedId();
        } else {
            throw new Exception(self::$model->getErrorDesc(), ERR_C_MYSQL);
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->update($this->toArray($this->excluded_fields),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getLs() {
        return $this->ls;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setLs ($value) {
        $this->ls = $value;
        return $this;    
    }
    public function getFio() {
        return $this->fio;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setFio ($value) {
        $this->fio = $value;
        return $this;    
    }
    public function getUl() {
        return $this->ul;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setUl ($value) {
        $this->ul = $value;
        return $this;    
    }
    public function getBalance() {
        return $this->balance;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setBalance ($value) {
        $this->balance = $value;
        return $this;    
    }
    public function getCounter() {
        return $this->i_counter;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setCounter ($value) {
        $this->i_counter = $value;
        return $this;    
    }
    public function getBCounter() {
        return $this->b_counter;
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setBCounter ($value) {
        $this->b_counter = $value;
        return $this;
    }
    public function getPenyalastexec() {
        return $this->d_penyaLastExec;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPenyalastexec ($value) {
        $this->d_penyaLastExec = $value;
        return $this;    
    }
    public function getNotified() {
        return $this->b_notified;    
    }

    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setNotified ($value) {
        $this->b_notified = $value;
        return $this;    
    }
    public function getPhone() {
        return $this->s_phone;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPhone ($value) {
        $this->s_phone = $value;
        return $this;    
    }

    public function getPassport() {
        return $this->s_passport;
    }
    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPassport ($value) {
        $this->s_passport = $value;
        return $this;
    }
    public function getDogovor() {
        return $this->s_dogovor;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setDogovor ($value) {
        $this->s_dogovor = $value;
        return $this;    
    }

    public function getUlname() {
        return $this->ulname;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setUlname ($value) {
        $this->ulname = $value;
        return $this;    
    }
    public function getCname() {
        return $this->cname;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setCname ($value) {
        $this->cname = $value;
        return $this;    
    }
}