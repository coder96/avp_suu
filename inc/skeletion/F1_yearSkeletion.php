<?php
/** Created by don console **/

namespace Skeletion;

use \Exception;
use \F1_year;

//если найдете ошибку чините ее в файле Models-TemplateSkeletion в папке skeletion
class F1_yearSkeletion
{
    /**
     * @access private
     * @since unknown
     * @var F1_yearSkeletion
     */
    private static $instance;
    private static $fields = [];

    /**
     * @var F1_year
     */
    private static $model;

    /**
     * @var string primary key
     */
    private static $primary;

    private $excluded_fields = [];

    
    private $pk_i_id;
    private $id;
    private $ls;
    private $ostNaNach;
    private $i_litr;
    private $penya;
    private $nachislPoTarifu;
    private $oplacheno;
    private $korrekt;
    private $ostNaKonec;

    /**
     * @access private
     * @var array
     */
    private $array;

    /**
     * @access public
     * @since unknown
     * @param $array array of F1_year
     * @return F1_yearSkeletion
     */
    public static function fromArray(array $array)
    {
        self::$instance = new self();
        self::$instance->array = $array;

        return self::$instance;
    }

    function __construct($array = []) {
        if (!self::$model instanceof F1_year){
            self::$model = F1_year::newInstance();
        }
        if (empty(self::$fields)){
            self::$fields = self::$model->getFields();
        }
        if (!isset(self::$primary)){
            self::$primary = self::$model->getPrimaryKey();
        }

        $this->array = [$array];
        $this->next();
    }

    /**
     * @return $this|bool
     */
    public function next()
    {
        $current = current($this->array);
        if ($current){
            next($this->array);
            try {
                foreach (array_keys($current) as $key) {
                    $this->$key = $current[$key];
                }
            } catch (Exception $ex){
                don_logError($ex->getMessage());
                return false;
            }
            return $this;
        } else {
            return false;
        }
    }

    public function getAll()
    {
        return $this->array;
    }

    /**
     * @param $array array fields of F1_year
     * @return bool|F1_yearSkeletion
     */
    public static function build($array = [])
    {
        $skeletion = self::fromArray([$array]);
        $next = $skeletion->next();
        return  $next ? $next : self::$instance;
    }

    /**
     * @param $exclude
     * @return array
     */
    public function toArray($exclude = [])
    {
        $fields = self::$fields;
        if (empty($fields)){
            return [];
        }
        $return = [];
        try {
            foreach ($fields as $field) {
                if (!in_array($field, $exclude)){
                    $return[$field] = $this->$field;
                }
            }
        } catch (Exception $ex){
            don_logError($ex->getMessage());
        }
        return $return;
    }

    public function insert()
    {
        $res = self::$model->insert($this->toArray($this->excluded_fields));
        if ($res){
            $primary = self::$primary;
            $this->$primary = F1_year::newInstance()->insertedId();
        }
        return $res;
    }

    public function update()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->parent_update($this->toArray($this->excluded_fields),[$primary => $value]);
    }

    public function delete()
    {
        $primary = self::$primary;
        $value = $this->$primary;
        return self::$model->deleteByPrimaryKey($value);
    }

    /**
     * @param $name
     * @param $value
     * @throws Exception
     */
    public function __set($name, $value)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function __get($name)
    {
        throw new Exception('Нет такого поля у объекта: '.$name);
    }

    //не трогай!!! тут вставится динамические функции
    
    public function getId() {
        return $this->pk_i_id;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setId ($value) {
        $this->pk_i_id = $value;
        return $this;    
    }

    public function getReportId() {
        return $this->id;
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setReportId ($value) {
        $this->id = $value;
        return $this;
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setLs ($value) {
        $this->ls = $value;
        return $this;    
    }
    public function getOstnanach() {
        return $this->ostNaNach;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setOstnanach ($value) {
        $this->ostNaNach = $value;
        return $this;    
    }
    public function getLitr() {
        return $this->i_litr;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setLitr ($value) {
        $this->i_litr = $value;
        return $this;    
    }
    public function getPenya() {
        return $this->penya;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setPenya ($value) {
        $this->penya = $value;
        return $this;    
    }
    public function getNachislpotarifu() {
        return $this->nachislPoTarifu;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setNachislpotarifu ($value) {
        $this->nachislPoTarifu = $value;
        return $this;    
    }
    public function getOplacheno() {
        return $this->oplacheno;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setOplacheno ($value) {
        $this->oplacheno = $value;
        return $this;    
    }
    public function getKorrekt() {
        return $this->korrekt;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setKorrekt ($value) {
        $this->korrekt = $value;
        return $this;    
    }
    public function getOstnakonec() {
        return $this->ostNaKonec;    
    }

    /**
     * @access public
     * @since unknown
     * @param $value mixed
     * @return $this
     */
    public function setOstnakonec ($value) {
        $this->ostNaKonec = $value;
        return $this;    
    }
}