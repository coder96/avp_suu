<?php
define('LIB_PATH', $_SERVER['DOCUMENT_ROOT'].'/inc/');
define('VIEW_PATH', $_SERVER['DOCUMENT_ROOT'].'/view/');
define('UPLOADS_PATH', $_SERVER['DOCUMENT_ROOT'].'/public/uploads/');
define('INCLUDES_PATH', $_SERVER['DOCUMENT_ROOT']);
define('MODELS_PATH', LIB_PATH . 'models/');
define('SKELETION_PATH', LIB_PATH . 'skeletion/');
define('MULTISITE', false);

define('DB_TABLE_PREFIX', '');
define('ABS_PATH', '');

if (strpos($_SERVER['PATH'], 'Doniyor\phpStorm') !== FALSE || strpos($_SERVER['PATH'], 'openserver') !== FALSE) {
    define('OSC_DEBUG', true);
} else {
    define('OSC_DEBUG', false);
}

define('WEB_PATH', 'http://'.$_SERVER['HTTP_HOST'].'/');
define('REL_WEB_URL', '/');

define('RULE_PROGRAMMIST', 1);
define('RULE_GLAVNIY', 2);
define('RULE_BUHGALTER', 3);
define('RULE_KASSIR', 4);
define('RULE_ABONENT', 5);
define('RULE_KONTROLLER', 6);
define('RULE_STREET', 7);
define('RULE_TARIFF', 8);
define('RULE_OROSHENIYE', 9);

//define('RULE_OTCHET_ABONENT', 10);
//define('RULE_OTCHET_KONTROLLER', 11);
//define('RULE_OTCHET_STREET', 12);
//define('RULE_OTCHET_TARIFF', 13);

define('RULE_SETTINGS', 14);

//define('js_url', $_SERVER['DOCUMENT_ROOT']);