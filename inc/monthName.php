<?php
function monthName($number,$descr = TRUE, $return = false){
    $number = intval($number);
    $month = getMonths();
    $res = $month[$number];
    if ($descr){
        $res .= ' айына';
    }
    if ($return){
        return $res;
    } else {
        echo $res;
    }
}

function getMonths (){
    return [
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь'
    ];
}
