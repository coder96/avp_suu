<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use \Models\Report, \Models\AbonentHistory;

    class ReportController extends BaseModel
    {
        private $report, $f1_year;
                
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_BUHGALTER);
            $this->report = Report::newInstance();
            $this->f1_year = F1_year::newInstance();
            if (__existParam('print')){
                don_not_draw_header();
            }
            $this->doModel();
        }

        public function actionForm1()
        {
            $this->title = 'Форма №1 Сведения по учету абонентской платы';
            if (__getParam('year')){
                $this->showForm1();
                return;
            }
            __setReferer();
            __export('type', 'form1');
            $this->doView('form.php');
        }

        public function actionMirab()
        {
            if (__getParam('year')){
                $this->showReportMirab();
                return;
            }
            $this->title = 'Форма №3. Сведения по учету абонентов в разрезе мирабов';
            __setReferer();
            $this->doView('form.php');
        }

        public function actionKanal()
        {
            if (__getParam('year')){
                $this->showReportKanal();
                return;
            }
            $this->title = 'Сведения по учету абонентов в разрезе каналов. Форма №2';
            __setReferer();
            $this->doView('form.php');
        }

        public function actionOroshenii()
        {
            $this->title = 'Форма №6. Сведения по количеству орошении на урожай';
            if (__getParam('ul')){
                $this->showReportOroshenii();
                return;
            }
            __export('type', 'oroshenii');
            __setReferer();
            $this->doView('form.php');
        }

        public function actionHistory()
        {
            if (__getParam('ls')){
                $this->showOrosheniyeHistory(false);
                $this->showAbonentHistory();
                return;
            }
            $this->title = 'История абонента';
            __setReferer();
            __export('type', 'history');
            $this->doView('form.php');
        }

        public function actionOrosheniye()
        {
            if (__getParam('ls')){
                $this->showOrosheniyeHistory();
                return;
            }
            $this->title = 'История орошении абонента';
            __setReferer();
            __export('type', 'history');
            $this->doView('form.php');
        }

        public function actionIzvesheniye()
        {
            $this->title = 'Печать извещении';
            if (Params::existParam('ul') || Params::existParam('ls')){
                don_not_draw_header(false);
                $year = date('Y');
                $data = $this->f1_year->getDataForIzvesheniye(
                    $year,
                    Params::getParam('ul', false, false, false),
                    Params::getParam('ls', false, false, false)
                );
                __export('result', $data);
                __export('year', $year);
                $this->doView('print_izvesheniye.php');
            } else {
                $this->doView('form_izv.php');
            }
        }

        public function actionCloseReport()
        {
            check_rule(RULE_GLAVNIY);
            $this->title = 'Закрытие отчета за ' . don_current_report_date() ;
            __export()->header = 'Закрытие отчета';
            $this->doView('confirm.php');
        }

        public function postCloseReport()
        {
            check_rule(RULE_GLAVNIY);
            if (__existParam('sbt') && __getParam('sbt') == 'Подтвердить') {
                try {
                    $res = $this->report->createReport();
                    if($res) {
                        osc_add_flash_ok_message('Отчет успешно закрыт');
                    } else {
                        $current_month = monthName(date('m'), false, true);
                        osc_add_flash_error_message("Ошибка! Отчет за $current_month уже закрыт");
                    }
                } catch (Exception $ex) {
                    don_handle_exception($ex);
                }
            }
            $this->redirectTo(osc_base_url());
        }

        //hopefully generic...
        function doView($file,$from = 'report')
        {
            $this->exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path($from.'/'.$file);
            osc_current_web_theme_path('footer.php');
            Session::newInstance()->_clearVariables();
        }

        private function getReportFromParam()
        {
            $year = __getParam('year');

            $otchet = Report::newInstance()->findByYear("$year");
            if (!$otchet){
                return false;
            }
            return $otchet;
        }

        private function getReportDate($report)
        {
            return monthName($report['month'], false, true) . '/' . $report['year'].'-г.';
        }

        private function showOrosheniyeHistory($doView = true)
        {
            $fromYear = __getParam('from_year');
            $toYear = __getParam('to_year');
//            $fromMonth = __getParam('from_month');
//            $toMonth = __getParam('to_month');

            $this->title = 'История орошении абонента';

            $fromId = Report::newInstance()->findByYear("$fromYear")['id'];
            $toId = Report::newInstance()->findByYear("$toYear")['id'];

            $ls = __getParam('ls');
            try {
                $abonent = \Models\Abonent::newInstance()->findByPrimaryKey($ls);
                __export('abonent', $abonent);

                $hist = \Models\View\Orosheniye::newInstance()->history($ls, (int)$fromId, $toId);
                __export('orosheniye', $hist);

                $ekinlar = \Models\Abonent_ekin::newInstance()->getHistory($ls, $fromId, $toId);
                __export('ekinlar', $ekinlar);

                $abonentHistory = AbonentHistory::newInstance()->get($ls, $fromId, $toId);
                __export('abonentHistory', $abonentHistory);
            } catch (Exception $ex) {
                __export('drawHeader', true);
                don_handle_exception($ex);
            }
            __export('type','orosheniye');
            if ($doView) {
                $this->doView('history.php');
            }
        }

        private function showForm1()
        {
            $id = $this->getReportFromParam()['id'];

            //вывод отчета
            $f1 = $this->f1_year->listByf1($id);
            //history - для вывода
            __export('history', $f1);
            $this->doView('otchet.php');
        }


        private function showReportMirab()
        {
            $report = $this->getReportFromParam();

            $reportData = $this->f1_year->reportGroup($report['id'],'mirab');
            $this->title = 'Сведения по учету абонентов в разрезе мирабов за '.$this->getReportDate($report).' Форма №3';
            __export('report', $report);
            __export('reportData', $reportData);

            $this->doView('mirab.php');
        }


        private function showReportKanal()
        {
            $report = $this->getReportFromParam();

            $this->title = 'Сведения по учету абонентов в разрезе каналов за '.$this->getReportDate($report).' Форма №2';

            $reportData = $this->f1_year->reportGroup($report['id'],'kanal');
            __export('reportData', $reportData);

            $this->doView('kanal.php');
        }

        private function showAbonentHistory()
        {
            $fromYear = __getParam('from_year');
            $toYear = __getParam('to_year');
//            $fromMonth = __getParam('from_month');
//            $toMonth = __getParam('to_month');

            $this->title = 'История абонента';

            $fromId = Report::newInstance()->findByYear($fromYear)['id'];
            $toId = Report::newInstance()->findByYear($toYear)['id'];

            $ls = __getParam('ls');
            try {
                $hist = $this->f1_year->abonentHistory($ls, $fromId, $toId);
                $payments = Payment::newInstance()->abonentHistory($ls, $fromId, $toId);
                __export('payments', $payments);
                __export('history', $hist);
            } catch (Exception $ex) {
                __export('drawHeader', true);
                don_handle_exception($ex);
            }
            __export('type','all');

            $this->doView('history.php');
        }

        private function showReportOroshenii()
        {
            $year = __getParam('year');
            $ul = __getParam('ul');

            //вывод отчета
            $result = $this->f1_year->oroshenii($year);
            __export('reportData', $result);
            $this->doView('oroshenii.php');
        }
    }
