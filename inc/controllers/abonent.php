<?php

use Models\Abonent;


if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class AbonentController extends BaseModel
    {
        private $manager;
                
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_KONTROLLER);
            
            $this->manager = new AbonentActions();
            $this->title = 'Абоненты';
            __get()->header = 'Абоненты <a href="'.don_url_abonent_add().'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i></a>';
            $this->doModel();
        }

        public function actionIndex()
        {
            __setReferer();
            $this->doView('index.php');
        }

        public function actionAdd()
        {
            __get()->header = $this->title = 'Добавление абонента';
            $this->doView('addnew.php');
        }

        public function postAdd()
        {
            $this->manager->prepareData();
            $message = $insert = '';
            try {
                $insert = $this->manager->insert();
            } catch (Exception $ex){
                don_handle_exception($ex);
            }
            if (__existParam('ajax')){
                echo $insert ? '1' : 'Ошибка абонент не добавлен';
                exit();
            }
            if ($insert){
                osc_add_flash_ok_message('Абонент успешно добавлен. <a href="'.don_url_abonent().'dogovor/'.$this->manager->abonentId.'" target="_blank">Распечатать договор</a>');
            }
//            else {
//                osc_add_flash_error_message('Ошибка абонент не добавлен '.(OSC_DEBUG ? $message : ''));
//            }
            $this->redirectTo(don_url_abonent_add());
        }

        public function actionEdit()
        {
            __get()->header = $this->title = 'Изменение данных абонента';
            $this->form_action = 'edit';
            $ls = Params::getParam('id');
            try {
                $abonent = Abonent::newInstance()->findByPrimaryKey($ls);
                $ekin = \Models\Abonent_ekin::newInstance()->findByLs($ls);

                $this->_export('abonent', $abonent);
                $this->_export('ekin', $ekin);
                $this->_export('sb', 'Обновить');
                $this->_export('action', 'edit_post');
                $this->_export('title', 'Обновление данных абонента');
                $this->doView('addnew.php');
            } catch (Exception $ex){
                osc_add_flash_error_message($ex->getMessage());
                $this->redirectTo(don_url_abonent());
            }
        }

        public function postEdit()
        {
            __setParam('updated_at', date('Y-m-d H:i:s'));
            $this->manager->prepareData();
            try {
                $result = $this->manager->edit();
                if (__existParam('ajax')){
                    echo $result ? '1' : 'Ошибка данные абонента не обновлены';
                    exit();
                }
                if ($result){
                    osc_add_flash_ok_message('Данные абонента успешно обновлены');
                } else {
                    osc_add_flash_error_message('Ошибка данные абонента не обновлены!');
                    $this->redirectTo(don_url_abonent_edit($this->manager->abonentId));
                }
                if (__getReferer() != ''){
                    $this->redirectTo(__getReferer());
                }
                $this->redirectTo(don_url_abonent_add());
            } catch (Exception $ex){
                osc_add_flash_error_message('Ошибка данные абонента не обновлены! '.$ex->getMessage());
                $this->redirectTo(don_url_abonent_edit($this->manager->abonentId));
            }
        }

        public function actionDogovor()
        {
            $id = __getParam('id');
            try {
                $abon = Abonent::newInstance()->findByPrimaryKey($id);
                __export('abonent', $abon);
                don_not_draw_header();
            } catch (Exception $ex){
                don_handle_exception($ex);
            }
            $this->doView('dogovor.php');
        }


        //hopefully generic...
        function doView($file)
        {
            $this->exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('abonent/'.$file);
            osc_current_web_theme_path('footer.php');
//            Session::newInstance()->_clearVariables();
        }
    }
