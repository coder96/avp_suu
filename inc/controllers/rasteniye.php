<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class rasteniyeController extends BaseModel
    {
        private $manager;
                
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_GLAVNIY);
            $this->manager = new \Models\Ekin();
            $this->title = 'Растения';
            __get()->header = 'Растения <a href="'.don_url_rasteniye_add().'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i></a>';
            $this->doModel();
        }

        public function actionIndex()
        {
            __setReferer();
            $this->doView('index.php');
        }

        public function actionAdd()
        {
            $this->doView('addnew.php');
        }

        public function postAdd()
        {
            Params::unsetParam('id');
            $insert = $this->manager->insert();
            if ($insert){
                osc_add_flash_ok_message('Растение успешно добавлено');
            } else {
                osc_add_flash_error_message('Ошибка растение не добавлен. '.(OSC_DEBUG ? $this->manager->getErrorDesc() : ''));
            }
            $this->redirectTo(don_url_rasteniye());
        }

        public function actionEdit()
        {
            $id = __getParam('id');
            $rasteniye = null;
            try {
                if (is_numeric($id)){
                    $rasteniye = $this->manager->findByPrimaryKey($id);
                }
                __export('rasteniye', $rasteniye);
                $this->form_action = don_url_tariff().'edit_post';
                $this->title = 'Редактирование растания';
                $this->doView('addnew.php');
            } catch (Exception $ex){
                osc_add_flash_error_message('Растение не найдено');
                if (__getReferer() != ''){
                    $this->redirectTo(__getReferer());
                }
                $this->redirectTo(don_url_rasteniye_add());
            }

        }

        public function postEdit()
        {
            $update = $this->manager->updateByPrimaryKey();
            if ($update || $update === 0){
                osc_add_flash_ok_message('Растение успешно обновлено');
            } else {
                osc_add_flash_error_message('Ошибка растение не обновлено');
            }
            if (__getReferer() != ''){
                $this->redirectTo(__getReferer());
            }
            $this->redirectTo(don_url_rasteniye_add());
        }

        //hopefully generic...
        function doView($file)
        {
            parent::exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('rasteniye/'.$file);
            osc_current_web_theme_path('footer.php');
//            Session::newInstance()->_clearVariables();
        }
    }
