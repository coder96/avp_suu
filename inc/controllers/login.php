<?php
if (!defined('ABS_PATH'))
    exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class loginController extends BaseModel {

    function __construct() {
        parent::__construct(FALSE);
        $this->doModel();
    }

    public function actionIndex()
    {
        __setReferer();
        don_not_draw_header();
        $this->title = 'Авторизация';
        $this->doView('/login.php');
    }

    public function postIndex()
    {
        don_not_draw_header();
        osc_csrf_check();
        $password = don_get_md5_password();
        $errorsCount = (int)__getSess('wrongPassCount');
        $tryAt = __getSess('tryAgainAfter');
        $access = true;

        if ($errorsCount >= 5 && $tryAt > date('Y-m-d H:i:s')){
            $access = false;
        }
        if ($access){
            if (dbContainUser(__getParam('name'), $password)){
                $log = new \Skeletion\Users_logSkeletion();
                $log->setDate(date('Y-m-d H:i:s'))
                    ->setIp($_SERVER['REMOTE_ADDR'])
                    ->setUserId($_SESSION['logged_user_id'])
                    ->setUserAgent($_SERVER['HTTP_USER_AGENT'])
                    ->insert();
                User::newInstance()->setSecretCookie($_SESSION['logged_user_id']);
                $this->redirectTo(osc_base_url());
            }
        }
        $msg = 'Неправильный логин или пароль!';
        if ($tryAt < date('Y-m-d H:i:s')){
            $errorsCount++;
            switch ($errorsCount){
                case 5:
                    $tryAt = date('Y-m-d H:i:s',strtotime('+3 minute'));
                    break;
                case 10:
                    $tryAt = date('Y-m-d H:i:s',strtotime('+10 minute'));
                    break;
                case ($errorsCount > 10):
                    $tryAt = date('Y-m-d H:i:s',strtotime('+1 hour'));
                    break;
            }
            __setSess('wrongPassCount',$errorsCount);
            __setSess('tryAgainAfter',$tryAt);
        } else {
            $time = strtotime($tryAt) - strtotime(date('Y-m-d H:i:s'));
            $str = '';
            if ($time > 3000){
                $str .= '1 часа';
            } else {
                $min = floor($time / 60);
                $str .= ($min > 0 ? "$min минут ": '')
                    . ($time % 60) . ' секунд ';
            }
            $msg = 'Вы много раз ввели неверный пароль. Попробуйте через '.$str;
            __export('wait',$msg);
        }
        osc_add_flash_error_message($msg);
        $this->doView('/login.php');
    }

    public function actionLogout()
    {
        unset($_SESSION['loggedIn']);
        unset($_SESSION['name']);
        unset($_SESSION['password']);
        unset($_SESSION['db_name']);
        unset($_SESSION['db_user']);
        unset($_SESSION['user_rules']);
        User::newInstance()->dropCookie();
        Session::newInstance();
        $this->redirectTo(don_url_login());
    }


    //hopefully generic...
    function doView($file) {
        parent::exportView();
//        osc_current_web_theme_path('header.php');
        osc_current_web_theme_path($file);
//        osc_current_web_theme_path('footer.php');
//        Session::newInstance()->_clearVariables();
    }

}
