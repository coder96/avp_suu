<?php

use Models\Abonent;
use Models\Orosheniye;
use Skeletion\OrosheniyeSkeletion;
use Models\View\Orosheniye as ViewOrosheniye;

if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class OrosheniyeController extends BaseModel
    {
        /**
         * @var Abonent
         */
        private $manager;

        function __construct()
        {
            parent::__construct();
            $this->manager = Abonent::newInstance();
            check_rule(RULE_OROSHENIYE);

            $this->title = 'Орошение';
            $this->doModel();
        }

        public function actionIndex()
        {
            __setReferer();
            $this->doView('index.php');
        }

        public function postIndex()
        {
            $ls = __getParam('ls');
            $abonent_ekinId = __getParam('posev');
            $sutka = __getParam('sutka');
            $date = __getParam('date');
            $data = Orosheniye::newInstance()->calculate($abonent_ekinId, $sutka, $date);
            try {
                if (!$data){
                    throw new Exception('Неправильные данные');
                }
                $oroshenie = OrosheniyeSkeletion::build();
                if (!$oroshenie->setLs($data['lit_s'])
                    ->setDate($date)
                    ->setReportId(don_current_report_id())
                    ->setAbonentEkinId($abonent_ekinId)
                    ->setTariffPrice($data['tariff'])
                    ->setSumma($data['summa'])
                    ->insert()){
                    throw new Exception('Не удалось добавить орошение ' . (OSC_DEBUG ? Orosheniye::newInstance()->getErrorDesc() : ''), ERR_C_MYSQL);
                }
                F1_year::newInstance()->update($ls, $data);
                osc_add_flash_ok_message('Орошение успешно добавлено');
            } catch (Exception $ex){
                osc_add_flash_error_message($ex->getMessage());
            }
            $this->redirectTo(don_url_orosheniye());
        }

        public function actionCancel()
        {
            $id = __getParam('id');
            try {
                $orosheniye = ViewOrosheniye::newInstance()->findByPrimaryKey($id);
                $data = [
                    'lit_s' => $orosheniye->getLs() * -1,
                    'summa' => $orosheniye->getSumma() * -1
                ];
                if (F1_year::newInstance()->update($orosheniye->getAbonentLs(), $data) !== false){
                    osc_add_flash_ok_message('Орошение успешно отменено');
                    Orosheniye::newInstance()->deleteByPrimaryKey($id);
                } else {
                    osc_add_flash_error_message('Ошибка при отмене');
                }
            } catch (Exception $ex){
                don_handle_exception($ex);
            }
            if (__getReferer()){
                $this->redirectTo(__getReferer());
            } else {
                $this->redirectTo(don_url_orosheniye());
            }
        }

        //hopefully generic...
        function doView($file)
        {
            $this->exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('orosheniye/'.$file);
            osc_current_web_theme_path('footer.php');
//            Session::newInstance()->_clearVariables();
        }
    }
