<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class paymentController extends BaseModel
    {
        private $manager;
        
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_KASSIR);
            $this->title = 'Оплаты';
            $this->manager = new Payment();
            $this->doModel();
        }


        public function actionIndex()
        {
            __setReferer('', TRUE);
            if (Session::newInstance()->_get('np') != ''){
                $np = Session::newInstance()->_get('np');
            } else {
                $np = $this->manager->getLastNP();
            }
            $last_payments = $this->manager->getLastPayments($np);
            $date = Session::newInstance()->_get('date') == '' ? date('Y-m-d') : Session::newInstance()->_get('date');
            $date .= date('\TH:i:s');
            $this->title .= ', добавить';

            __export('np', $np);
            __export('date', $date);
            __export('last_payments', $last_payments);

            $this->doView('add.php');
        }

        public function postIndex($korrek = false)
        {
            if (__getParam('date') !== ''){
                $date = date('Y-m-d', strtotime(__getParam('date')));
                Session::newInstance()->_set('date', $date);
            }
            if ($korrek){
                $type = 'korrekt';
                $url = don_url_payment().'korrekt';
                $message_ok = 'Корректировка добалена';
                $message_error = 'Ошибка корректировка не добавлена!';
            } else {
                $type = 'pay';
                $url = don_url_payment();
                $message_ok = 'Оплата добавлена!';
                $message_error = 'Ошибка оплата не добавлена!';
            }
            try {
                $res = $this->manager->insertPay(null,$type);
                if ($res == '-88'){
                    __setSess('ls', __getParam('ls'));
                    __setSess('summa', __getParam('summa'));
                    osc_add_flash_error_message('Оплата с такой суммой за указанный день уже проведена. Попробуйте изменить сумму или дату.');
                } elseif ($res){
                    Session::newInstance()->_set('np', __getParam('numPachki'));
                    osc_add_flash_ok_message($message_ok);
                    __setSess('ls', '');
                    __setSess('summa', '');
                } else {
                    __setSess('ls', __getParam('ls'));
                    __setSess('summa', __getParam('summa'));
                    osc_add_flash_error_message($message_error);
                }
            } catch (Exception $ex){
                don_handle_exception($ex);
            }
            $this->redirectTo($url);
        }

        public function actionReestr()
        {
            if (__getParam('year')){
                $this->showReestr();
                return;
            }
            $this->title = 'Реестр оплат';

            $this->doView('index.php');
        }

        private function showReestr()
        {
            $month = __getParam('month');
            $year = __getParam('year');
            $svodniy = __getParam('svodniy');

            if (__existParam('print')){
                don_not_draw_header();
            }

            $otchet = \Models\Report::newInstance()->findByYear($year);
            $id = $otchet['id'];

            try {
                $reestr = $this->manager->listByF1($id,"$year-$month-01","$year-$month-31 23:59:59", $svodniy);
                __setReferer('', TRUE);
                __export('show_cancel', $otchet['id'] == don_current_report_id());
                __export('reestr', $reestr);

                $aUsers = [];
                if (!$svodniy && user_has_rule(RULE_GLAVNIY)){
                    $User = new User();
                    $users = $User->listWhere('fk_i_parent_id', get_current_user_id());
                    foreach ($users as $u) {
                        $aUsers[$u['pk_i_id']] = $u;
                    }
                }
                __export('users', $aUsers);
                if ($svodniy){
                    $this->doView('reestrSvodn.php');
                } else {
                    $this->doView('reestr.php');
                }
            } catch (Exception $ex) {
                don_handle_exception($ex);
                $this->redirectTo(don_url_reestr());
            }
        }

        public function actionKorrekt()
        {
            check_rule(RULE_GLAVNIY);
            $date = Session::newInstance()->_get('date') == '' ? date('Y-m-d') : Session::newInstance()->_get('date');
            $date .= date('\TH:i:s');
            __export('date', $date);
            $this->title = 'Корректировка';
            $this->doView('korrekt.php');
        }

        public function postKorrekt()
        {
            check_rule(RULE_GLAVNIY);
            if (__getParam('date') == ''){
                __setParam('date', date('Y-m-d H:i:s'));
            }
            $this->postIndex(true);
        }


        public function actionAddBalance2()
        {
            $this->doView('add_balance_2.php');
        }

        public function postAddBalance2()
        {
            $summa_balance_2 = __getParam('summa_balance_2');
            $ls = __getParam('ls');
            $balance_2_name = don_get_preference('balance_2_name');

            if (!is_numeric($ls) || $ls < 1 || !is_numeric($summa_balance_2)){
                $ok = 0;
            } else {
                $abonent = new Abonent();
                $ok = $abonent->dao->query("UPDATE abonent SET balance_2 = balance_2 + $summa_balance_2 WHERE ls = $ls");
            }

            if ($ok){
                osc_add_flash_ok_message("\"$balance_2_name\" успешно обновлен!");
            } else {
                osc_add_flash_error_message("Ошибка! \"$balance_2_name\" не обновлено");
            }
            $this->redirectTo(don_url_payment().'/add_balance_2');
        }

        public function actionCheck()
        {
            $paymentId = __getParam('id');
            $payment = Payment::newInstance()->findByPrimaryKey($paymentId);
            __export('payment', $payment);
            $this->doView('check.php', 'payment',false);
        }

        public function actionCancel()
        {
            $id = __getParam('id');
            try {
                $this->manager->cancel($id);
                osc_add_flash_ok_message("Оплата успешно отменен!");
            } catch (Exception $exc) {
                $m = $exc->getMessage();
                osc_add_flash_error_message("Ошибка! Оплата не отменен по причине: " . $m);
            }
            $referer = __getReferer() == '' ? don_url_reestr() :__getReferer();
            $this->redirectTo($referer);
        }


        //hopefully generic...
        function doView($file,$folder = 'payment', $drawHeader = true)
        {
            parent::exportView();
            if ($drawHeader){
                osc_current_web_theme_path('header.php');
            }
            osc_current_web_theme_path($folder.'/'.$file);
            if ($drawHeader){
                osc_current_web_theme_path('footer.php');
            }
        }
    }
