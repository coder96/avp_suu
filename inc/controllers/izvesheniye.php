<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class izvesheniyeController extends BaseModel
    {
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_BUHGALTER);
            $this->doModel();
        }

        //Business Layer...
        function doModel()
        {
            switch( $this->action ) {
                case 'empty':
                    $_GET['empty'] = 1;
                case 'street':
                case 'abonent':
                    require osc_base_path().'inc/db.inc.php';
                    require osc_base_path().'izveshenie/index.php';
                    break;
                case 'kvitantsiya':
                    $this->doFile('kvitantsiya.php');
                    break;
                default:
                    $this->title = 'Печать извещений';
                    $this->form_action = '/izvesheniye/';

                    $this->doView('form.php');
                    break;
            }
        }

        //hopefully generic...
        function doView($file)
        {
            $this->exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('izvesheniye/'.$file);
            osc_current_web_theme_path('footer.php');
            Session::newInstance()->_clearVariables();
        }

        //hopefully generic...
        function doFile($file)
        {
            osc_current_web_theme_path('izvesheniye/'.$file);
            Session::newInstance()->_clearVariables();
        }
    }
