<?php

use Models\Abonent;


if (!defined('ABS_PATH'))
    exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class ajaxController extends BaseModel
{
    function __construct()
    {
        parent::__construct(FALSE);
        $this->ajax = true;
        don_json_header();
        $this->doModel();
    }

    public function actionIndex()
    {
        echo json_encode([]);
    }

    public function actionGetLsInfo()
    {
        try {
            $abonent = Abonent::newInstance()->findByPrimaryKey();
            if ($abonent) {
                echo json_encode($abonent->toArray());
                return;
            }
        } catch (Exception $ex) {
        }
        echo json_encode([]);
    }

    public function actionGetAbonent()
    {
        $res = Abonent::newInstance()->listWhere()->getAll();
        foreach ($res as &$row) {
            $row['updated_at'] = $row['updated_at'] ? date('d.m.Y H:i', strtotime($row['updated_at'])) : '';
        }
        echo json_encode($res);
    }

    public function postGetOroshObyom()
    {
        $abonent_ekinId = __getParam('ab_ekin');
        $sutka = __getParam('sutka');
        $date = __getParam('date');
        $data = \Models\Orosheniye::newInstance()->calculate($abonent_ekinId, $sutka, $date);
        if ($data){
            echo json_encode($data);
        } else {
            echo json_encode([]);
        }
    }

    public function actionGetAbonentEkin()
    {
        $ls = __getParam('ls');
        $ekin = \Models\Abonent_ekin::newInstance()->findByLs($ls)->getAll();
        echo json_encode($ekin);
    }

    public function actionAbonentAutocomplete()
    {
        $term = __getParam('term');
        if (isset($_GET['equals'])){
            $sql = "SELECT * FROM abonent WHERE ls = '$term'";
            $label = 'ls';
        } elseif (is_numeric($term)){
            $sql = "SELECT * FROM abonent WHERE ls like '$term%'";
            $label = 'ls';
        } else {
            $term = preg_replace('/[\'"`#\-]/', '', $term);
            $sql = "SELECT * FROM abonent WHERE fio like '$term%'";
            $label = 'fio';
        }
        if (!Params::existParam('unlimit')){
            $sql .= ' limit 10';
        }
        $r = sql_in($sql);
        $abonents = [];
        foreach ($r as $i=>$v) {
            $abonents[$i]['label'] = $v[$label];
            $abonents[$i]['fio'] = $v['fio'];
            $abonents[$i]['ls'] = $v['ls'];
            $abonents[$i]['balance'] = $v['balance'];
            $abonents[$i]['balance_2'] = $v['balance_2'];
            $abonents[$i]['i_counter'] = $v['i_counter'];
        }
        if (!empty($abonents)){
            echo json_encode($abonents);
        }
    }

    //hopefully generic...
    function doView($file)
    {
        parent::exportView();
//        osc_current_web_theme_path('header.php');
        osc_current_web_theme_path($file);
//        osc_current_web_theme_path('footer.php');
//        Session::newInstance()->_clearVariables();
    }

}
