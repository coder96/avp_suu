<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class userController extends BaseModel
    {
        private $manager;
        private $payment;
        
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_PROGRAMMIST);
            $this->manager = User::newInstance();
            $this->payment = new UserPayment();
            $this->title = 'Пользователи';
            $this->doModel();
        }

        //Business Layer...
        function doModel()
        {
            switch( $this->action ) {
                case 'add_pay_post' :
                    $date = date('Y-m-d H:i:s');
                    $id = __getParam('id');
                    $summ = __getParam('summa');
                    
                    try {
                        $this->payment->insert($id, $summ, $date);
                        osc_add_flash_ok_message('Оплата произведена');
                    } catch (Exception $ex){
                        osc_add_flash_error_message($ex->getMessage());
                    }
                    $this->redirectTo(don_url_user_add_pay());
                    break;
                case 'add_pay' :
                    $clients = $this->manager->listAll(RULE_GLAVNIY);
                    __export('clients', $clients);
                    $this->title = 'Добавить оплату';
                    $this->doView('add.php');
                default:
                    break;
            }
        }

        //hopefully generic...
        function doView($file,$folder = 'user')
        {
            parent::exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path($folder.'/'.$file);
            osc_current_web_theme_path('footer.php');
        }
    }
