<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class usersController extends BaseModel
    {
        private $manager;
        private $payment;
        
        function __construct()
        {
            parent::__construct();
            if (!user_has_rule(RULE_GLAVNIY)){
                don_do_403();
            }
            __setReferer();
            $this->manager = new User();
            $this->title = 'Персонал';
            $this->doModel();
        }

        //Business Layer...
        function doModel()
        {
            switch( $this->action ) {
                case 'add':
                    $this->doView('add.php');
                    
                    break;
                case 'add_post':
                    __saveFrom();
                    $res = $this->manager->insert();
                    
                    if ($res){
                        osc_add_flash_ok_message('Пользователь успешно добавлен!');
                        Session::newInstance()->_clearVariables();
                    } else {
                        osc_add_flash_error_message('Ошибка пользователь не добавлен!');
                    }
                    
                    osc_redirect_to('/users/add');
                    break;
                case 'edit':
                    $userId = __getParam('id');
                    
                    $user = $this->manager->findByPrimaryKey($userId);
                    if ($user === false){                    
                        osc_add_flash_error_message('Нет такого пользователя');
                        osc_redirect_to('/users');
                    }
                    
                    __export('user', $user);
                    $this->doView('add.php');
                    break;
                case 'edit_post':
                    __saveFrom();
                    $res = $this->manager->update();
                    
                    if ($res){
                        osc_add_flash_ok_message('Данные успешно обновлены!');
                        Session::newInstance()->_clearVariables();
                    } else {
                        osc_add_flash_error_message('Ошибка!');
                    }
                    osc_redirect_to('/users/edit/'. __getParam('pk_i_id'));
                    break;
                case 'add_pay_post' :
                    $date = date('Y-m-d H:i:s');
                    $id = __getParam('id');
                    $summ = __getParam('summa');
                    
                    $payment = new ClientUserPayment();
                    try {
                        $payment->insert($id, $summ, $date);
                        osc_add_flash_ok_message('Оплата произведена');
                    } catch (Exception $ex){
                        echo 'error';
                        osc_add_flash_error_message($ex->getMessage());
                    }
                    $this->redirectTo('/users/add_pay');
                    break;
                case 'add_pay' :
                    $clients = $this->manager->listAll(RULE_KASSIR);
                    __export('clients', $clients);
                    
                    $this->form_action = '/users/add_pay';
                    $this->title = 'Добавить оплату';
                    $this->doView('add.php','user');
                    break;
                default:
                    __setReferer();
                    $this->doView('main.php');
                    break;
            }
        }

        //hopefully generic...
        function doView($file,$folder = 'users')
        {
            parent::exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path($folder.'/'.$file);
            osc_current_web_theme_path('footer.php');
        }
    }
