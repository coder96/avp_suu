<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class mainController extends BaseModel
{
    function __construct()
    {
        parent::__construct(FALSE);

        $this->doModel();
    }

    public function actionIndex()
    {
        __dropReferer();
        if (!don_user_logged_in()){
            $this->redirectTo('/login');
        }
        $this->title = 'Главная страница';
        $this->doView('menu.php');
    }

    //hopefully generic...
    function doView($file)
    {
        parent::exportView();
        osc_current_web_theme_path('header.php');
        osc_current_web_theme_path($file);
        osc_current_web_theme_path('footer.php');
        Session::newInstance()->_clearVariables();
    }
}
