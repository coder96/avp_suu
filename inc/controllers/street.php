<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class streetController extends BaseModel
    {
        private $manager;
                
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_GLAVNIY);
            $this->manager = new Street();
            $this->title = 'Участки';
            __get()->header = 'Каналы <a href="'.don_url_street_add().'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i></a>';
            $this->doModel();
        }

        public function actionIndex()
        {
            __setReferer();
            $this->doView('index.php');
        }

        public function actionAdd()
        {
            $this->form_action = don_url_street().'add_post';
            $this->title = 'Добавление улицы';
            $this->doView('addnew.php');
        }

        public function postAdd()
        {
            $insert = $this->manager->insert();
            if ($insert){
                osc_add_flash_ok_message('Улица успешно добавлена');
            } else {
                osc_add_flash_error_message('Ошибка улица не добавлена');
            }
            $this->redirectTo(don_url_street());
        }

        public function actionEdit()
        {
            $id = __getParam('id');
            $street = null;
            if (is_numeric($id)){
                $street = $this->manager->findByPrimaryKey($id);
            }
            if ($street == null || empty($street)){
                osc_add_flash_error_message('Улица не найдено');
                if (__getReferer() != ''){
                    $this->redirectTo(__getReferer());
                }
                $this->redirectTo(don_url_street_add());
            } else {
                __export('street', $street);
                $this->form_action = don_url_street().'edit_post';
                $this->title = 'Редактирование улицы';
                $this->doView('addnew.php');
            }
        }

        public function postEdit()
        {
            $update = $this->manager->updateByPrimaryKey();
            if ($update || $update === 0){
                osc_add_flash_ok_message('Улица успешно обновлена');
            } else {
                osc_add_flash_error_message('Ошибка улица не обновлена');
            }
            if (__getReferer() != ''){
                $this->redirectTo(__getReferer());
            }
            $this->redirectTo(don_url_street_add());
        }


        //hopefully generic...
        function doView($file)
        {
            parent::exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('street/'.$file);
            osc_current_web_theme_path('footer.php');
//            Session::newInstance()->_clearVariables();
        }
    }
