<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');


class settingController extends BaseModel
{
    function __construct()
    {
        parent::__construct();
        check_rule(RULE_SETTINGS);
        $this->title = 'Настройки';
        __get()->header = 'Настройки системы';
        $this->doModel();
    }

    public function actionIndex()
    {
        __setReferer();
        $this->doView('index.php');
    }


    public function postIndex()
    {
        Params::unsetParam('sb');
        $_params = Params::getParamsAsArray('post');
        foreach ($_params as $key => $value) {
            don_set_preference($key,$value);
        }
        osc_add_flash_ok_message('Настройки сохранены');
        $this->redirectTo(don_url_setting());
    }
    //hopefully generic...
    function doView($file)
    {
        parent::exportView();
        osc_current_web_theme_path('header.php');
        osc_current_web_theme_path('setting/'.$file);
        osc_current_web_theme_path('footer.php');
//            Session::newInstance()->_clearVariables();
    }
}
