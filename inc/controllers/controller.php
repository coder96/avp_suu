<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    class controllerController extends BaseModel
    {
        private $manager;
                
        function __construct()
        {
            parent::__construct();
            check_rule(RULE_GLAVNIY);
            $this->manager = new Controller();
            $this->title = 'Мирабы';
            __get()->header = 'Мирабы <a href="'.don_url_controller_add().'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-plus"></i></a>';
            $this->doModel();
        }

        public function actionIndex()
        {
            __setReferer();
            $this->doView('index.php');
        }

        public function actionAdd()
        {
            $this->form_action = don_url_controller().'add_post';
            $this->title = 'Добавление мираба';
            $this->doView('addnew.php');
        }

        public function postAdd()
        {
            $insert = $this->manager->insert();
            if ($insert){
                osc_add_flash_ok_message('Мираб успешно добавлен');
            } else {
                osc_add_flash_error_message('Ошибка мираб не добавлен');
            }
            $this->redirectTo(don_url_controller());
        }

        public function actionEdit()
        {
            $id = __getParam('id');
            $contorller = null;
            if (is_numeric($id)){
                $contorller = $this->manager->findByPrimaryKey($id);
            }
            if ($contorller == null || empty($contorller)){
                osc_add_flash_error_message('Мираб не найден');
                if (__getReferer() != ''){
                    $this->redirectTo(__getReferer());
                }
                $this->redirectTo(don_url_controller_add());
            } else {
                __export('contorller', $contorller);
                $this->form_action = don_url_controller().'edit_post';
                $this->title = 'Редактирование данных мираба';
                $this->doView('addnew.php');
            }
        }

        public function postEdit()
        {
            $update = $this->manager->updateByPrimaryKey();
            if ($update || $update === 0){
                osc_add_flash_ok_message('Данные успешно обновлены');
            } else {
                osc_add_flash_error_message('Ошибка Данные не обновлены');
            }
            if (__getReferer() != ''){
                $this->redirectTo(__getReferer());
            }
            $this->redirectTo(don_url_controller_add());
        }

        //hopefully generic...
        function doView($file)
        {
            parent::exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('controller/'.$file);
            osc_current_web_theme_path('footer.php');
        }
    }
