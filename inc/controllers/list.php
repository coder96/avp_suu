<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Class listController
 * @deprecated
 */
    class listController extends BaseModel
    {
        function __construct()
        {
            parent::__construct();
            $not_check_rule = ['users-payments','мои_оплаты'];
            if (!in_array($this->action, $not_check_rule)){
                check_rule(RULE_KONTROLLER);
            }
            $this->doModel();
        }

        //Business Layer...
        function doModel()
        {
            $ref = __getReferer();
            __setReferer();
            switch( $this->action ) {
                case  'abonents':
                    $this->title = 'Список абонентов';
                    $list = Abonent::newInstance()->listWhere();
                    __export('list', $list);
                    $this->doView('abonents.php');
                    break;
                case  'controllers':
                    check_rule(RULE_GLAVNIY);
                    $this->title = 'Список контроллеров';
                    $list = Controller::newInstance()->listAll();
                    __export('list', $list);
                    $this->doView('controllers.php');
                    break;
                case  'tariffs':
                    check_rule(RULE_GLAVNIY);
                    $this->title = 'Список Тарифов';
                    $list = Tariff::newInstance()->listAll();
                    __export('list', $list);
                    $this->doView('tariffs.php');
                    break;
                case  'streets':
                    check_rule(RULE_GLAVNIY);
                    $this->title = 'Список улиц';
                    $list = Street::newInstance()->listAll();
                    __export('list', $list);
                    $this->doView('streets.php');
                    break;
                case  'users':
                    check_rule(RULE_GLAVNIY);
                    __setReferer($ref);
                    
                    $users = new User();
                    $listUsers = $users->listAll();
                    __export('users', $listUsers);
                    $this->doView('users_list.php');
                    break;
                case  'мои_оплаты':
                case  'users-payments':
                    __setReferer($ref);
                    $kassir = !user_has_rule(RULE_GLAVNIY);
                    if (!user_has_rule(RULE_KASSIR)){
                        check_rule(RULE_GLAVNIY);
                    }
                    
                    $payments = new ClientUserPayment();
                    if ($kassir){
                        $payments->dao->orderBy('pk_i_id','DESC');
                        $list = $payments->listWhere('fk_i_user_id', get_current_user_id());
                    } else {
                        $list = $payments->listAll();
                    }
                    
                    $users = new User();
                    if ($kassir){
                        $listUsers[0] = $users->findByPrimaryKey(get_current_user_id());
                    } else {
                        $listUsers = $users->listAll(RULE_KASSIR);
                    }
                    $aUsers = [];
                    foreach ($listUsers as $u) {
                        $aUsers[$u['pk_i_id']] = $u;
                    }
                    __export('list', $list);
                    __export('users', $aUsers);
                    $this->doView('users-payments.php');
                    break;
                default :
                    $this->redirectTo('/');
                    break;
            }
        }

        //hopefully generic...
        function doView($file,$from = 'list')
        {
            $this->exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path($from.'/'.$file);
            osc_current_web_theme_path('footer.php');
            Session::newInstance()->_clearVariables();
        }
    }
