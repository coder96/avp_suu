<?php

if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

use Models\Migration;


    abstract class BaseModel
    {
        protected $page;
        protected $action;
        protected $form_action;
        protected $ajax;
        protected $time;
        protected $title;
        function __construct($check_log = true)
        {
            if ($check_log){
                if(!don_user_logged_in()){
                    $this->redirectTo(osc_base_url());
                } else {
                    Migration::newInstance()->chekVersion();
                }
            }
            // this is necessary because if HTTP_HOST doesn't have the PORT the parse_url is null
            $current_host = parse_url(Params::getServerParam('HTTP_HOST'), PHP_URL_HOST);
            if( $current_host === null ) {
                $current_host = Params::getServerParam('HTTP_HOST');
            }

            if( parse_url(osc_base_url(), PHP_URL_HOST) !== $current_host ) {
                // first check if it's http or https
                $url = 'http://';
                // append the domain
                $url .= parse_url(osc_base_url(), PHP_URL_HOST);
                // append the port number if it's necessary
                $http_port = parse_url(Params::getServerParam('HTTP_HOST'), PHP_URL_PORT);
                if( $http_port !== 80 ) {
                    $url .= ':' . parse_url(Params::getServerParam('HTTP_HOST'), PHP_URL_PORT);
                }
                // append the request
                $url .= Params::getServerParam('REQUEST_URI', false, false);
                $this->redirectTo($url);
            }

            $this->page   = Params::getParam('page');
            $action = __getParam('action') != '' ? __getParam('action') : 'index';
            $this->action = preg_replace_callback('/([\w\d]+)([^\w\d]*)/', function ($a) {return ucfirst($a[1]);},$action);;
            $this->ajax   = false;
            $this->time   = microtime(true);
        }

        function __destruct()
        {
            if( !$this->ajax && OSC_DEBUG ) {
                echo '<!-- ' . $this->getTime() . ' seg. -->';
            }
        }

        //to export variables at the business layer
        function _export($key, $value)
        {
            View::newInstance()->_exportVariableToView($key, $value);
        }

        //only for debug (deprecated, all inside View.php)
        function _view($key = null)
        {
            View::newInstance()->_view($key);
        }

        function doModel(){
            if (!empty(Params::getParamsAsArray('post'))){
                $func = 'post'.$this->action;
            } else {
                $func = 'action'.$this->action;
            }
            $methods = get_class_methods($this);
            if (in_array($func, $methods)){
                call_user_func([$this,$func]);
            } else {
                __get()->controller = 'error';
                __get()->action = 'error';
                $this->title = 'Ошибка 404';
                __get()->header = 'Извините у нас нет такой страницы';
                $this->do404();
            }
        }

        //Funciones que se tendran que reescribir en la clase que extienda de esta
        protected abstract function doView($file);
        
        protected function exportView(){
            __export()->title = $this->title;
            if (__export()->header == ''){
                __export()->header = __export()->title;
            }
            __export('title', $this->title);
            __export('form_action', $this->form_action);
        }

        function do400()
        {
//            Rewrite::newInstance()->set_location('error');
            header('HTTP/1.1 400 Bad Request');
            osc_current_web_theme_path('404.php');
            exit;
        }

        function do404()
        {
//            Rewrite::newInstance()->set_location('error');
            header('HTTP/1.1 404 Not Found');
            $this->exportView();
            osc_current_web_theme_path('header.php');
            osc_current_web_theme_path('404.php');
            osc_current_web_theme_path('footer.php');
            exit;
        }

        function do410()
        {
            //Rewrite::newInstance()->set_location('error');
            header('HTTP/1.1 410 Gone');
            osc_current_web_theme_path('404.php');
            exit;
        }

        function redirectTo($url, $code = null)
        {
            osc_redirect_to($url, $code);
        }

        function getTime()
        {
            $timeEnd = microtime(true);
            return $timeEnd - $this->time;
        }

    }

    /* file end: ./oc-includes/osclass/core/BaseModel.php */
