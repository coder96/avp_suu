<?php
$host = 'localhost';
if (strpos($_SERVER['PATH'], 'Doniyor\phpStorm') !== FALSE || strpos($_SERVER['PATH'], 'openserver') !== FALSE){
    $user = $userMain = 'root';
    $pass = $passMain = '1234';
} else {
    $userMain = $_ENV['DBUSER'];
    $passMain = $_ENV['DBPASS'];
    $user = $_ENV['DBUSER'];
    $pass = $_ENV['DBPASS'];
}
define('DB_HOST', $host);

define('DB_MAIN_NAME', $_ENV['DBNAME']);
define('DB_MAIN_USER', $userMain);
define('DB_MAIN_PASSWORD', $passMain);

define('DB_NAME', $_SESSION['db_name']);
define('DB_USER', $user);
define('DB_PASSWORD', $pass);

define('OSC_DEBUG_DB_EXPLAIN', false);
define('OSC_DEBUG_DB', false);
