<?php

namespace Frm;

use Models\Abonent_ekin;
use Models\Ekin;
use \Session;
use Skeletion\View\OrosheniyeSkeletion;

if (!defined('ABS_PATH'))
    exit('ABS_PATH is not loaded. Direct access is not allowed.');

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class FormReport {
    private static $headerDrawed = false;

    //dannie
    private static $itog, $kol_abonent, $kol_chel, $ostNaNach, $nachislTarif,$balance_2,
        $schet_summa, $penya, $oplacheno, $korrekt, $ekin1, $ekin2, $ostNaKonec;

    //oprions
    private static $show_balance_2,
        $balance_2_name, $show_dom_num, $show_phone, $b_penya;

    /**
     * @param $orosheniye OrosheniyeSkeletion
     */
    static public function draw_orosheniye($orosheniye)
    {
        ?>
        <table class="table table-bordered dataTable" style="width: auto">
            <tr>
                <th>За посев</th>
                <th>га</th>
                <th>сут</th>
                <th>лит/с</th>
                <th>цена</th>
                <th>сумма</th>
                <th>дата</th>
            </tr>
            <?php
            $sumSut = $sumLs = $sum = 0;
            while ($orosheniye->next()) {
                $abonEkin = Abonent_ekin::newInstance()->findByPrimaryKey($orosheniye->getAbonentEkinId());
                $ekin = Ekin::newInstance()->findByPrimaryKey($abonEkin->getEkinId());
                $sutka = round($orosheniye->getLs() / $orosheniye->getGa() / $ekin->getLs());
                $sumSut += $sutka;
                $sumLs += $orosheniye->getLs();
                $sum += $orosheniye->getSumma();
                ?>
                <tr>
                    <td><?= $orosheniye->getName() ?></td>
                    <td><?= $orosheniye->getGa() ?></td>
                    <td><?= $sutka ?></td>
                    <td><?= $orosheniye->getLs() ?></td>
                    <td><?= $orosheniye->getTariffPrice() ?></td>
                    <td><?= don_format_price($orosheniye->getSumma()) ?></td>
                    <td><?= date('d.m.Y',strtotime($orosheniye->getDate())); ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <th colspan="2">Итого:</th>
                <th><?=$sumSut?></th>
                <th><?=$sumLs?></th>
                <th></th>
                <th><?=don_format_price($sum)?></th>
                <th></th>
            </tr>
        </table>
        <?php
    }

    static public function draw_f1($report, $for_abonent = false) {
        $user_id = Session::newInstance()->_get('logged_user_id');

        self::$show_balance_2   = don_get_preference('show_balance_2');
        self::$balance_2_name   = don_get_preference('balance_2_name');
        self::$show_dom_num     = don_get_preference('show_dom_num');
        self::$show_phone       = don_get_preference('show_phone');
        self::$b_penya          = don_get_preference('penya') === 'on' ? true : false;

        $ul_name = NULL;

        foreach ($report as $row) {
            $year = $row['year'];
            $month = $row['month'];
            if ($ul_name !== $row['ulname']) {
                if ($ul_name !== NULL) {
                    if (self::$show_balance_2) {
                        self::$ostNaKonec += self::$balance_2;
                    }
                    self::f1_footer($for_abonent);
                    self::setParamsToZero();
                }
                $ul_name = $row['ulname'];
                $c_name = $row['cname'];
                self::f1_header($ul_name, $c_name, $year, $month, $for_abonent);
            }
            $nach_po_tarifu = $row['nachislPoTarifu'];
            $ekin1 = don_format_price($row['ga_1']);
            $ekin2 = $row['ga_2'];
            $colspanPlosh = $ekin2 ? '' : ' colspan="2"';
            $ekin2 = don_format_price($ekin2);
            self::$kol_abonent++;
            ?>
            <tr>
                <?php if ($for_abonent) {?>
                    <td>
                        <?= $row['year'] ?>
                    </td>
                <?php } else {
                    if (self::$show_phone){
                        $phone = don_format_price($row['s_phone'],0);
                        $phone = $phone != '' ? $phone : '';
                        echo "<td class='phone'>$phone</td>";
                    }
                    ?>
                    <td>
                        <?php echo $row['ls'] ?>
                    </td>
                    <td>
                        <?= $row['updated_at'] ?>
                    </td>
                    <td class="fio">
                        <?php echo $row['fio'] ?>
                    </td>
                    <?php
                }
                ?>
                <td<?=$colspanPlosh?>>
                    <?php echo $ekin1 ?>
                </td>
                <?php
                if ($ekin2){
                    echo "<td>$ekin2</td>";
                }
                ?>
            <!--                    <td>
                <?php //echo  ?>
                </td>-->
<!--                --><?php //if (self::$show_counter) { ?>
                    <td>
                        <?= $row['i_litr'] ?>
                    </td>
<!--                --><?php //} ?>
                <td>
                    <?php echo don_format_price($row['ostNaNach']) ?>
                </td>
                <?php if (self::$show_balance_2) { ?>
                    <td>
                        <?php echo $row['balance_2'] ?>
                    </td>
                <?php } ?>
                <td>
                    <?= don_format_price($nach_po_tarifu) ?>
                </td>
                <?php if (self::$b_penya) { ?>
                    <td>
                        <?php echo $row['penya'] ?>
                    </td>
                <?php
                }
                ?>
                <td>
                    <?php echo don_format_price($row['oplacheno']) ?>
                </td>
                <td>
                    <?php echo $row['korrekt'] ?>
                </td>
                <td>
                    <?php echo self::$show_balance_2 ? don_format_price($row['ostNaKonec'] + $row['balance_2']) : don_format_price($row['ostNaKonec']) ?>
                </td>
            </tr>
            <?php
            self::$balance_2 += $row['balance_2'];
            self::$kol_chel += $row['kol'];
            self::$ostNaNach += $row['ostNaNach'];
            self::$nachislTarif += $nach_po_tarifu;
            self::$penya += $row['penya'];
            self::$oplacheno += $row['oplacheno'];
            self::$korrekt += $row['korrekt'];
            self::$ostNaKonec += $row['ostNaKonec'];
            self::$ekin1 += $ekin1;
            self::$ekin2 += $ekin2;
            self::$schet_summa += $row['i_litr'];
        }

        if (self::$show_balance_2) {
            self::$ostNaKonec += self::$balance_2;
        }
        self::f1_footer($for_abonent);
        self::setParamsToZero();

        if (!$for_abonent){
            //вывести полный итог
            self::drawItog();
        }
    }

    static private function f1_header($ul_name, $c_name, $year, $month, $for_abonent) {
        if (!$for_abonent){
            if (!self::$headerDrawed){
                self::$headerDrawed = true;
                ?>
                <h3 style="text-align: center">Форма №1<br>Сведения по учету абонентской платы<br>за <?php
                    echo $year . 'г';
                    ?>.</h3>
                <?php
            }
            ?>
            <h4><?= $ul_name ?>, мурап: <?= $c_name ?></h4>
        <?php } ?>
            <table border="1" cellspacing="0" class="report">
            <tr>
                <?php if ($for_abonent) {
                    ?>
                    <th>
                        год
                    </th>
                    <?php
                } else {
                    if (self::$show_phone){
                        echo "<th>тел.</br>№</th>";
                    }
                    ?>
                    <th>
                        Л/с
                    </th>
                    <th>
                        Дата
                    </th>
                    <th>
                        ФИО
                    </th>
                    <?php
                }
                ?>
                <th colspan="2">
                    пло-<br/>щадь<br/>га
                </th>
<!--                --><?php //if (self::$show_counter) { ?>
                    <th>
                        литр/с
                    </th>
<!--                --><?php //} ?>
                <th>
                    Остаток<br>на<br>начало<br>месяца
                </th>
                <?php if (self::$show_balance_2) { ?>
                    <th>
                        <?= self::$balance_2_name ?>
                    </th>
                <?php } ?>
                <th>
                    Начисл.
<!--                    <br>по<br>тарифу-->
                </th>
                <?php if (self::$b_penya) {
                    echo "<th>Пеня</th>";
                }
                ?>
                <th>
                    Опла-<br>чено
                </th>
                <th>
                    Кор-<br>рек-<br>тиров-<br>ка
                </th>
                <th>
                    Остаток<br>на<br>конец<br>месяца
                </th>
            </tr>
            <?php
        }

        static private function f1_footer($for_abonent) {
            if (!$for_abonent) {
                ?>
            <tr>
                <th colspan="<?=(self::$show_phone) ? '3' : '2'?>">
                    <?php echo self::$kol_abonent ?>
                </th>
                <th>
                    Итого
                </th>
                <?php
                $ekin1 = self::$ekin1;
                $ekin2 = self::$ekin2;
                $colspan = $ekin2 > 0 ? '' : ' colspan="2"';
                ?>
                <th<?=$colspan?>>
                    <?=$ekin1?>
                </th>
                <?php
                if (!$colspan){
                    echo "<th>$ekin2</th>";
                }
                $count_all = self::$schet_summa;
                echo "<th>$count_all</th>";
                ?>
                <th>
                    <?php echo don_format_price(self::$ostNaNach) ?>
                </th>
                <?php if (self::$show_balance_2) { ?>
                    <th>
                        <?= don_format_price(self::$balance_2) ?>
                    </th>
                <?php } ?>
                <th>
                    <?php echo don_format_price(self::$nachislTarif) ?>
                </th>
                <?php if (self::$b_penya) {
                    $penya = self::$penya;
                    echo "<th>$penya</th>";
                }
                ?>
                <th>
                    <?php echo don_format_price(self::$oplacheno) ?>
                </th>
                <th>
                    <?php echo don_format_price(self::$korrekt) ?>
                </th>
                <th>
                    <?php echo don_format_price(self::$ostNaKonec) ?>
                </th>
            </tr>
            <?php } ?>
        </table>
        <?php
    }

    private static function setParamsToZero (){

        $vars = get_class_vars('Frm\FormReport');
        foreach ($vars as $k => $v) {
            if (is_numeric($v)){
                self::$itog[$k] += $v;
            }
        }

        self::$kol_abonent = self::$balance_2 = self::$kol_chel = self::$ostNaNach =
            self::$nachislTarif = self::$penya = self::$oplacheno = self::$korrekt =
            self::$ostNaKonec = self::$schet_summa = self::$ekin1 = self::$ekin2 = 0;
    }

    private static function drawItog(){
        if (__getParam('ul')){
            return;
        }
        foreach (self::$itog as $k => $v) {
            if (is_numeric($v)){
                self::$$k = $v;
            }
        }
        //table tag will closed in footer function
        echo '<h4>Итого по всем каналам</h4>';?>
        <table border="1" cellspacing="0" class="report">
            <tr>
                <th>кол-во<br>записей</th>
                <th colspan="2">площадь</th>
                <th>
                    Ост. <br>на<br>нач. <br>мес.
                </th>
                <th>
                    Начисл.
<!--                    <br>по тари<br>фу-->
                </th>
                <?php if (self::$b_penya){
                    echo "<th>Пеня</th>";
                }
                ?>
                <th>
                    Опла-<br>чено
                </th>
                <th>
                    Кор-<br>рект.
                </th>
                <th>
                    Ост.<br>на <br>конец мес.
                </th>
            </tr>
            <tr>
                <th>
                    <?= don_format_price(self::$kol_abonent, 0) ?>
                </th>
                <?php
                $ekin1 = self::$itog['ekin1'];
                $ekin2 = self::$itog['ekin2'];
                $colspan = $ekin2 > 0 ? '' : ' colspan="2"';
                ?>
                <th<?=$colspan?>>
                    <?=$ekin1?>
                </th>
                <?php
                if (!$colspan){
                    echo "<th>$ekin2</th>";
                }
                ?>
                <th>
                    <?= don_format_price(self::$ostNaNach)?>
                </th>
                <th>
                    <?= don_format_price(self::$nachislTarif)?>
                </th>
                <?php if (self::$b_penya){
                    $penya = don_format_price(self::$penya);
                    echo "<th>$penya</th>";
                }
                ?>
                <th>
                    <?= don_format_price(self::$oplacheno)?>
                </th>
                <th>
                    <?= don_format_price(self::$korrekt)?>
                </th>
                <th>
                    <?= don_format_price(self::$ostNaKonec)?>
                </th>
            </tr>
        </table>
        <?php
    }
}
