<?php
class Form  {
    public static function drawStreetSelect($selectedId = null, $required = false, $name = 'ul', $default = '', $attr = '')
    {
        $streets = Street::newInstance()->listAll();
        echo "<select class='form-control' name='$name' ".($required ? 'required' : '') . ' ' . $attr.' >'.PHP_EOL;
        if ($default == ''){
            echo '<option value="" hidden>Выберите канал</option>';
        } else {
            echo $default;
        }
        foreach ($streets as $ul) {
            $selected = "";
            if ($selectedId == $ul['id']) {
                $selected = "selected";
            }
            echo '<option value="' . $ul['id'] . '" ' . $selected . ' >' . $ul['name'] . '</option>'.PHP_EOL;
        }
        echo '</select>'.PHP_EOL;
    }

    public static function F1_select($drawMonth = true)
    {
        $current_year = \Models\Report::newInstance()->currentReportYear();
        $current_month = date('m');
        if ($sesYear = Session::newInstance()->_getForm('current_year')){
            $current_year = $sesYear;
        }
        if ($sesMonth = Session::newInstance()->_getForm('current_month')){
            $current_month = $sesMonth;
        }
        ?>
        <div class="box-header with-border">
            <h3 class="box-title">Выберите дату</h3>
        </div>
        <div class="row">
            <?php
            if ($drawMonth) {
                ?>
                <div class="col-xs-12 col-md-6 form-group">
                    <ul class="list-unstyled date-select month">
                        <?php
                        for ($i = 1; $i <= 12; $i++) {
                            $month = monthName($i, false, true);
                            $checked = $i == $current_month ? 'checked' : '';
                            echo "<li><label class='$checked'><input type='radio' name='month' value='$i' $checked >$month</label></li>";
                        }
                        ?>
                    </ul>
                </div>
                <?php
            }
            ?>
            <div class="col-xs-4 col-md-1 form-group">
                <ul class="list-unstyled date-select">
                <?php
                $year = date('Y') - 2;
                while ($year <= $current_year){
                    $checked = $year == $current_year ? 'checked' : '';
                    echo "<li><label class='$checked'><input type='radio' name='year' value='$year' $checked >$year</label></li>";
                    $year++;
                }
                ?>
                </ul>
            </div>
            <script>
                $('.date-select label').click(function () {
                    $(this).closest('ul').find('label.checked').removeClass('checked');
                    $(this).addClass('checked');
                });
            </script>
        </div>
        <?php
    }

    public static function monthYearSelect($name = '', $monthId = 0, $year = 0)
    {
        if (!$monthId){
            $monthId = date('n');
        }
        ?>
        <div class="col-md-2">
            <div class="form-group">
                <label for="<?=$name?>month">Месяц</label>
                <select id="<?=$name?>month" name="<?=$name?>month" multiple="" class="form-control" style="height: 240px">
                <?php
                $months = getMonths();
                foreach ($months as $id => $m) {
                    $selected = $id == $monthId ? 'selected' : '';
                    echo "<option value='$id' $selected>$m</option>";
                }
                ?>
                </select>
            </div>
        </div>
        <?php
        self::yearSelect($name, $year);
    }

    public static function yearSelect($name, $year, $class = 'col-md-2')
    {
        if (!$year){
            $year = date('Y');
        }
        ?>
        <div class="<?=$class?>">
            <div class="form-group">
                <label for="<?=$name?>year">Год</label>
                <select id="<?=$name?>year" name="<?=$name?>year" multiple="" class="form-control">
                    <?php
                    $current = date('Y');
                    $y = date('Y', strtotime('-3 year'));
                    while ($y <= $current){
                        $selected = $y == $year ? 'selected' : '';
                        echo "<option $selected>$y</option>";
                        ++$y;
                    }
                    ?>
                </select>
            </div>
        </div>
        <?php
    }
    
    public static function DrawControllersSelect($selectedId = null, $required = false,$name = 'contrId')
    {
        $options = Controller::newInstance()->listAll();
        echo "<select name='$name' ".($required ? 'required' : '').'>'.PHP_EOL;
        echo '<option value="">Выберите мираба</option>'.PHP_EOL;
        foreach ($options as $option) {
            $selected = "";
            if ($selectedId == $option['id']) {
                $selected = "selected";
            }
            echo '<option value="' . $option['id'] . '" ' . $selected . ' >' . $option['fio'] . '</option>'.PHP_EOL;
        }
        echo '</select>'.PHP_EOL;
    }

    public static function drawAbonentLsAndFio($callback = '')
    {
        ?>
        <div class="form-group row">
            <div class="col-xs-2">
                ФИО абонента
            </div>
            <div class="col-xs-4">
                <input class="form-control" type="text" id="fio" placeholder="если не знаете л/с" autocomplete="off">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-2">
                Л.с абонента
            </div>
            <div class="col-xs-4">
                <input class="form-control" type="number" name="ls" id="ls" value="<?=__getParam('ls')?>" required autocomplete="off">
            </div>
        </div>
        <?php
        don_ls_fio_autocompleteJs('summa', $callback);
    }

    public static function button($class, $text = '', $attrs = '', $show_icon = true)
    {
        if ($text == ''){
            $text = stristr($class, 'success') ? 'Добавить' : 'Обновить';
        }
        switch (true){
            case stristr($class, 'success'):
                $icon = 'plus';
                break;
            case stristr($class, 'primary'):
                $icon = 'save';
                break;
            case stristr($class, 'delete'):
                $icon = 'remove';
                break;
            default:
                $icon = '';
        }
        if (!$show_icon){
            $icon = '';
        }
        if ($icon != ''){
            $icon = '<i class="glyphicon glyphicon-'.$icon.'"></i> ';
        }
        ?>
        <button class="btn btn-sm btn-<?=$class?>" <?=$attrs?>><?= $icon . $text ?></button>
        <?php
    }

    public static function buttonSuccess($text = '', $attrs = '')
    {
        self::button('success', $text, $attrs);
    }

    public static function buttonUpdate($text = '', $attrs = ''){
        self::button('primary', $text, $attrs);
    }

    public static function buttonPrimary($text = '', $attrs = ''){
        self::button('primary', $text, $attrs,false);
    }

    public static function buttonRed($text = '', $attrs = ''){
        self::button('danger delete', $text, $attrs);
    }

    public static function checkForPrint()
    {
        ?>
        <div class="form-group">
            <label class="text-bold"><input type="checkbox" name="print" value="1"
                    <?=Session::newInstance()->_getForm('print') ? 'checked' : ''?>>
                Для печати
            </label>
        </div>
        <?php
    }
    public static function ekin($id = '', $ekinId = null, $ga = null, $num = null, $disabled = false)
    {
        ?>
        <td>
            <?php don_icon('glyphicon glyphicon-grain'); Form::ekinSelect("ekin_id[$id]", $ekinId, $disabled) ?>
        </td>
        <td>
            <?php Form::ekinNum("ekin_num[$id]", $num, $disabled) ?>
        </td>
        <td>
            <?php Form::ekinGa("ekin_ga[$id]", $ga, true, $disabled);
            echo $disabled ? '' : '<button class="cancel_ekin btn btn-danger">убрать</button>';
            ?>
        </td>
<?php
    }

    public static function ekinSelect($name = 'ekin_id', $selected = false, $disabled = false)
    {
        $ekin = \Models\Ekin::newInstance()->listAll()->getAll();
        self::generic_select($name,$ekin,'id', 'name', '',$selected, $disabled);
    }

    public static function ekinNum($name = 'ekin_num', $selected = false, $disabled = false)
    {
        self::generic_select($name,[['id' => 1], ['id' => 2]],'id', 'id', '',$selected, $disabled);
    }

    public static function ekinGa($name = 'ekin_ga', $value = false, $required = true, $disabled = false)
    {
        echo '<input type="text" '.($disabled ? 'disabled' : '').' class="ekin_ga" name="'.$name.'" value="'.$value.'" '.($required ? 'required' : '').'>';
    }

    static protected function generic_select($name, $items, $fld_key, $fld_name, $default_item, $id, $disabled = false)
    {
        $name = osc_esc_html($name);
        echo '<select name="' . $name . '" '.($disabled ? 'disabled' : '' ).' id="' . preg_replace('|([^_a-zA-Z0-9-]+)|', '', $name) . '" '.$attr.'>';
        if ($default_item) echo '<option value="">' . $default_item . '</option>';
        foreach($items as $i) {
            if(isset($fld_key) && isset($fld_name))
                echo '<option value="' . osc_esc_html($i[$fld_key]) . '"' . ( ($id == $i[$fld_key]) ? ' selected="selected"' : '' ) . '>' . $i[$fld_name] . '</option>';
        }
        echo '</select>';
    }

}