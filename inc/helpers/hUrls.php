<?php

function don_url_login() {
    return osc_base_url().'login/';
}
function don_url_js($file = ''){
    return osc_base_url().'assets/js/'.$file;
}

function don_path_js($file = ''){
    return osc_base_path().'public/assets/js/'.$file;
}

function don_path_css($file = ''){
    return osc_base_path().'public/assets/css/'.$file;
}

function don_url_css($file = ''){
    return osc_base_url().'assets/css/'.$file;
}

function don_url_abonent(){
    return osc_base_url().'abonent/';
}

function don_url_abonent_add(){
    return osc_base_url().'abonent/add';
}

function don_url_abonent_confirm(){
    return osc_base_url().'abonent/confirm';
}

function don_url_abonent_edit($ls = ''){
    return osc_base_url().'abonent/edit/'.$ls;
}

function don_url_payment(){
    return osc_base_url().'payment/';
}

function don_url_orosheniye(){
    return osc_base_url().'orosheniye/';
}

function don_url_reestr(){
    return osc_base_url().'payment/reestr';
}

function don_url_list($type = ''){
    return osc_base_url()."list/$type";
}

function don_url_street(){
    return osc_base_url().'street/';
}

function don_url_street_add(){
    return osc_base_url().'street/add';
}

function don_url_street_edit($id = ''){
    return osc_base_url().'street/edit/'.$id;
}

function don_url_tariff(){
    return osc_base_url().'tariff/';
}

function don_url_tariff_add(){
    return osc_base_url().'tariff/add';
}

function don_url_tariff_edit($id = ''){
    return osc_base_url().'tariff/edit/'.$id;
}

function don_url_rasteniye(){
    return osc_base_url().'rasteniye/';
}

function don_url_rasteniye_add(){
    return osc_base_url().'rasteniye/add';
}

function don_url_rasteniye_edit($id = ''){
    return osc_base_url().'rasteniye/edit/'.$id;
}

function don_url_controller(){
    return osc_base_url().'controller/';
}

function don_url_controller_add(){
    return osc_base_url().'controller/add';
}

function don_url_controller_edit($id = ''){
    return osc_base_url().'controller/edit/'.$id;
}

function don_url_report(){
    return osc_base_url().'report/';
}

function don_url_report_close(){
    return osc_base_url().'report/close';
}

function don_url_form_1(){
    return osc_base_url().'report/form-1';
}

function don_url_form_4(){
    return osc_base_url().'report/po-tarifam';
}

function don_url_uploads(){
    return osc_base_url().'uploads';
}

function don_url_images($file = ''){
    return don_url_uploads().'/images/'.$file;
}

function don_url_user_add_pay(){
    return osc_base_url().'user/add_pay';
}

function don_url_setting(){
    return osc_base_url().'setting/';
}