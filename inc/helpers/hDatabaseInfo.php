<?php

/*
 * Copyright 2014 Osclass
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

    /**
    * Helper Database Info
    * @author Osclass
    */

    /**
     * Gets database name
     *
     * @return string
     */
    function osc_db_name() {
        return DB_NAME;
    }

    /**
     * Gets database host
     *
     * @return string
     */
    function osc_db_host() {
        return DB_HOST;
    }

    /**
     * Gets database user
     *
     * @return string
     */
    function osc_db_user() {
        return DB_USER;
    }

    /**
     * Gets database password
     *
     * @return string
     */
    function osc_db_password() {
        return DB_PASSWORD;
    }

