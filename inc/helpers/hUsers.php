<?php

function user_has_rule($ruleId) {
    if ($_SESSION['main_account']){
        return true;
    }
    $rules = get_user_rules();
    return in_array($ruleId,$rules);
}

function get_user_rules() {
    $rules = __getSess('user_rules');
    if (!is_array($rules)){
        $userId = get_user_id();
        $userRules = new UserRules();
        $aRules = $userRules->listWhere('fk_i_user_id',$userId);
        $rules = [];
        foreach ($aRules as $r) {
            $rules[] = $r['i_rule'];
        }
        __setSess('user_rules', $rules);
    }
    return $rules;
}

function get_user_id() {
    return get_current_user_id();
}

function get_current_user_id() {
    return __getSess('user_id');
}

function get_current_user_db() {
    return __getSess('db_name');
}

function get_current_user_balance() {
    return __getSess('user_balance');
}