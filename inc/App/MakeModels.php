<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.01.2019
 * Time: 14:31
 */

namespace App;

use \DAO;
use Exception;

class MakeModels
{
    /**
     * @var DAO
     */
    private static $dao;

    /**
     * @var DAO
     */
    public static $rewrite;

    /**
     * MakeModels constructor.
     */
    public function __construct(){}

    public static function create($modelName, $tableName = '')
    {
        if (!$tableName){
            $tableName = strtolower($modelName);
        }
        $aFieldsName = self::getFields($tableName);

        $fieldsStr = join("','", $aFieldsName);

        $search = [
            'ModelsTemplate',
            '$tableName',
            'replace_fieldsStr',
            'replace_primary',
        ];
        $replace = [
            $modelName,
            $tableName,
            $fieldsStr,
            $aFieldsName[0]
        ];
        $newClass = str_replace($search, $replace, file_get_contents(MODELS_PATH . 'ModelsTemplate.php'));

        $fileName = MODELS_PATH . $modelName .'.php';
        $success = 0;
        if ((!file_exists($fileName) || self::$rewrite) && file_put_contents($fileName, $newClass)){
            $success = 1;
        }
        if ($success){
            $success = self::createSkeletion($modelName, $tableName, $aFieldsName);
        }
        return $success;
    }

    public static function createSkeletion($modelName, $tableName = '', $aFieldsName = [])
    {
        if (empty($aFieldsName)){
            if (!$tableName){
                $tableName = strtolower($modelName);
            }
            $aFieldsName = self::getFields($tableName);
        }

        $properties = '';
        $functions = '';

        foreach ($aFieldsName as $item) {
            $properties .= PHP_EOL."    private $$item;";
            $item_replaced = preg_replace('/^(i_|b_|s_|d_|dt_|pk_i_|fk_|fk_i)/', '', $item);
            $field_name = explode('_', $item_replaced);
            $funcName = '';
            foreach ($field_name as $name) {
                $funcName .= mb_convert_case($name, MB_CASE_TITLE);
            }
            $functions .= <<<here

    public function get$funcName() {
        return \$this->$item;    
    }

    /**
     * @access public
     * @since unknown
     * @param \$value mixed
     * @return \$this
     */
    public function set$funcName (\$value) {
        \$this->$item = \$value;
        return \$this;    
    }
here;
        }
        $search = [
            'ModelsTemplate',
            '/*replace_properties*/',
            '/*replace_functions*/',
        ];
        $replace = [
            $modelName,
            $properties,
            $functions,
        ];
        $newClassSkeletion = str_replace($search, $replace, file_get_contents(SKELETION_PATH . 'ModelsTemplateSkeletion.php'));

        $fileName = SKELETION_PATH . $modelName .'Skeletion.php';
        $success = 0;

        if ((!file_exists($fileName) || self::$rewrite) && file_put_contents($fileName, $newClassSkeletion)){
            $success = 2;
        }

        return $success;
    }

    private static function getFields($tableName)
    {
        if (!self::$dao instanceof DAO){
            self::$dao = new DAO('suunetk_avp');
        }

        $query = self::$dao->dao->query('DESCRIBE '.$tableName);
        if (!$query){
            throw new Exception('Ошибка при выполнении запроса '.self::$dao->getErrorDesc());
        }
        if (!$query->numRows()){
            throw new Exception('Нет такой таблицы');
        }

        $fields = $query->result();

        $aFieldsName = [];
        foreach ($fields as $field) {
            $aFieldsName[] = $field['Field'];
        }

        return $aFieldsName;
    }
}