<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\Users_logSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Users_log extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Users_log
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Users_log
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Users_log table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('users_log');
        $this->setPrimaryKey('id');
        $this->setFields(['id','fk_user_id','ip','user_agent','date']);
    }

    /**
     * @param null $value
     * @return Users_logSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return Users_logSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Users_log ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return Users_logSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return Users_logSkeletion::fromArray($result);
    }

    /**
     * @return Users_logSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return Users_logSkeletion::fromArray($result);
    }
}