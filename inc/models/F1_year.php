<?php

use Models\Abonent,
    \Models\Abonent_ekin,
    \Models\Orosheniye,
    \Models\Report,
    \Skeletion\F1_yearSkeletion;


if (!defined('ABS_PATH'))
    exit('ABS_PATH is not loaded. Direct access is not allowed.');

/**
 * Model database for City table
 *
 * @package Osclass
 * @subpackage Model
 * @since unknown
 */
class F1_year extends DAO {

    /**
     * It references to self object: F1_year.
     * It is used as a singleton
     *
     * @var F1_year
     */
    private static $instance;

    /**
     * @var string
     */
    public $lastId;

    /**
     * @var Abonent
     */
    private $abonent;

    /**
     * @var Report
     */
    private $report;

    /**
     * It creates a new F1_year object class ir if it has been created
     * before, it return the previous object
     *
     * @return $this
     */
    public static function newInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Set data related to t_city table
     */
    function __construct() {
        parent::__construct();
        $this->lastId = don_current_report_id();
        $this->abonent = Abonent::newInstance();
        $this->report = new Report();

        $this->setTableName('f1_year');
        $this->setPrimaryKey('pk_i_id');
        $this->setFields([
            'pk_i_id', 'id', 'ls', 'ostNaNach', 'i_litr', 'penya', 'nachislPoTarifu', 'oplacheno', 'korrekt'
            , 'ostNaKonec'
        ]);
    }


    /**
     * @param $id
     * @return DBRecordsetClass|mixed
     * @throws Exception
     */
    public function createNew($id)
    {
        $prevReportId = don_prev_report_id();
        $sql = sprintf('INSERT INTO f1_year (id, ls, ostNaNach, i_litr, penya, nachislPoTarifu, oplacheno, korrekt, ostNaKonec) SELECT %d, ls, balance, 0,0,0,0,0, balance FROM abonent', $id);
        $res = $this->dao->query($sql);
        if ($res) {
            $subQuery = sprintf('SELECT a.ls, sum(e.d_sum * ae.i_ga) as tarif FROM abonent a '
                . ' INNER join abonent_ekin ae on a.ls = ae.fk_abonent_id '
                . ' INNER join ekin e ON e.id = ae.fk_ekin_id '
                . ' WHERE a.b_counter = 0 and ae.fk_report_id = %d '
                . ' GROUP BY a.ls', $prevReportId);
            $sql = 'UPDATE f1_year '
                . ' INNER JOIN ('. $subQuery .') a ON a.ls = f1_year.ls '
                . ' SET nachislPoTarifu = tarif, ostNaKonec = tarif + ostNaNach '
                . ' WHERE id = ' . $id;
            $res = $this->dao->query($sql);
        }
        if (!$res) {
            throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
        }
        return $res;
    }

    /**
     * @param null $values
     * @return bool
     * @throws Exception
     */
    public function insert($values = null) {
        $data = [
            'id' => $this->lastId,
            'ls' => $values['ls'],
            'ostNaNach' => 0,
            'i_litr' => 0,
            'penya' => 0,
            'nachislPoTarifu' => 0,
            'oplacheno' => 0,
            'korrekt' => 0,
            'ostNaKonec' => $values['balance']
        ];

        $this->dao->from($this->getTableName());
        $this->dao->set($data);
        return $this->dao->insert();
    }

    /**
     * @param int $ls
     * @param null $values
     * @return bool|mixed
     * @throws Exception
     */
    public function update($ls, $values) {
        $abonent = Abonent::newInstance()->findByPrimaryKey($ls);
        $f1_year = $this->getLastDet($ls);

        $ostNaNach = $f1_year->getOstnanach();
        if (isset($values['nachislPoTarifu'])) {
            $f1_year->setNachislpotarifu($values['nachislPoTarifu']);
        }
        $nachPotar = $f1_year->getNachislpotarifu();
        $penya = $f1_year->getPenya();

        $oplacheno = $f1_year->getOplacheno() + $values['oplacheno'];
        $korrekt = $f1_year->getKorrekt() + $values['korrekt'];
        if (isset($values['lit_s'])){
            $nachPotar = $nachPotar + $values['summa'];
            $schetTek = $f1_year->getLitr() + $values['lit_s'];
            $f1_year->setLitr($schetTek);
            $f1_year->setNachislpotarifu($nachPotar);
        }
        //oplacheno, korrekt
        $f1_year->setOplacheno($oplacheno);
        $f1_year->setKorrekt($korrekt);

        $ostNaKonec = $f1_year->setOstnakonec($ostNaNach + $penya + $nachPotar + $korrekt - $oplacheno)
            ->getOstnakonec();

        $res = $f1_year->update();
        if ($res !== false) {
            $res = $abonent->setBalance($ostNaKonec)->update();
            if ($res === false){
                throw  new Exception($this->abonent->getErrorDesc(),ERR_C_MYSQL);
            }
            return $res;
        } else {
            throw  new Exception($this->dao->getErrorDesc(),ERR_C_MYSQL);
        }
    }

    /**
     * @param $ls
     * @return bool|F1_yearSkeletion
     * @throws Exception
     */
    private function getLastDet($ls) {
        if (empty($ls)){
            throw new Exception('LS ne zadan!', ERR_C_ABON_NENAYDEN);
        }
        $this->dao->select();
        $this->dao->from($this->getTableName());
        $this->dao->where(['ls' => $ls, 'id' => $this->lastId]);
        $res = $this->dao->get();
        if ($res) {
            return F1_yearSkeletion::build($res->row());
        } else {
            throw new Exception($this->getErrorDesc(),ERR_C_MYSQL);
        }
    }

    public function parent_update($values, $where)
    {
        return parent::update($values, $where);
    }


    public function abonentHistory($ls,$from,$to){
        $this->dao->select('a.*,c.fio cname,f.*,r.year,ul.name ulname, sum(ae1.i_ga) ga_1, sum(ae2.i_ga) ga_2')
            ->from($this->getTableName().' f')
            ->join('report r', 'f.id = r.id','INNER')
            ->join('abonent a', 'f.ls = a.ls','INNER')
            ->join('abonent_ekin ae1', 'ae1.fk_abonent_id = a.ls and ae1.i_num = 1 and ae1.fk_report_id=r.id','INNER')
            ->join('abonent_ekin ae2', 'ae2.fk_abonent_id = a.ls and ae2.i_num = 2 and ae1.fk_report_id=r.id','LEFT')
            ->join('ul', 'a.ul = ul.id','INNER')
            ->join('controller c', 'c.id = ul.contrId','INNER');
        if ($from){
            $this->dao->where('f.id >= '.$from);
        }
        $res = $this->dao->where('f.id <= ' . $to)
            ->orderBy('year','ASC')
            ->where('f.ls',$ls)
            ->groupBy('f.id')
            ->get();
        if ($res){
            return $res->result();
        } else {
            return [];
        }
    }


    public function listByf1($date = null) {
        if ($date == null){
            $date = __getParam('date');
        }

        $commerce = __getParam('b_is_commerce');
        $type = __getParam('report_type') == 'counter';
        $contrId = __getParam('contrId');
        $ul = __getParam('ul');
        $dolgOt = __getParam('dolg_ot');
        $dolgDo = __getParam('dolg_do');

        if (is_numeric($date)){
            $f1 = $this->report->findByPrimaryKey($date);
        } else {
            $f1 = $this->report->findByYear($date);
        }
        $id = $f1['id'];
        $this->dao->select('a.*,c.fio cname,f.*,r.year,ul.name ulname, sum(ae1.i_ga) ga_1, sum(ae2.i_ga) ga_2, DATE_FORMAT(a.updated_at, "%d.%m.%Y") as updated_at')
            ->from($this->getTableName().' f')
            ->join('report r', 'f.id = r.id','INNER')
            ->join('abonent a', 'f.ls = a.ls','INNER')
            ->join('abonent_ekin ae1', 'ae1.fk_abonent_id = a.ls and ae1.i_num = 1 and ae1.fk_report_id=r.id','INNER')
            ->join('abonent_ekin ae2', 'ae2.fk_abonent_id = a.ls and ae2.i_num = 2 and ae1.fk_report_id=r.id','LEFT')
            ->join('ul', 'a.ul = ul.id','INNER')
            ->join('controller c', 'c.id = ul.contrId','INNER')
            ->where('r.id',$id)
            ->groupBy('a.ls')
            ->orderBy('a.ul','ASC');
        if ($ul != ''){
            $this->dao->where('ul.id',$ul);
        }
        if ($contrId != ''){
            $this->dao->where('ul.contrId',$contrId);
        }
        if ($dolgOt != ''){
            $this->dao->where('f.ostNaKonec >=',$dolgOt);
        }
        if ($dolgDo != ''){
            $this->dao->where('f.ostNaKonec <=',$dolgDo);
        }
        $pref_sort = don_get_preference('sorting');
        switch ($pref_sort) {
            case 'fio':
                $this->dao->orderBy('a.fio','ASC');
                break;
            case 'domNom':
                $this->dao->orderBy('a.domNom','ASC');
                break;
            default :
                $this->dao->orderBy('a.ls','ASC');
                break;
        }
        $res = $this->dao->get();

        if ($res){
            return $res->result();
        } else {
            return [];
        }
    }


    /**
     * @param $reportId int report id
     * @param $type string mirab|kanal
     * @return array|mixed
     */
    public function reportGroup($reportId, $type)
    {
        if ($type == 'mirab'){
            $this->dao->groupBy('c.id')
                ->orderBy('cname', 'asc');
        } else {
            $this->dao->select('ul.name ulname')
                ->groupBy('ul.id')
                ->orderBy('ulname', 'asc');
        }
        $query = $this->select([
            'COUNT(f1_year.ls) AS ls',
            'SUM(lit_s) AS lit_s',
            'SUM(ostNaNach) AS ostNaNach',
            'SUM(nachislPoTarifu) AS nachislPoTarifu',
            'SUM(penya) AS penya',
            'SUM(oplacheno) AS oplacheno',
            'SUM(korrekt) AS korrekt',
            'SUM(ostNaKonec) ostNaKonec',
            'SUM(ostNaKonec) ostNaKonec',
            'SUM(ga) ga',
            'c.fio as cname'
        ])
            ->join('abonent a', 'a.ls = f1_year.ls','INNER')
            ->join(
                '(SELECT SUM(i_ga) ga,
                 fk_abonent_id ls,
                 SUM(o.i_ls) lit_s, ae.fk_report_id
                 FROM abonent_ekin ae
                 LEFT JOIN (
                    SELECT SUM(i_ls) i_ls, fk_abonent_ekin_id 
                    FROM orosheniye
                    WHERE fk_report_id = "'.(int)$reportId.'"
                    GROUP BY fk_abonent_ekin_id
                    ) o ON o.fk_abonent_ekin_id = ae.id
                 GROUP BY ae.fk_report_id, ae.fk_abonent_id) ae', 'a.ls = ae.ls and ae.fk_report_id = f1_year.id','INNER')
            ->join('ul','ul.id = a.ul','INNER')
            ->join('controller c','c.id = ul.contrId')
            ->where('f1_year.id', $reportId)
            ->get();

        if ($query){
            return $query->result();
        } else {
            don_logError($this->getErrorDesc());
            return [];
        }
    }

    public function getDataForIzvesheniye($year, $ul = null, $ls = null)
    {
        $this->select([
            'f.ls',
            'ostNaNach',
            'i_litr schet',
            'nachislPoTarifu',
            'oplacheno',
            'korrekt',
            'ostNaKonec'
        ],'f1_year f')
            ->join('abonent a', 'f.ls = a.ls', 'inner')
            ->join('report r', 'r.id = f.id', 'inner')
            ->where('r.year', $year)
            ->groupBy('f.ls')
            ->orderBy('a.ul', 'asc')
            ->orderBy('a.fio', 'asc');
        if ($ul){
            $this->dao->whereIn('f.ls', 'SELECT ls FROM abonent WHERE ul in ('.join(',',$ul).')');
        } elseif (!empty($ls)) {
            $ls = is_array($ls) ? $ls : [$ls];
            $this->dao->whereIn('f.ls', $ls);
        }
        $query = $this->dao->get();
        $f1_report = [];
        if ($query) {
            $f1_report = $query->result();
        }
        $this->select('a.*,ul.name ulname, c.fio cname,c.phone cphone, fd.ostNaNach', "abonent a")
            ->join('f1_year fd','fd.ls = a.ls','INNER')
            ->join('report r','r.id = fd.id and r.year = "' . $year . '"','INNER')
            ->join('ul','ul.id = a.ul','INNER')
            ->join('controller c','ul.contrId = c.id','INNER');
        if ($ul) {
            $this->dao->whereIn('a.ul', $ul);
        } elseif ($ls) {
            $ls = is_array($ls) ? $ls : [$ls];
            $this->dao->whereIn('a.ls', $ls);
        }
        $query = $this->dao->get();
        $abonents = $aLs = [];
        if ($query) {
            $res = $query->result();
            foreach ($res as $r) {
                $aLs[] = $r['ls'];
                $abonents[$r['ls']] = $r;
            }
        }

        if ($ul || $ls) {
            Abonent_ekin::newInstance()->dao
                ->join('report r', 'r.id = fk_report_id AND r.year = "' . $year . '"', 'INNER')
                ->whereIn('fk_abonent_id', $aLs);
        }
        $ekin = Abonent_ekin::newInstance()->listAll();
        $ekins = [];
        $ekinIds = [];
        while ($ekin->next()) {
            $e = $ekin->toArray();
            $ekins[$ekin->getAbonentId()][] = $e;
            $ekinIds[] = $e['id'];
        }

        $aOrosheniya = [];
        if (!empty($ekinIds)) {
            Orosheniye::newInstance()->dao
                ->join('report r', 'r.id = orosheniye.fk_report_id', 'inner')
                ->where('r.year', $year)
                ->whereIn('fk_abonent_ekin_id', $ekinIds);
            $orosheniya = Orosheniye::newInstance()->listAll()->getAll();
            foreach ($orosheniya as $item) {
                $aOrosheniya[$item['fk_abonent_ekin_id']][] = $item;
            }
        }
        return ['report' => $f1_report, 'abonents' => $abonents, 'ekinlar' => $ekins, 'orosheniya' => $aOrosheniya];
    }

    public function oroshenii($year) {
        $ul = __getParam('ul');

        $this->select('a.ls, ek.i_ls ekin_ls, e.*, count(o.id) ct_orosh, sum(o.i_ls / (ek.i_ls * e.i_ga)) sutki','v_abonent_ekin e')
            ->join('ekin ek','ek.id = e.fk_ekin_id','INNER')
            ->join('abonent a','a.ls = e.fk_abonent_id','INNER')
            ->join('report r','e.fk_report_id = r.id','INNER')
            ->join('orosheniye o','o.fk_abonent_ekin_id = e.id','LEFT')
            ->where('a.b_counter', 1)
            ->where('a.ul', $ul)
            ->where('r.year', $year)
            ->groupBy('e.id')
//            ->orderBy('sutki')
            ->orderBy('e.fio');

        $res = $this->dao->get();
        if ($res){
            return $res->result();
        } else {
            return [];
        }
    }
}
