<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\Abonent_ekinSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Abonent_ekin extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Abonent_ekin
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Abonent_ekin
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Abonent_ekin table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('v_abonent_ekin')
            ->setOrigTableName('abonent_ekin');
        $this->setPrimaryKey('id');
        $this->setFields(['id','fk_report_id','fk_abonent_id','fk_ekin_id','i_ga','i_num','name','fio']);
    }

    /**
     * @param null $value
     * @return Abonent_ekinSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return Abonent_ekinSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Abonent_ekin ne nayden');
        }
    }

    /**
     * @param $ls
     * @return Abonent_ekinSkeletion
     */
    public function findByLs($ls)
    {
        return $this->listWhere(['fk_abonent_id' => $ls, 'fk_report_id' => don_current_report_id()]);
    }
    /**
     * @param $key
     * @param null $value
     * @return Abonent_ekinSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return Abonent_ekinSkeletion::fromArray($result);
    }

    /**
     * @return Abonent_ekinSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return Abonent_ekinSkeletion::fromArray($result);
    }

    public function deleteByAbon($ls)
    {
        return $this->delete(['fk_abonent_id' => $ls, 'fk_report_id' => don_current_report_id()]);
    }

    /**
     * @param $reportId
     * @return bool
     * @throws Exception
     */
    public function createNew($reportId)
    {
        $prevReportId = don_prev_report_id();
        $sql = sprintf('INSERT INTO abonent_ekin (fk_report_id, fk_abonent_id, fk_ekin_id, i_ga, i_num) SELECT %d, fk_abonent_id, fk_ekin_id, i_ga, i_num FROM abonent_ekin WHERE fk_report_id = %d', $reportId, $prevReportId);
        if ($this->dao->query($sql)) {
            return true;
        } else {
            throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
        }
    }

    public function getHistory($ls, $fromId, $toId)
    {
        $this->dao->where('fk_report_id >=', $fromId)
            ->where('fk_report_id <=', $toId)
            ->orderBy('fk_report_id');
        return $this->listWhere(['fk_abonent_id' => $ls]);
    }
}