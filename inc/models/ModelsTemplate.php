<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\ModelsTemplateSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class ModelsTemplate extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var ModelsTemplate
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return ModelsTemplate
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to ModelsTemplate table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('$tableName');
        $this->setPrimaryKey('replace_primary');
        $this->setFields(['replace_fieldsStr']);
    }

    /**
     * @param null $value
     * @return ModelsTemplateSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return ModelsTemplateSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('ModelsTemplate ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return ModelsTemplateSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return ModelsTemplateSkeletion::fromArray($result);
    }

    /**
     * @return ModelsTemplateSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return ModelsTemplateSkeletion::fromArray($result);
    }
}