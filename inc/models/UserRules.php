<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class UserRules extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var City
         */
        private static $instance;

        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return City
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        /**
         * Set data related to t_city table
         */
        function __construct()
        {
            parent::__construct(DB_MAIN_NAME);
            $this->setTableName('users_rules');
            $this->setPrimaryKey('pk_i_id');
            $this->setFields([
                'pk_i_id', 'fk_i_user_id', 'i_rule'
                ]);
        }
    }