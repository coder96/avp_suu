<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\MigrationSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Migration extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Migration
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Migration
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Migration table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('migration');
        $this->setPrimaryKey('id');
        $this->setFields(['id','name','dt_date','status']);
    }

    /**
     * @param null $value
     * @return MigrationSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return MigrationSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Migration ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return MigrationSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return MigrationSkeletion::fromArray($result);
    }

    /**
     * @return MigrationSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return MigrationSkeletion::fromArray($result);
    }

    private function getMigrationFiles()
    {
        // Находим папку с миграциями
        $sqlFolder = osc_base_path().'inc/migrations/';
        // Получаем список всех sql-файлов
        $allFiles = glob($sqlFolder . '*.sql');

        $rows = $this->listAll()->getAll();
        $firstMigration = count($rows) == 0;

        // Первая миграция, возвращаем все файлы из папки sql
        if ($firstMigration) {
            return $allFiles;
        }

        $versionsFiles = array();
        foreach ($rows as $row) {
            array_push($versionsFiles, $sqlFolder . $row['name']);
        }

        // Возвращаем файлы, которых еще нет в таблице versions
        return array_diff($allFiles, $versionsFiles);
    }

    private function migrate($file)
    {
        $content = file_get_contents($file);
        $queries = explode(';',$content);
        $status = [];
        foreach ($queries as $query) {
            if (empty(trim($query))) {
                continue;
            }
            $res = $this->dao->query($query);
            $status[] = $res ? 1 : 0;
//            if (!$res) {
//                cc($query, 'res: ' . $this->getErrorDesc());
//            }
        }

        // Вытаскиваем имя файла, отбросив путь
        $baseName = basename($file);
        $this->insert(['name' => $baseName, 'dt_date' => date('Y-m-d H:i:s'), 'status' => join(',', $status)]);
    }

    public function chekVersion()
    {
        $files = $this->getMigrationFiles();

        foreach ($files as $file) {
            $this->migrate($file);
        }
    }
}