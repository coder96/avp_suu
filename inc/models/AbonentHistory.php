<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\AbonentHistorySkeletion,
    Skeletion\AbonentSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class AbonentHistory extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var AbonentHistory
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return AbonentHistory
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to AbonentHistory table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('abonent_history');
        $this->setPrimaryKey('id');
        $this->setFields(['id','report_id','ls','fio','s_phone','s_passport']);
    }

    /**
     * @param null $value
     * @return AbonentHistorySkeletion
     * @throws Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return AbonentHistorySkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('AbonentHistory ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return AbonentHistorySkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return AbonentHistorySkeletion::fromArray($result);
    }

    /**
     * @return AbonentHistorySkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return AbonentHistorySkeletion::fromArray($result);
    }

    /**
     * @param $id int report id
     * @return bool
     * @throws Exception
     */
    public function createHistory($id)
    {
        $sql = "INSERT INTO abonent_history SELECT null, $id, ls, fio, s_phone, s_passport FROM abonent";
        if ($this->dao->query($sql)) {
            return true;
        } else {
            throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
        }
    }

    public function updateHistory(AbonentSkeletion $abonent)
    {
        $currentReportId = don_current_report_id();
        $history = $this->listWhere(['ls' => $abonent->getLs(), 'report_id' => $currentReportId]);
        $history->next();

        $history->setReportId($currentReportId)
            ->setLs($abonent->getLs())
            ->setFio($abonent->getFio())
            ->setPhone($abonent->getPhone())
            ->setPassport($abonent->getPassport());
        return $history->update();
    }

    public function get($ls, $fromId, $toId)
    {
        $this->dao->where('report_id >=', $fromId)
            ->where('report_id <=', $toId)
            ->orderBy('report_id');
        return $this->listWhere(['ls' => $ls]);
    }
}