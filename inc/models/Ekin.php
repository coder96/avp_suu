<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\EkinSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Ekin extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Ekin
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Ekin
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Ekin table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('ekin');
        $this->setPrimaryKey('id');
        $this->setFields(['id','name','i_ls','d_sum']);
    }

    /**
     * @param null $value
     * @return EkinSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return EkinSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Ekin ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return EkinSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return EkinSkeletion::fromArray($result);
    }

    /**
     * @return EkinSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return EkinSkeletion::fromArray($result);
    }

    /**
     * @param null $values
     * @param null $key
     * @return mixed
     * @throws Exception
     */
    public function updateByPrimaryKey($values = null, $key = null)
    {
        $ekin = Ekin::newInstance()->findByPrimaryKey($key);
        if ($values == null){
            $values = $this->prepareFromInput();
            if ($key == NULL){
                $key = $values[$this->getPrimaryKey()];
            }
            unset($values[$this->getPrimaryKey()]);
        }
        $this->startTransaction();
        if ($ekin->getSum() != $values['d_sum']){
            $currentReportId = don_current_report_id();
            $sumGa = $values['d_sum'] - $ekin->getSum();
            //берем все годовые абоненты
            $subquery = 'select ls from abonent where b_counter = 0';

            //берем абонентов у кого текущий посев, и на текущем отчетном периоде
            $subqueryEkin = "select sum(i_ga) i_ga, fk_abonent_id ls from abonent_ekin where fk_abonent_id in ($subquery) and fk_ekin_id = {$ekin->getId()} and fk_report_id = $currentReportId group by fk_abonent_id";

            //обновляем начисление
            $query = "UPDATE f1_year INNER JOIN ($subqueryEkin) ekin ON ekin.ls = f1_year.ls SET nachislPoTarifu = nachislPoTarifu + ekin.i_ga * $sumGa WHERE f1_year.id = $currentReportId";
            if (!$res = $this->dao->query($query)) {
                $this->rollback();
                return false;
            }

            //обновляем отчет
            $query = "UPDATE f1_year SET ostNaKonec = ostNaNach + penya + nachislPoTarifu + korrekt - oplacheno WHERE f1_year.id = $currentReportId";
            if (!$res = $this->dao->query($query)) {
                $this->rollback();
                return false;
            }

            //обновляем баланс абонентов
            $query = "UPDATE abonent INNER JOIN f1_year f1y on abonent.ls = f1y.ls SET balance = ostNaKonec WHERE f1y.id = $currentReportId";
            if (!$res = $this->dao->query($query)) {
                $this->rollback();
                return false;
            }
        }
        $res = parent::updateByPrimaryKey($values, $key);
        if ($res) {
            $this->commit();
        } else {
            $this->rollback();
        }
        return $res;
    }
}