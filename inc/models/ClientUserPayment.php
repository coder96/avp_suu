<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');
    /**
     * Model database for ClientUserPayment table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class ClientUserPayment extends DAO
    {
        /**
         * It references to self object: ClientUserPayment.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var ClientUserPayment
         */
        private static $instance;
        
        private $manager;

        /**
         * It creates a new ClientUserPayment object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return ClientUserPayment
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to t_ClientUserPayment table
         */
        function __construct()
        {
            parent::__construct();
            $this->setTableName('user_payments');
            $this->setPrimaryKey('pk_i_id');
            $this->setFields([
                'pk_i_id', 'fk_i_user_id', 'd_summ', 'd_balance', 'd_date', 's_comment'
                ]);
            $this->manager = User::newInstance();
        }
        
        public function insert($userId = null) {
            $userId = func_get_arg(0);
            $summ = func_get_arg(1);
            $date = func_get_arg(2);
            $args = func_get_args();
            if (count($args) > 3){
                $comment = func_get_arg(3);
            } else {
                $comment = 'Оплата';
            }
            $user = $this->manager->findByPrimaryKey($userId);
            if (!$user || !user_has_rule(RULE_PROGRAMMIST) && $user['fk_i_parent_id'] != get_current_user_id()){
                throw new Exception('Пользователь не найден',ERR_C_USER_NENAYDEN);
            }
            
            $currentBalance = $user['d_balance'];
            
            if (!parent::insert(['d_date' => $date,'fk_i_user_id' => $userId, 'd_summ' => $summ, 'd_balance' => $summ + $currentBalance,'s_comment' => $comment])){
                $message = $this->getErrorDesc();
                if ($message == ''){
                    $message = 'Ошибка! Оплата не произведена';
                }
                throw new Exception($message,ERR_C_MYSQL);
            }
            
            return $this->manager->updateByPrimaryKey(['d_balance'  => $summ + $currentBalance], $userId);
        }
        
        public function updateBalance($summ) {
            $user = $this->manager->findByPrimaryKey(get_user_id());
            if (!$user){
                throw new Exception('Пользователь не найден',ERR_C_USER_NENAYDEN);
            }
            $currentBalance = $user['d_balance'];
            
            $this->manager->updateByPrimaryKey(['d_balance'  => $summ + $currentBalance], get_user_id());
        }
        
    }