<?php 
if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class Payment extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var Payment
         */
        private static $instance;
        private $f1_year;
        private $abonentLastPayments;
        
        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return Payment
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to t_city table
         */
        function __construct()
        {
            parent::__construct();
            $this->f1_year = F1_year::newInstance();
            $this->abonentLastPayments = [];
            
            $this->setTableName('payment');
            $this->setPrimaryKey('pk_i_id');
            $this->setFields([
                'pk_i_id','id_otcheta','fk_i_user_id','ls','numPachki','numDok','summa','date'
                ]);
        }

        public function findByPrimaryKey($pk = null)
        {
            $this->dao->select('a.fio')
                ->join('abonent a','a.ls = payment.ls','INNER');
            return parent::findByPrimaryKey($pk);
        }
        
        function getLastNP(){
            $this->dao
                    ->select('numPachki')
                    ->from($this->getTableName())
                    ->where('id_otcheta', $this->f1_year->lastId);
            if (!user_has_rule(RULE_GLAVNIY)){
                $this->dao->where('fk_i_user_id', get_current_user_id());
            }
            $res = $this->dao->orderBy('numPachki','DESC')
                    ->limit(1)
                    ->get();
            if ($res){
                if (isset($res->result()[0]['numPachki'])){
                    $np = $res->result()[0]['numPachki'];
                } else {
                    $np = 1;
                }
            } else {
                $np = 1;
            }
            return $np;
        }
        
        function getLastPayments($np,$order = 'DESC'){
            $ordering = strtoupper($order) == 'DESC' ? $order : 'ASC';
            $this->dao
                    ->select()
                    ->from($this->getTableName())
                    ->join('abonent', 'abonent.ls = '.$this->getTableName().'.ls','INNER')
                    ->join('ul', 'abonent.ul = ul.id','INNER')
                    ->where('id_otcheta', $this->f1_year->lastId)
                    ->where('numPachki', $np)
                    ->orderBy('pk_i_id',$ordering);
            
            if (user_has_rule(RULE_GLAVNIY)){
                $this->dao->where('(fk_i_user_id = '. get_current_user_id() . ' OR fk_i_user_id IS NULL)');
            } else {
                $this->dao->where('fk_i_user_id', get_current_user_id());
            }
            $res = $this->dao
                    ->get();
            if ($res){
                $result = $res->result();
            } else {
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }
            return $result;
        }

        /**
         * @param $id
         * @param $from
         * @param $to
         * @param bool $svodniy
         * @return array
         * @throws Exception
         */
        function listByF1($id, $from, $to, $svodniy = false) {
            if (!is_numeric($id)){
                $f1 = F1::newInstance()->findByDate($id);
                if (!$f1){
                    return [];
                }
                $id = $f1['id'];
            }
            $this->dao->orderBy('fk_i_user_id','ASC');
            if ($svodniy){
                $this->dao
                        ->select('count(*) as numDok, sum(summa) as summa, numPachki')
                        ->groupBy('numPachki')
                        ->orderBy('numPachki','ASC');
            } else {
                $this->dao
                        ->select()
                        ->orderBy('numPachki','ASC')
                        ->orderBy('pk_i_id','ASC')
                        ->join('abonent', 'abonent.ls = '.$this->getTableName().'.ls','INNER')
                        ->join('ul', 'abonent.ul = ul.id','INNER');
            }
            $this->dao
                ->from($this->getTableName())
                ->where('id_otcheta',$id)
                ->where('numPachki > 0')
                ->where('date >=', $from)
                ->where('date <=', $to)
                ->where('numPachki < 999');
            if (!user_has_rule(RULE_GLAVNIY)){
                $this->dao->where('fk_i_user_id', get_current_user_id());
            }
            $res = $this->dao->get();
            if ($res){
                return $res->result();
            } else {
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }
        }
        
        /**
         * 
         * @param type $id nomer pachki
         * @return array list paymets
         */
        
        function listByPachka($id,$id_otcheta) {
            if (!is_numeric($id_otcheta)){
                $f1 = F1::newInstance()->findByDate($id_otcheta);
                if (!$f1){
                    return [];
                }
                $id_otcheta = $f1['id'];
            }
            $this->dao
                    ->select()
                    ->from($this->getTableName())
                    ->join('abonent', 'abonent.ls = '.$this->getTableName().'.ls','INNER')
                    ->join('ul', 'abonent.ul = ul.id','INNER')
                    ->where('numPachki',$id)
                    ->where('id_otcheta',$id_otcheta)
                    ->orderBy('fk_i_user_id',ASC)
                    ->orderBy('pk_i_id',ASC);
            if (!user_has_rule(RULE_GLAVNIY)){
                $this->dao->where('fk_i_user_id', get_current_user_id());
            }
            $res = $this->dao->get();
            if ($res){
                return $res->result();
            } else {
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }
        }


        /**
         * @param null $values
         * @param string $type
         * @return bool|mixed|string
         * @throws Exception
         */
        function insertPay($values = null, $type = 'pay') {
            if ($values === null){
                $values = $this->prepareFromInput();
            }
            $ls = (int)$values['ls'];
            $summa = (int)$values['summa'];
            if (($type == 'pay' && $summa < 1 )|| $ls < 1){
                return false;
            }
            $lastId = $this->f1_year->lastId;
            
            $values['id_otcheta'] = $lastId;
            $values['fk_i_user_id'] = get_current_user_id();
            $date = date('Y-m-d',strtotime($values['date']));
            $otherPays = $this->listWhere(["(date >= '$date 00:00:00' AND date <= '$date 23:59:59' )" => null, 'ls' => $ls, 'summa' => $summa]);
            if (count($otherPays)){
                return '-88';
            }
            $res = $this->insert($values);
            
            if ($type == 'pay'){
                $f1_values = ['oplacheno' => $summa,'ls' => $ls,'id' => $lastId];
            } else {
                $f1_values = ['korrekt' => $summa,'ls' => $ls,'id' => $lastId];
            }
            if ($res){
                if (!user_has_rule(RULE_GLAVNIY)){
                    ClientUserPayment::newInstance()->updateBalance($summa * -1);
                }
                $r = $this->f1_year->update($ls,$f1_values);
            } else {
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }
            return $r;
        }
        
        /**
         * 
         */
        
        function insertKorrekt($values) {
            $ls = (int)$values['ls'];
            $summa = (int)$values['summa'];
            if ($ls < 1){
                return false;
            }
            $values['fk_i_user_id'] = get_current_user_id();
            
            $res = $this->insert($values);
            $f1_values = ['korrekt' => $summa,'ls' => $ls];
            if ($res){
                $r = $this->f1_year->update($ls,$f1_values);
            } else {
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }
            return $r;
        }

        /**
         * @param $id
         * @return bool|mixed
         * @throws Exception
         */
        function cancel($id) {
            $pay = $this->findByPrimaryKey($id);
            if ($pay === FALSE || $pay['id_otcheta'] !== $this->f1_year->lastId || ($pay['fk_i_user_id'] != get_current_user_id() && $pay['fk_i_user_id'] != null) || (!user_has_rule(RULE_GLAVNIY) && $pay['fk_i_user_id'] == null)){
                throw new Exception('оплата не найдена или не имеете право отменять', ERR_C_OTCHET_NENAYDEN);
            }
            
            $summa = $pay['summa'] * -1;
            $ls = $pay['ls'];
            $r = $this->f1_year->update($ls,['oplacheno' => $summa]);
            $res = false;
            if ($r){
                $res = $this->deleteByPrimaryKey($id);
            } else {
                throw new Exception($this->f1_year->getErrorDesc(), ERR_C_MYSQL);
            }
            if ($res && !user_has_rule(RULE_GLAVNIY)){
                ClientUserPayment::newInstance()->updateBalance($summa * -1);
            }
            return  $res;
        }

        /**
         * Преподготовка для вывода последней оплаты в извещении
         * @param array $ids
         * @param $f1 array|int
         * @return array
         */
        public function getAbonentsLastPayment(array $ids, $f1)
        {
            if (is_array($f1)){
                $f1 = $f1['id'];
            }
            $prev_id = $f1 - 1;
            $res = $this->dao
                ->select()
                ->from($this->getTableName())
                ->whereIn('ls',$ids)
                ->where("(id_otcheta = $f1 OR id_otcheta = $prev_id)")
                ->orderBy('pk_i_id','DESC')
                ->get();
            if ($res) {
                $data = $res->result();
                $p = [];
                foreach ($data as $pay) {
                    $old = $pay['id_otcheta'] == $f1 ? 'new': 'old';
                    $type = $pay['numPachki'] > 0 ? 'oplata' : 'korrekt';
                    if ($type == 'korrekt' && isset($p[$pay['ls']][$old][$type][0])){
                        //чтобы было понятно в извещении надо суммировать сумму корректировки
                        $p[$pay['ls']][$old][$type][0]['summa'] += $pay['summa'];
                    } else {
                        $p[$pay['ls']][$old][$type][] = $pay;
                    }
                }
                $this->abonentLastPayments = $p;
                return $data;
            } else {
                return [];
            }
        }

        /**
         * последняя оплата в извещении
         * @param $ls int
         * @return array|bool
         */
        public function getAbonentLP($ls)
        {
            $return = [];

            $oplata = $this->abonentLastPayments[$ls]['new']['oplata'][0];
            if (isset($oplata)){
                $oplata['type'] = 'new';
                $return[] = $oplata;
            } else {
                $oplata = $this->abonentLastPayments[$ls]['old']['oplata'][0];
                if (isset($oplata)){
                    $oplata['type'] = 'old';
                    $return[] = $oplata;
                }
            }

            $korrektirovka = $this->abonentLastPayments[$ls]['new']['korrekt'][0];
            if (isset($korrektirovka)){
                $korrektirovka['type'] = 'korrekt';
                $return[] = $korrektirovka;
            }
            return empty($return) ? false : $return;
        }

        public function abonentHistory($ls, $fromId, $toId)
        {
            $res = $this->dao
                ->select()
                ->from($this->getTableName())
                ->where('numPachki > 0')
                ->where('id_otcheta >=', $fromId)
                ->where('id_otcheta <=', $toId)
                ->where('numPachki < 999')
                ->where('ls', $ls)
                ->orderBy('date','ASC')
                ->get();

            return $res ? $res->result() : [];
        }
    }