<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @deprecated use don_get_preference('tarif_ii-iii') || don_get_preference('tarif_iv-i')
     * @since unknown
     */
    class Tariff extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var City
         */
        private static $instance;

        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return Tariff
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to t_city table
         */
        function __construct()
        {
            parent::__construct();
            
            $this->setTableName('tariff');
            $this->setPrimaryKey('id');
            $this->setFields([
                'id', 'name', 'price'
                ]);
        }
        
        public function updateByPrimaryKey($values = null, $key = null) {
            cc('Не доделано');
            exit;
            $old_tariff = $this->findByPrimaryKey();
            $new_tariff = $this->prepareFromInput();
            $res = parent::updateByPrimaryKey($values, $key);
            if ($res && ($new_tariff['price'] != $old_tariff['price'])){
                $diff = $new_tariff['price'] - $old_tariff['price'];
                $tariffId = $old_tariff['id'];
                $otchetId = don_current_report_id();
                
                //для обновление отчета тарифников
                $sql = "UPDATE f1_det 
                        SET tariffPrice = tariffPrice + $diff, 
                            nachislPoTarifu = nachislPoTarifu + kol * $diff,
                            nachislVsego = nachislVsego + kol * $diff,
                            ostNaKonec = ostNaKonec + kol * $diff
                        WHERE f1_det.ls in (SELECT ls FROM abonent WHERE b_counter = 0 AND tariff = $tariffId)"
                        . " AND f1_det.id = $otchetId";
                $this->dao->query($sql);
                
                //для обновление отчета счетчиков
                $sql = "UPDATE f1_det 
                        SET tariffPrice = tariffPrice + $diff, 
                            nachislPoTarifu = nachislPoTarifu + (schet_tek - schet_nach) * $diff,
                            nachislVsego = nachislVsego + (schet_tek - schet_nach) * $diff,
                            ostNaKonec = ostNaKonec + (schet_tek - schet_nach) * $diff
                        WHERE f1_det.ls in (SELECT ls FROM abonent WHERE b_counter = 1 AND tariff = $tariffId)"
                        . " AND f1_det.id = $otchetId";
                $this->dao->query($sql);
                
                Abonent::newInstance()->renewBallance($otchetId);
            }
            return $res;
        }
    }