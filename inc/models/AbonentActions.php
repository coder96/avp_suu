<?php

use Models\Abonent,
    \Skeletion\AbonentSkeletion,
    Skeletion\AbonentHistorySkeletion,
    Models\Ekin;

class AbonentActions {
    private $data = [];
    public $abonentId = '';
    private $abonent;

    public function __construct() {
        $this->abonent = new Abonent;
    }
    
    public function prepareData(){
        $fields = $this->abonent->getFields();
        $params = Params::getParamsAsArray();
        foreach ($params as $key => $value) {
            if (in_array($key, $fields)){
                $this->data[$key] = $value;
            }
        }
        return $this->data;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function insert() {
        $this->abonent->startTransaction();
        $abonent = AbonentSkeletion::build($this->data);
        $currentReportId = don_current_report_id();

        $res = $abonent->insert();
        if (!$res){
            throw new Exception($this->abonent->getErrorDesc(), ERR_C_MYSQL);
        }

        $history = AbonentHistorySkeletion::build();
        $history->setReportId($currentReportId)
            ->setLs($abonent->getLs())
            ->setFio($abonent->getFio())
            ->setPhone($abonent->getPhone())
            ->setPassport($abonent->getPassport());
        $res = $history->insert();
        if (!$res){
            throw new Exception($this->abonent->getErrorDesc(), ERR_C_MYSQL);
        }

        $ls = $abonent->getLs();
        $this->abonentId = $ls;
        $r = F1_year::newInstance()->insert($abonent->toArray());
        if (!$r){
            throw new Exception(F1_year::newInstance()->getErrorDesc(), ERR_C_MYSQL);
        }

        //ekinlar
        $ekin = __getParam('ekin_id');
        $ekin_ga = __getParam('ekin_ga');
        $ekin_num = __getParam('ekin_num');
        $skEkin = \Skeletion\Abonent_ekinSkeletion::build();
        $skEkin->setAbonentId($ls)
            ->setReportId($currentReportId);
        foreach ($ekin as $key => $id) {
            $ekin_ga[$key] = str_replace(',','.',$ekin_ga[$key]);
            if (!$skEkin->setEkinId($id)
                ->setId(null)
                ->setGa($ekin_ga[$key])
                ->setNum($ekin_num[$key])
                ->insert()){
                throw new Exception('Не удалось добавить посев. '. \Models\Abonent_ekin::newInstance()->getErrorDesc(), ERR_C_MYSQL);
            }
            if (!$abonent->getBCounter()){
                $ekin = Ekin::newInstance()->findByPrimaryKey($skEkin->getEkinId());
                $sumGa = $ekin->getSum() * $skEkin->getGa();
                F1_year::newInstance()->update($ls, ['lit_s' => 0,'summa' => $sumGa]);
            }
        }

        $this->abonent->commit();
        return $r;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function edit() {
        $this->abonent->startTransaction();
        $success = TRUE;
        $abonent = $this->data;
        $ls = $abonent['ls'];
        $this->abonentId = $ls;
        $currentReportId = don_current_report_id();

        unset($abonent['balance']);
        unset($abonent['balance_2']);
        unset($abonent['ls']);

        $res = $this->abonent->update($abonent,['ls' => $ls]);

        $abonentSk = Abonent::newInstance()->findByPrimaryKey($ls);
        \Models\AbonentHistory::newInstance()->updateHistory($abonentSk);

        if ($res || $res === 0){
            //ekinlar
            $ekin = __getParam('ekin_id');
            if (!is_array($ekin)){$ekin = [];}
            $ekin_ga = __getParam('ekin_ga');
            $ekin_num = __getParam('ekin_num');
            $skEkin = \Skeletion\Abonent_ekinSkeletion::build();
            $skEkin->setAbonentId($ls)
                ->setReportId($currentReportId);
            \Models\Abonent_ekin::newInstance()->deleteByAbon($ls);
            $error = 0;
            foreach ($ekin as $key => $id) {
                $ekin_ga[$key] = str_replace(',','.',$ekin_ga[$key]);
                $skEkin->setEkinId($id)
                    ->setGa($ekin_ga[$key])
                    ->setNum($ekin_num[$key]);
                if (is_numeric($key)){
                    $skEkin->setId(null);
                } else {
                    //key ~ e_18 --- 18 = id
                    $skEkin->setId(substr($key,2));
                }
                $res = $skEkin->insert();

                if ($res === false){
                    $error++;
                }
            }
            $nachPotarifu = 0;
            $oroshenii = \Models\View\Orosheniye::newInstance()->history($ls, $currentReportId, $currentReportId);
            while ($oroshenii->next()) {
                $nachPotarifu += $oroshenii->getSumma();
            }
            if ($error){
                throw new Exception('Абонент обновлен, но не удалось обновить '.$error.' посев.'. (OSC_DEBUG ? \Models\Abonent_ekin::newInstance()->getErrorDesc() : ''), ERR_C_MYSQL);
            } elseif (!$abonent['b_counter']){
                $ekins = \Models\Abonent_ekin::newInstance()->getHistory($ls, $currentReportId, $currentReportId);
                while ($ekins->next()) {
                    $ekin = Ekin::newInstance()->findByPrimaryKey($ekins->getEkinId());
                    $nachPotarifu += $ekin->getSum() * $ekins->getGa();
                }
            }

            F1_year::newInstance()->update($ls, ['lit_s' => 0, 'nachislPoTarifu' => $nachPotarifu]);

            $this->abonent->commit();
        } else {
            $success = false;
        }
        return $success;
    }
}