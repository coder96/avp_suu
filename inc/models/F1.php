<?php 
if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class F1 extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var F1
         */
        private static $instance;

        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return F1
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to F1 table
         */
        function __construct()
        {
            parent::__construct();
            
            $this->setTableName('f1');
            $this->setPrimaryKey('id');
            $this->setFields([
                'id','month','year'
                ]);
        }

        public function currentReportDate()
        {
            $last_report = $this->findByPrimaryKey(don_current_report_id());
            $month = strlen($last_report['month']) > 1 ? $last_report['month'] : '0' . $last_report['month'];
            return $last_report['year'] . '-' . $month;
        }
        
        public function insert($date = null) {
            $year = date('Y', strtotime($date));
            $month = date('m', strtotime($date));
            if (parent::insert(['year' => $year, 'month' => $month])){
                $res = $this->insertedId();
                $this->dao->query('update `lastId` set lastId=' . $res);
            } else {
                $res = false;
            }
            return $res;
        }
        
        function findByDate($date, $equals = false) {
            $year = date('Y', strtotime($date));
            $month = date('m', strtotime($date));
            $this->dao->select();
            $this->dao->from($this->getTableName());
            if ($equals){
                $this->dao->where(['year' => $year,'month' => (int)$month]);
            } else {
                $this->dao->where(['year <=' => $year,'month <=' => (int)$month]);
            }
            $this->dao->orderBy('id','desc')
                ->limit(1);
            $res = $this->dao->get();
            if ($res){
                return $res->row();
            } else {
                return false;
            }
        }
    }