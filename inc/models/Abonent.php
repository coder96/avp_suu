<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\AbonentSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Abonent extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Abonent
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Abonent
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Abonent table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('v_abonent');
        $this->setPrimaryKey('ls');
        $this->setFields(['ls','fio','ul','balance','i_counter','b_counter','d_penyaLastExec'
            , 'b_notified', 's_phone', 's_passport', 's_dogovor', 'ulname', 'cname', 'updated_at']);
    }

    /**
     * @param null $value
     * @return AbonentSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return AbonentSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Abonent ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return AbonentSkeletion
     */
    public function listWhere($key = null,$value = null)
    {
        if ($key == null){
            $fields = [
                'ul' => 'ulId',
                'ls' => 'ls',
                'u.contrId' => 'contrId',
            ];
            $key = $this->prepareFromInput($fields);
            foreach ($key as $k => $v) {
                if ($v !== ''){
                    $this->dao->where($k,$v);
                }
            }
            if ($fields['u.contrId']){
                $this->dao->join('ul u', 'u.id = v_abonent.ul', 'inner');
            }
        }
        $this->dao
            ->select('sum(e.i_ga) ga')
            ->join('abonent_ekin e', 'v_abonent.ls = e.fk_abonent_id and e.fk_report_id=' . don_current_report_id(), 'LEFT')
            ->groupBy('v_abonent.ls');
        $result = parent::listWhere(['1' => '1'],$value = null);
        return AbonentSkeletion::fromArray($result);
    }

    /**
     * @return AbonentSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return AbonentSkeletion::fromArray($result);
    }

    public function renewBallance($otchetId) {
        if ($otchetId < 1){
            return false;
        }
        //для обновление баланса абонентов
        $sql = "UPDATE abonent a INNER JOIN f1_year b ON a.ls = b.ls AND b.id =$otchetId SET balance = ostNaKonec ";
        $this->dao->query($sql);
    }
}