<?php

class Rewrite {
    public static function init(){
        $len = strpos($_SERVER['REQUEST_URI'], '?') !== false ? strpos($_SERVER['REQUEST_URI'], '?') : strlen($_SERVER['REQUEST_URI']);
        $uri = mb_substr(urldecode($_SERVER['REQUEST_URI']), 0, $len);
        $array = [];
        $count = preg_match_all('/[А-яA-z_0-9\-]+/iu', $uri,$array);
        if ($count > 0){
            $controller = array_shift($array[0]);
            Params::setParam('controller', $controller);
            __get()->controller = $controller;
        }
        if ($count > 1){
            $action = array_shift($array[0]);
            Params::setParam('action', $action);
            __get()->action = $action;
        }
        if ($count > 2){
            Params::setParam('id', array_shift($array[0]));
        }
        if ($count > 3){
            Params::setParam('sub_action', array_shift($array[0]));
        }
    }
}