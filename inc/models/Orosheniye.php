<?php
/**
 * Created by don console
 **/

namespace Models;

use \DAO;
use Exception;
use Skeletion\OrosheniyeSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Orosheniye extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Orosheniye
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Orosheniye
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Orosheniye table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('orosheniye');
        $this->setPrimaryKey('id');
        $this->setFields(['id','fk_report_id','fk_abonent_ekin_id','d_tariff_price','i_ls','i_summa','d_date']);
    }

    /**
     * @param null $value
     * @return OrosheniyeSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return OrosheniyeSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Orosheniye ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return OrosheniyeSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return OrosheniyeSkeletion::fromArray($result);
    }

    public function calculate($abonent_ekinId, $sutka, $date)
    {
        try {
            $abonent_ekin = Abonent_ekin::newInstance()->findByPrimaryKey($abonent_ekinId);
            $ekin = Ekin::newInstance()->findByPrimaryKey($abonent_ekin->getEkinId());
            $kv = ceil(date('n', strtotime($date)) / 3);
            if (in_array($kv,[1,4])){
                $tariff = don_get_tarif_41();
            } else {
                $tariff = don_get_tarif_23();
            }

            $ls = round($ekin->getLs() * $abonent_ekin->getGa() * $sutka);
            $res = round($ls * $tariff);
            $data = ['lit_s' => $ls, 'summa' => $res, 'tariff' => $tariff];
            return $data;
        } catch (Exception $ex){
            return '';
        }
    }

    /**
     * @return OrosheniyeSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return OrosheniyeSkeletion::fromArray($result);
    }
}