<?php
/**
 * Created by don console
 **/

namespace Models\View;

use \DAO;
use Exception;
use \Skeletion\View\OrosheniyeSkeletion;

//если найдете ошибку чините ее в файле Models-Template в папке Models
class Orosheniye extends DAO
{
    /**
     * @access private
     * @since unknown
     * @var Orosheniye
     */
    private static $instance;


    /**
     * @access public
     * @since unknown
     * @return Orosheniye
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Orosheniye table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('v_orosheniye');
        $this->setPrimaryKey('id');
        $fields = array_merge(\Models\Orosheniye::newInstance()->getFields(), ['ls','fio','i_ga','name']);
        $this->setFields($fields);
    }

    /**
     * @param null $value
     * @return OrosheniyeSkeletion
     * @throws \Exception
     */
    function findByPrimaryKey($value = null)
    {
        $result = parent::findByPrimaryKey($value);
        if ($result){
            try {
                return OrosheniyeSkeletion::build($result);
            } catch (\Error $error){
                throw new Exception($error->getMessage());
            }
        } else {
            throw new Exception('Orosheniye ne nayden');
        }
    }

    /**
     * @param $key
     * @param null $value
     * @return OrosheniyeSkeletion
     */
    public function listWhere($key,$value = null)
    {
        $result = parent::listWhere($key,$value = null);
        return OrosheniyeSkeletion::fromArray($result);
    }

    /**
     * @param $ls
     * @param $from
     * @param $to
     * @return \Skeletion\View\OrosheniyeSkeletion
     */
    public function history($ls, $from, $to)
    {
        $this->dao->where('ls', $ls)
            ->where('fk_report_id >=', $from)
            ->where('fk_report_id <=', $to)
            ->orderBy('d_date');
        return $this->listAll();
    }

    /**
     * @return \Skeletion\View\OrosheniyeSkeletion
     */
    public function listAll()
    {
        $result = parent::listAll();
        return OrosheniyeSkeletion::fromArray($result);
    }
}