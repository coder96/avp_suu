<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class Controller extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var Controller
         */
        private static $instance;

        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return Controller
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to t_city table
         */
        function __construct()
        {
            parent::__construct();
            
            $this->setTableName('controller');
            $this->setPrimaryKey('id');
            $this->setFields([
                'id', 'fio', 'phone'
                ]);
        }
        
        public function insert($values = null) {
            $res = parent::insert($values);
            if ($res){
                $id = $this->insertedId();
                $streets = __getParam('streets');
                Street::newInstance()->updateController($id,$streets);
            }
            return $res;
        }
        
        public function updateByPrimaryKey($values = null,$key = null) {
            $res = parent::updateByPrimaryKey($values, $key);
            if ($res || $res === 0){
                if ($key == null){
                    $key = __getParam($this->primaryKey);
                }
                $streets = __getParam('streets');
                Street::newInstance()->updateController($key,$streets);
            }
            return $res;
        }
        
        public function findByPrimaryKey($key = null) {
            $controller = parent::findByPrimaryKey($key);
            if ($controller){
                $streets = Street::newInstance()->listWhere('contrId',$controller['id']);
                foreach ($streets as $street) {
                    $controller['streets'][] = $street['id'];
                }
            }
            return $controller;
        }
        
        public function listAll() {
            $this->dao->select('c.*, count(u.id) as count');
            $this->dao->from($this->getTableName().' c');
            $this->dao->join('ul u', 'u.contrId = c.id','LEFT');
            $this->dao->groupBy('c.id');
            $result = $this->dao->get();

            if($result == false) {
                return array();
            }

            return $result->result();
        }
    }