<?php if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class Street extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var Street
         */
        private static $instance;

        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return Street
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to t_city table
         */
        function __construct()
        {
            parent::__construct();
            
            $this->setTableName('ul');
            $this->setPrimaryKey('id');
            $this->setFields([
                'id', 'name', 'contrId'
                ]);
        }
        
        public function updateController($id,$steets_id) {
            if (!is_array($steets_id)){
                $steets_id = [$steets_id];
            }
            $this->update(['contrId' => null], ['contrId' => $id]);
            $this->dao->from($this->getTableName());
            $this->dao->set('contrId',$id);
            $this->dao->whereIn('id',$steets_id);
            return $this->dao->update();
        }
        
        public function listAll() {
            $result = $this->dao->select('c.fio cname, u.*')
                ->from($this->getTableName().' u')
                ->join('controller c', 'u.contrId = c.id','LEFT')
                ->orderBy('u.name')
                ->get();

            if($result == false) {
                return array();
            }

            return $result->result();
        }
    }