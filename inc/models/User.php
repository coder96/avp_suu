<?php

use Classes\Cookie;

if ( ! defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');

    /**
     * Model database for City table
     *
     * @package Osclass
     * @subpackage Model
     * @since unknown
     */
    class User extends DAO
    {
        /**
         * It references to self object: City.
         * It is used as a singleton
         *
         * @access private
         * @since unknown
         * @var User
         */
        private static $instance;
        
        private $data;

        /**
         * It creates a new City object class ir if it has been created
         * before, it return the previous object
         *
         * @access public
         * @since unknown
         * @return self
         */
        public static function newInstance()
        {
            if( !self::$instance instanceof self ) {
                self::$instance = new self;
            }
            return self::$instance;
        }

        /**
         * Set data related to t_city table
         */
        function __construct()
        {
            parent::__construct(DB_MAIN_NAME);
            $this->setUserTable();
        }

        private function setUserTable()
        {
            $this->setTableName('users');
            $this->setPrimaryKey('pk_i_id');
            $this->setFields([
                'pk_i_id','fk_i_parent_id', 'fio', 's_name', 's_password', 's_db_name', 's_db_user', 's_iskakb', 's_domain', 'd_balance','i_tariffPrice','i_block', 'i_rule'
            ]);
        }

        private function setUserCookieTable()
        {
            $this->setTableName('user_coockie_auth');
            $this->setPrimaryKey('id');
            $this->setFields([
                'id',
                'fk_i_user_id',
                's_secret',
                's_agent',
                'd_expires',
                'i_logs'
            ]);
        }
        /**
         * @param int $ruleId
         * @return array
         */
        public function listAll($ruleId = null) {
            $result = [];
            $this->dao
                    ->select('u.*')
                    ->from($this->getTableName().' u')
                    ->groupBy('u.pk_i_id');
            $res = $this->dao->get();
            if ($res){
                $result = $res->result();
            }
            return $result;
        }
        
        public function insert($values = null) {
            $this->prepareData();
            
            $values = $this->data['values'];
            $rule = $this->data['rule'];
            
            $res = parent::insert($values);
            if (!$res){
                echo $this->getErrorDesc();
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }
            
            $insertedId = $this->dao->insertedId();
            
            $userRules = new UserRules();
            foreach ($rule as $r) {
                $fields = [
                    'fk_i_user_id' => $insertedId,
                    'i_rule' => $r
                ];
                if (!$userRules->insert($fields)){
                    osc_add_flash_error_message('Ошибка должность не определена');
                }
            }
            return ($res);
        }
        
        public function update($values = null,$userId = null) {
            if ($values === null){
                $this->prepareData(TRUE);

                $values = $this->data['values'];
                $rules = $this->data['rule'];
                $userId = ['pk_i_id' => $values['pk_i_id']];
                unset($values['pk_i_id']);
            }
            
            $res = parent::update($values, $userId);
            
            if ($res === FALSE){
                echo $this->getErrorDesc();
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            } else {
                $res = true;
            }
            
            if ($rules != null){
                $userRules = new UserRules();
                $userRules->delete(['fk_i_user_id' => $userId]);
                foreach ($rules as $r) {
                    $fields = [
                        'fk_i_user_id' => $userId,
                        'i_rule' => $r
                    ];
                    if (!$userRules->insert($fields)){
                        osc_add_flash_error_message('Ошибка должность не определена');
                    }
                }
            }
            return ($res);
        }
        
        public function findByPrimaryKey($id = null) {
            $result = [];
            $this->dao
                    ->select('u.*')
                    ->from($this->getTableName().' u')
                    ->where(['u.pk_i_id' => $id]);

            $res = $this->dao
                    ->groupBy('u.pk_i_id')
                    ->get();
            if ($res){
                $result = $res->result();
            }
            if (empty($result)){
                return false;
            } else {
                return $result[0];
            }
        }

        public function dbContainUser($login, $password)
        {
            $rows = $this->listWhere(['s_name' => $login, 's_password' => $password]);
            if (count($rows) == 1){
                return array_shift($rows);
            } else {
                return false;
            }
        }
        
        public function prepareData($edit = false, $redirect = true) {
            $error = false;
            $fields = [
                'fio' => 'fio',
                's_name' => 'name',
                's_password' => 'password_1',
                'password_confirm' => 'password_2'
            ];
            if ($edit){
                $fields['pk_i_id'] = 'pk_i_id';
                $fields['i_block'] = 'i_block';
            }
            $values = $this->prepareFromInput($fields);
            
            if ($edit && !isset($values['i_block'])){
                $values['i_block'] = 0;
            }
            
            if ($values['s_password'] != $values['password_confirm']){
                $errCode = 1;
                $error = 'Пароль и подтверждение пароля не совпадают';
            } elseif(!preg_match('/^(?=.{8,})(?=.*[a-z])(?=.*[0-9])/i', $values['s_password'])) {
                $errCode = 1;
                $error = 'Пароль слишком простой';
            } else {
                unset($values['password_confirm']);
            }
            
            $user = $this->listWhere('s_name',$values['s_name']);
            if (!empty($user) && !$edit){
                $error = 'Такой логин существует, измените логин';
            }
            
            if (strlen($values['s_name']) < 6){
                $error = 'Логин слишком короткий должно быть больше 6 символов';
            }
            
            if ($edit && $values['s_password'] == '' && $errCode == 1){
                $error = false;
                unset($values['s_password']);
                unset($values['password_confirm']);
            } else {
                $values['s_password'] = don_get_md5_password($values['s_password']);
            }
            
            $values['fk_i_parent_id'] = get_user_id();
            $values['s_db_name'] = $_SESSION['db_name'];
            $values['s_db_user'] = $_SESSION['db_user'];
            $values['s_iskakb'] = $_SESSION['iskakb'];
            $values['s_domain'] = 'avpsuu';
            
            $rule = __getParam('i_rule');
            if (empty($rule)){
                $error = 'Ошибка должность не определена';
            } else {
                foreach ($rule as $r) {
                    if ($r <= 2){
                        $error = 'Пользователь не может быть выше или равным должностью главного аккаунта!';
                    }
                }
            }
            
            if ($error){
                osc_add_flash_error_message($error);
                if ($edit && $redirect){
                    osc_redirect_to('/users/edit/'.$values['pk_i_id']);
                } elseif ($redirect) {
                    osc_redirect_to('/users/add');
                }
            }
            $this->data['values'] = $values;
            $this->data['rule'] = $rule;
        }

        public function setSecretCookie($id)
        {
            $secret = osc_genRandomPassword();

            $this->setUserCookieTable();
            $res = parent::insert([
                'fk_i_user_id' => $id,
                's_secret' => $secret,
                's_agent' => $_SERVER['HTTP_USER_AGENT'],
                'd_expires' => date('Y-m-d', strtotime('+1 month'))
            ]);
            $this->setUserTable();

            Cookie::newInstance()->push('oc_userId', $id);
            Cookie::newInstance()->push('oc_userSecret', $secret);
            Cookie::newInstance()->set();
        }

        public function loginBySecret()
        {
            $id = Cookie::newInstance()->get_value('oc_userId');
            $secret = Cookie::newInstance()->get_value('oc_userSecret');
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
            $user = parent::findByPrimaryKey($id);
            if (!$user){
                return false;
            }

            $this->dao->query('DELETE FROM user_coockie_auth WHERE d_expires < "'.date('Y-m-d').'"');

            $this->setUserCookieTable();
            $secretAuth = $this->listWhere([
                'fk_i_user_id' => $id,
                's_secret' => $secret,
                's_agent' => $userAgent
            ])[0];
            if (!empty($secretAuth)){
                parent::update(['i_logs' => $secretAuth['i_logs'] + 1], ['id' => $secretAuth['id']]);
            }
            $this->setUserTable();
            if (empty($secretAuth)){

                return false;
            } else {
                return dbContainUser($user['s_name'], $user['s_password']);
            }
        }

        public function dropCookie()
        {
            Cookie::newInstance()->pop('oc_userId');
            Cookie::newInstance()->pop('oc_userSecret');
            Cookie::newInstance()->set();
        }
    }
    