<?php

namespace Models;


use \Exception, \F1_year;


if (!defined('ABS_PATH')) exit('ABS_PATH is not loaded. Direct access is not allowed.');


/**
 * Model database for City table
 *
 * @package Osclass
 * @subpackage Model
 * @since unknown
 */
class Report extends \DAO
{
    /**
     * It references to self object: City.
     * It is used as a singleton
     *
     * @access private
     * @since unknown
     * @var Report
     */
    private static $instance;

    /**
     * It creates a new City object class ir if it has been created
     * before, it return the previous object
     *
     * @access public
     * @since unknown
     * @return Report
     */
    public static function newInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * Set data related to Report table
     */
    function __construct()
    {
        parent::__construct();

        $this->setTableName('report');
        $this->setPrimaryKey('id');
        $this->setFields([
            'id', 'year'
        ]);
    }

    public function currentReportYear()
    {
        $last_report = $this->findByPrimaryKey(don_current_report_id());
        return $last_report['year'];
    }

    public function findByYear($year)
    {
        $res = $this->select()
            ->where('year', $year)
            ->get();
        return $res ? $res->row() : false;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function createReport()
    {
        $currentYear = $this->currentReportYear();

        if ($currentYear > date('Y') + 1) {
            throw new Exception('Отчет за текущий год уже закрыт!');
        }
        try {
            $this->startTransaction();

            ++$currentYear;
            if (!$this->insert(['year' => $currentYear])) {
                throw new Exception($this->getErrorDesc(), ERR_C_MYSQL);
            }

            $reportId = $this->dao->insertedId();

            don_set_prev_report_id(don_current_report_id());
            don_set_current_report_id($reportId);

            AbonentHistory::newInstance()->createHistory($reportId);
            F1_year::newInstance()->createNew($reportId);
            Abonent_ekin::newInstance()->createNew($reportId);
            Abonent::newInstance()->renewBallance($reportId);

            $this->commit();
            return true;
        } catch (Exception $ex) {
            $this->rollback();
            throw new Exception($ex->getMessage(), $ex->getCode());
        }
    }
}