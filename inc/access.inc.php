<?php
session_start();
if (isset($_SESSION['loggedIn'])) {
    if(!dbContainUser($_SESSION['name'], $_SESSION['password'])){
        header('Location: '.$_SERVER['HTTP_ORIGIN']);
        exit;
    }
}
