<?php
$pdo = new PDO('mysql:host=localhost;dbname=test','root','1234');
$res = $pdo->exec("DROP DATABASE IF EXISTS `testDB`");
$res = $pdo->exec("CREATE DATABASE `testDB`");

$tables = $pdo->query("SHOW TABLES FROM `test`");
$tables = $tables->fetchAll(PDO::FETCH_COLUMN, 0);

foreach ($tables as $table) {
    $res = $pdo->exec("CREATE TABLE `testDB`.`$table` LIKE `test`.`$table`");
}

define('DB_NAME', 'testDB');

require $_SERVER['DOCUMENT_ROOT']. '/app_load.php';