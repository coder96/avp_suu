<?php

use Models\View\Orosheniye;

$res = Street::newInstance()->listAll();
$streets = $res;

Orosheniye::newInstance()->dao->limit('500')->orderBy('id','desc');
$orosh = Orosheniye::newInstance()->listAll();

$current_reportId = don_current_report_id();
?>
<style>
    .report td, .report th {
        padding: 3px;
    }
</style>
    <div class="box box-primary">
        <div class="box-body">
            <form method="post">
                <h3>Добавить орошение</h3>
                <table>
                    <tr>
                        <td>Лс:</td>
                        <td><input class="form-control" type="number" name="ls" id="ls" value=""></td>
                    </tr>
                    <tr>
                        <td>ФИО:</td>
                        <td><input class="form-control" type="text" name="fio" id="fio"></td>
                    </tr>
                </table>
                <div id="form" style="display: none;">
                    <h3>&nbsp;</h3>
                    <table>
                        <tr>
                            <td>за посев:</td>
                            <td id="posev"></td>
                        </tr>
                        <tr>
                            <td>дата:</td>
                            <td><input type="date" class="form-control" name="date" id="date"
                                       value="<?= date('Y-m-d') ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>сутка:</td>
                            <td><input type="number" class="form-control" name="sutka" id="sutka" value=""></td>
                        </tr>
                        <tr>
                            <td>результат:</td>
                            <td id="result"></td>
                        </tr>
                        <tr>
                            <td>литр/сек:</td>
                            <td><input type="number" class="form-control" id="litr" name="litr" value="" readonly></td>
                        </tr>
                        <tr>
                            <td>сумма:</td>
                            <td><input type="number" class="form-control" id="summa" name="summa" value="" readonly>
                            </td>
                        </tr>
                    </table>
                    <?php Form::button('success') ?>
                </div>
            </form>
            <div id="oroshAbonenta" style="display: none;">

            </div>
            <div>
                <h3>Последние орошении:</h3>
                <table border="1" cellspacing="0" class="report" style="width: auto">
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>лс</th>
                        <th>Ф.И.О.</th>
                        <th>посев</th>
                        <th>га</th>
                        <th>л/с</th>
                        <th>цена</th>
                        <th colspan="2">сумма</th>
                    </tr>
                    <?php
                    $i = 1;
                    while ($orosh->next()){
                        ?>
                        <tr style="font-weight: ">
                            <td><?=$i++?></td>
                            <td><?=$orosh->getId()?></td>
                            <td><?=$orosh->getAbonentLs()?></td>
                            <td><?=$orosh->getFio()?></td>
                            <td><?=$orosh->getName()?></td>
                            <td><?=$orosh->getGa()?></td>
                            <td><?=$orosh->getLs()?></td>
                            <td><?=$orosh->getLs() > 0 ? don_format_price($orosh->getSumma() / $orosh->getLs()) : $orosh->getLs()?></td>
                            <td><?=$orosh->getSumma()?></td>
                            <td><?php if ($current_reportId == $orosh->getReportId()) Form::buttonRed('Отмена','data-id="'.$orosh->getId().'"'); else echo '-'; ?></td>
                        </tr>
                        <?php
                    } ?>
                </table>
            </div>
            <br/>
            <p>
                <a href="..">Назад</a>
            </p>
        </div>
    </div>
<?php
$ekinlar = \Models\Ekin::newInstance()->listAll()->getAll();
$ekin_lit_s = [];
foreach ($ekinlar as $item) {
    $ekin_lit_s[$item['id']] = $item;
}
?>
    <script type="text/javascript">
        var aAbonentEkin = [];
        var ekin_lit_s = <?=json_encode($ekin_lit_s)?>;

        var s = null;
        $('#ls').on('input', function () {
            var obj = $(this);
            clearInterval(s);
            s = setInterval(function () {
                get_oroshenie_form(obj.val());
                clearInterval(s);
            }, 1000);
        });

        function get_oroshenie_form(ls) {
            $.get('/ajax/getAbonentEkin/', {ls: ls}, function (data) {
                var posev = '#posev';
                var form = '#form';
                $(posev).html('');
                if (!data.length){
                    // alert('Абонент или посев не найден');
                    $(form).hide();
                    return;
                }
                aAbonentEkin = [];
                data.forEach(function (v, i) {
                    $('#fio').val(v.fio);
                    var checked = i === 0 ? 'checked' : '';
                    aAbonentEkin[v.id] = v;
                    var str = '<label><input type="radio" name="posev" value="' + v.id + '" ' + checked + '>' + v.name + ' ' + v.i_ga + 'га</label><br/>';
                    $(posev).append(str);
                });
                $(form).show();
                $('#sutka').focus();
            });
        }

        $('#date').change(function (){
            $('#sutka').trigger('input');
        });
        $('#posev, #date').on('change', 'input', function () {
            $('#sutka').trigger('input');
            console.log('trigg');
        });

        $('#sutka').on('input', function () {
            var sutka = $(this).val();
            var ab_ekin = aAbonentEkin[$('input[name=posev]:checked').val()];
            var lit_s = ekin_lit_s[ab_ekin['fk_ekin_id']]['i_ls'];
            var date = $('#date').val();
            $.post('/ajax/getOroshObyom', {sutka: sutka, date: date, ab_ekin: ab_ekin['id']}, function (data) {
                $('#result').html(
                    ab_ekin['name'] + '(' + lit_s + ') * ' + ab_ekin['i_ga'] + ' * ' + sutka + ' = ' + data['lit_s'] + ' * ' + data['tariff']
                );
                $('#litr').val(data['lit_s']);
                $('#summa').val(data['summa']);
            });
        });

        $('.btn.delete').click(function () {
            var id = $(this).data('id');
            if (confirm('Отменить орошение?')){
                window.location.href = '<?=don_url_orosheniye()?>/cancel/'+id;
            }
        });
    </script>
<?php don_ls_fio_autocompleteJs('new', 'get_oroshenie_form(ls);'); ?>