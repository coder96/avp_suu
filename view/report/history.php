<?php

use Frm\FormReport, \Skeletion\Abonent_ekinSkeletion;

$type = __get('type');
$history = __get('history');

$payments = __get('payments');

/**
 * @var $orosheniye \Skeletion\View\OrosheniyeSkeletion;
 */
$orosheniye = __get('orosheniye');
$title = __get('title');

/**
 * @var $abonentHistory \Skeletion\AbonentHistorySkeletion
 */
$abonentHistory = __get('abonentHistory');

$aEkin = [];

/**
 * @var $ekinlar Abonent_ekinSkeletion
 */
$ekinlar = __get('ekinlar');

while ($ekinlar->next()) {
    $aEkin[$ekinlar->getReportId()][] = $ekinlar->toArray();
}

$fromYear = __getParam('from_year');
$toYear = __getParam('to_year');

if (__get('drawHeader') !== false) {
    echo '<div class="box box-primary"><div class="box-body">';
} else {
    ?>
    <h1><?= $title ?></h1>
    <?php
}
if (empty($history) && empty($orosheniye)) {
    echo "ничего не нашлось";
} else {
    ?>
    <table class="table table-bordered table-striped dataTable" style="width: auto;">
        <caption class="h4"><?='с ' . $fromYear . 'г. по ' . $toYear . 'г. '?></caption>
        <tr>
            <th>год</th>
            <th>л/с</th>
            <th>ФИО</th>
            <th>тел №</th>
            <th>паспорт №</th>
        </tr>
        <?php
        while ($abonentHistory->next()) {
            $reportId = $abonentHistory->getReportId();
            $report = \Models\Report::newInstance()->findByPrimaryKey($reportId);
            ?>
            <tr>
                <td><?=$report['year']?></td>
                <td><?=$abonentHistory->getLs()?></td>
                <td><?=$abonentHistory->getFio()?></td>
                <td><?=$abonentHistory->getPhone()?></td>
                <td><?=$abonentHistory->getPassport()?></td>
            </tr>
            <?php
            foreach ((array)$aEkin[$reportId] as $item) {
                $ekin = Abonent_ekinSkeletion::build($item);
                ?>
                <tr>
                    <td colspan="2"></td>
                    <td><?= $ekin->getName() ?></td>
                    <td><?= $ekin->getGa() ?> га</td>
                    <td></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
<?php
    if ($type == 'history' || $type == 'all'){
        FormReport::draw_f1($history, true);
    }
    if (is_array($payments) && count($payments)) {
        ?>
        <br/>
        <table class="table table-bordered dataTable" style="width: auto">
            <caption>Оплаты</caption>
            <tr>
                <th>№</th>
                <th>сумма</th>
                <th>дата</th>
            </tr>
            <?php
            $i = 0;
            foreach ($payments as $item) {
                ++$i;
                ?>
                <tr>
                    <td><?=$i?></td>
                    <td><?=$item['summa']?></td>
                    <td><?=date('d.m.Y',strtotime($item['date']))?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        <br/>
        <?php
    }
    if ($type == 'orosheniye' || $type == 'all'){
        FormReport::draw_orosheniye($orosheniye);
    }
}
back_url();
if (!__get('drawHeader') !== false) {
    echo '</div></div>';
}