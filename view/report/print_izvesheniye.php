<?php

use \Models\Ekin;

$data = __get('result');
$result = $data['report'];
$abonents = $data['abonents'];
$ekinlar = $data['ekinlar'];
$orosheniya = $data['orosheniya'];
$year = __get('year');

$iskakb_name = don_get_preference('iskakb_name');
$iskakb_phone = don_get_preference('iskakb_phone');
$inn = don_get_preference('iskakb_inn');
$text = don_get_preference('text');
$show_balance_2 = don_get_preference('show_balance_2');
$balance_2_name = don_get_preference('balance_2_name');
$show_controller = don_get_preference('show_controller');
$srok = don_get_preference('srok') == '' ? 15 : intval(don_get_preference('srok'));
$srok = date('d.m.Y', strtotime("+$srok day"));

$aEkin = [];
$m_posev = Ekin::newInstance()->listAll();
while ($m_posev->next()) {
    $aEkin[$m_posev->getId()] = $m_posev->toArray();
}

$print_date = date('d.m.Y');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Печать извещении</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<style>
    body {
        margin: 0;
        padding: 0;
    }

    div {
        vertical-align: top;
    }

    ul {
        list-style: none;
        padding: 0;
    }

    p {
        margin: 0;
    }

    .izveshenie {
        display: block;
        padding: 0;
        height: 14.5cm;
        border-top: 1px dashed black;
        border-bottom: 1px dashed black;
        word-break: keep-all;
        border-right: 1px dashed black;
        overflow: hidden;
        margin: 16px 0;
    }

    .izveshenie:first-child {
        margin-top: 0;
    }

    .izveshenie:nth-child(3n) {
        margin-bottom: 0;
    }

    .nowrap {
        white-space: nowrap;
    }

    .izveshenie > li {
        vertical-align: top;
        height: 14.5cm;
        padding-top: 5px;
    }

    .inline-block {
        display: inline-block;
    }

    .for_controller {
        width: 6cm;
        border-right: 1px dashed black;
        border-left: 1px dashed black;
    }

    .for_abonent {
        width: 14.5cm;
    }

    .controller_header {
        /*font-size: 9pt;*/
    }

    .abonent_info li {
        text-align: center;
    }

    .abonent_info td {
        text-align: left;
    }

    .half {
        box-sizing: border-box;
        width: 49%;
    }

    .bold {
        font-weight: bold;
    }

    .right {
        text-align: right !important;
    }

    .center {
        text-align: center !important;
    }

    .h1 {
        width: 4.5cm;
    }

    .h2 {
        width: 6.5cm;
    }

    .small {
        font-size: 10pt;
    }

    .smaller {
        font-size: 8pt;
    }

    .large {
        font-weight: bold;
        font-size: 18pt;
    }

    .big {
        font-weight: bold;
        font-size: 15pt;
    }


    .dashed td {
        border: 1px dashed black;
    }

    .message {
        font-weight: bold;
        height: 88px;
    }

    #logo {
        float: right;
        height: 1.7cm;
        margin-right: 15px;
    }

    table {
        border-top: 1px solid black;
        border-right: 1px solid black;
    }

    table td, table th {
        border-left: 1px solid black;
        border-bottom: 1px solid black;
    }
</style>
<style media="print">
    @page {
        margin: 5.5mm;
        margin-left: 5mm;
        margin-right: 4mm;
    }
</style>
<body>
<?php
foreach ($result as $row) {
    $abonent = $abonents[$row['ls']];
    $b_counter = $abonent['b_counter'];
    $n = 1;
    $ostatok = $abonent['ostNaNach'];
    $oplata = $row['oplacheno'];
    $korrekt = $row['korrekt'];
    $nachisleno = $row['nachislPoTarifu'];
    $ekins = (array)$ekinlar[$abonent['ls']];
    ?>
    <ul class="izveshenie">
        <li class="for_controller inline-block">
            <div class="controller_header">
                <div class="center">
                    <span class="big"><?= $iskakb_name ?></span>
                </div>
                <div>
                    <p class="center">Сугат суусуна эсеп-билдирме</p>
                    <p class="big center"><?php
                        echo "$year - жыл";
                        ?></p>
                </div>
            </div>
            <div>
                <div class="inline-block left">
                    <span class="large">&nbsp;&nbsp;&nbsp;<?= $row['ls'] ?>&nbsp;&nbsp;&nbsp;</span>
                </div>
                <div class="inline-block">
                    <ul>
                        <li><b>чейин төлөңүз</b><br/><?= $srok ?></li>
                    </ul>
                </div>
                <ul class="abonent_info">
                    <li><b><?= $abonent['fio'] ?></b></li>
                    <li>Дареги: <b><?php
                            echo $abonent['ulname'] . ' ';
                            ?></b>
                    </li>
                    <li>Мураб: <?= $abonent['cname'] ?></li>
                    <li style="padding-left:20px;">
                        <br/>
                        <?php
                        if (!empty($ekins)) {
                            ?>
                            <table cellspacing="0">
                                <tr>
                                    <th>
                                        Эгин
                                    </th>
                                    <th>
                                        Га
                                    </th>
                                    <th>
                                        <?= $b_counter ? 'л/с' : 'сумма' ?>
                                    </th>
                                </tr>
                                <?php
                                foreach ($ekins as $key => $ekin) {
                                    ?>
                                    <tr>
                                        <td><?= $ekin['name'] ?></td>
                                        <td><?= $ekin['i_ga'] ?></td>
                                        <td><?= $ekins[$key]['tarif'] = $ekin['i_ga'] * ($b_counter ? $aEkin[$ekin['fk_ekin_id']]['i_ls'] : $aEkin[$ekin['fk_ekin_id']]['d_sum']) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            <?php
                        }
                        ?>
                    </li>
                    <li style="padding-left:20px;">
                        <br/>
                        <table class="" cellspacing="0">
                            <tr>
                                <th>Аталышы</th>
                                <th>сумма</th>
                            </tr>
                            <tr>
                                <td>Жылдын башында</td>
                                <td><?= $ostatok ?></td>
                            </tr>
                            <tr>
                                <td>Эсептелди</td>
                                <td><?= $nachisleno ?></td>
                            </tr>
                            <tr>
                                <td>Төлөндү</td>
                                <td><?= $oplata ?></td>
                            </tr>
                            <?php
                            if ($korrekt <> 0) {
                                ?>
                                <tr>
                                    <td>Кайра эсептелди</td>
                                    <td><?= $korrekt ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                            <tr>
                                <td>Жыйынтык</td>
                                <td><?= $tolonsun = $ostatok + $nachisleno + $korrekt - $oplata ?></td>
                            </tr>
                        </table>
                    </li>
                    <li><br/>Төлөнүүчү: <?= $tolonsun > 0 ? $tolonsun . ' сом' : '-' ?>&nbsp;&nbsp;</li>
                    <li class="bold">Төлөндү: _________&nbsp;&nbsp;&nbsp;&nbsp;</li>
                    <li class="bold ">«_________» <?= date('Y') ?>-ж.&nbsp;&nbsp;&nbsp;&nbsp;</li>
                </ul>
            </div>
        </li>
        <li class="for_abonent inline-block">
            <div>
                <div class="inline-block">
                    <ul>
                        <li>Сугат суусуна<br/>эсеп-дүмүрчөк</li>
                    </ul>
                </div>
                <div class="inline-block center" style="width: 6cm">
                    <ul>
                        <li class="big center"><?php echo "$year - жыл"; ?></li>
                    </ul>
                </div>
                <div class="inline-block" style="float: right;">
                    <ul>
                        <li class="small">Басылган күнү: <span class="bold"><?= $print_date ?></span></li>
                        <li class="small">Чейин төлөңүз: <span class="bold"><?= $srok ?></span></li>
                    </ul>
                </div>
            </div>
            <div>
                <p><span class="big"><?= $iskakb_name ?></span> ИНН <?= $inn ?></p>
            </div>
            <div class="abonent_header">
                <div class="h2 inline-block">
                    <ul>
                        <li><?= '<span class="big">' . $row['ls'] . '</span> ' . '<span class="bold">' . $abonent['fio'] . '</span>' ?></li>
                        <li class="small"></li>
                    </ul>
                </div>
                <div class="inline-block h2">
                    <ul>
                        <li class="">Дареги: <b><?php echo $abonent['ulname'] ?></b></li>
                        <li class="small" style="white-space: nowrap">
                            Мураб: <?= $abonent['cname'] . ($abonent['cphone'] ? " ({$abonent['cphone']})" : '') ?></li>
                    </ul>
                </div>
            </div>
            <div class="abonent_content">
                <div style="float: right;">
                    <ul>
                        <li>
                            <table class="small" cellspacing="0">
                                <?php
                                if ($b_counter) {
                                    echo '<caption><b>Сугарган суулары</b></caption>';
                                    ?>
                                    <tr>
                                        <th>Эгин</th>
                                        <th>га</th>
                                        <th>Тариф</th>
                                        <th>күн</th>
                                        <th>бардыгы</th>
                                        <th>дата</th>
                                    </tr>
                                    <?php
                                    $summa = $summa_sutki = 0;
                                    foreach ($ekins as $ekin) {
                                        if (isset($orosheniya[$ekin['id']])) {
                                            foreach ($orosheniya[$ekin['id']] as $orosh) {
                                                $summa += $orosh['i_summa'];
                                                $sutka = round($orosh['i_ls'] / $ekin['tarif']);
                                                $summa_sutki += $sutka;
                                                ?>
                                                <tr>
                                                    <td><?= $ekin['name'] ?></td>
                                                    <td><?= $ekin['i_ga'] ?></td>
                                                    <td><?= number_format($ekin['tarif'] * $orosh['d_tariff_price'], 1) ?></td>
                                                    <td><?= $sutka ?></td>
                                                    <td><?= $orosh['i_summa'] ?></td>
                                                    <td><?= date('d.m.Y', strtotime($orosh['d_date'])) ?></td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <tr>
                                                <td><?= $ekin['name'] ?></td>
                                                <td><?= $ekin['tarif'] ?></td>
                                                <td>0</td>
                                                <td>0</td>
                                                <td>-</td>
                                                <td>-</td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="3">Жаамы</td>
                                        <td><?= $summa_sutki ?></td>
                                        <td><?= $summa ?></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                } else { ?>
                                    <tr>
                                        <th>Эгин</th>
                                        <th>Тариф</th>
                                        <th>га</th>
                                        <th>сумма</th>
                                    </tr>
                                    <?php
                                    $summa = $summa_ga = 0;
                                    foreach ($ekins as $ekin) {
                                        $summa_ga += $ekin['i_ga'];
                                        $summa += $ekin['tarif'];
                                        ?>
                                        <tr>
                                            <td><?= $ekin['name'] ?></td>
                                            <td><?= $ekin['tarif'] / $ekin['i_ga'] ?></td>
                                            <td><?= $ekin['i_ga'] ?></td>
                                            <td><?= $ekin['tarif'] ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2">Жаамы</td>
                                        <td><?= $summa_ga ?></td>
                                        <td><?= $summa ?></td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </li>
                    </ul>
                </div>
                <div class="inline-block h2">
                    <table class="h2" cellspacing="0">
                        <tr>
                            <td>Аталышы</td>
                            <td>Жаамы</td>
                            <td>Туум</td>
                            <td>Жыйынтык</td>
                        </tr>
                        <tr>
                            <td>Жылдын башында</td>
                            <td><?= $ostatok ?></td>
                            <td></td>
                            <td><?= $ostatok ?></td>
                        </tr>
                        <tr>
                            <td>Эсептелди</td>
                            <td><?= $nachisleno ?></td>
                            <td></td>
                            <td><?= $nachisleno ?></td>
                        </tr>
                        <tr>
                            <td>Төлөндү</td>
                            <td><?= $oplata ?></td>
                            <td></td>
                            <td><?= $oplata ?></td>
                        </tr>
                        <?php

                        if ($korrekt <> 0) {
                            $n--;
                            ?>
                            <tr>
                                <td>Кайра эсептелди</td>
                                <td><?= $korrekt ?></td>
                                <td></td>
                                <td><?= $korrekt ?></td>
                            </tr>
                            <?php
                        }
                        /*
                        if ($show_balance_2 && $row['balance_2'] > 0) {
                            $n--;
                            ?>
                            <tr>
                                <td><?= $balance_2_name ?></td>
                                <td><?= $row['balance_2'] ?></td>
                                <td></td>
                                <td><?= $row['balance_2'] ?></td>
                            </tr>
                        <?php
                        }
                        */
                        for ($i = 0; $i < $n; $i++) {
                            ?>
                            <tr>
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td>Жыйынтык</td>
                            <td><?= $tolonsun ?></td>
                            <td></td>
                            <td><?= $tolonsun ?></td>
                        </tr>
                    </table>
                </div>
                <p class="message small"><?= $text ?></p></div>
            <br/>
            <div style="clear: both"></div>
            <div class="about_iskakb">
                <ul>
                    <?= ($row['phone'] != '' ? '<li>Телефон №: ' . $row['phone'] . '</li>' : '') ?>
                    <li><?= $iskakb_name ?></li>
                    <?= ($iskakb_phone != '' ? '<li>Телефон №: ' . $iskakb_phone . '</li>' : '') ?>
                    <!--<li>Вебсайт: suu.net.kg</li>-->
                </ul>
            </div>
            <br/>
            <br/>
            <div>
                <ul>
                    <li class="bold">Төлөнүүчү: &nbsp;&nbsp;<?= $tolonsun > 0 ? $tolonsun . ' сом' : '-' ?><span style="float: right;">Төлөндү: _________</span></li>
                </ul>
                <ul>
                    <li>Эсеп 3-жыл сакталсын&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        «_________» <?= date('Y') ?>-ж.
                    </li>
                </ul>
            </div>
            </div>
        </li>
    </ul>
<?php }
?>
</body>
</html>
