<?php
$aData = __get('aData');
if (empty($aData)){
    goto empty_data;
}
$tariffs = $aData['tariffs'];
$streets = $aData['streets'];

$f1 = __get('f1');

$year = $f1['year'];
$month = $f1['month'];

?>
<h3>Сведения
    о количестве абонентов и человеков в разрезе улиц
    за <?php echo monthName($month,false). ' ' . $year . 'г' ?>. Форма №4</h3>
<table border="1" cellspacing="0">
    <tr>
        <th rowspan="2">
            №
        </th>
        <th rowspan="2">
            Улица
        </th>
        <th colspan="<?=count($tariffs) + 1?>">
            кол-во<br>абонентов
        </th>
        <th colspan="<?=count($tariffs) + 1?>">
            кол-во<br>чел
        </th>
    </tr>
    <tr>
        <?php
        foreach ($tariffs as $tarif) {
            echo "<th>$tarif</th>";
        }
        ?>
        <th>
            всего
        </th>
        <?php
        foreach ($tariffs as $tarif) {
            echo "<th>$tarif</th>";
        }
        ?>
        <th>
            всего
        </th>
    </tr>
    <?php
    $i = 1;
    $countAll = [];
    foreach ($streets as $street) {
        ?>
        <tr>
            <td><?=$i?></td>
            <td><?=$street['name']?></td>
            <?php
            $ct = 0;
            //po abonentam
            foreach ($tariffs as $id => $name) {
                $kol = (int)$street['kol'][$id]['ct'];
                $ct += $kol;
                $countAll['ab'][$id] += $kol;
                echo "<td>" . $kol . "</td>";
            }
            echo "<td>$ct</td>";
            $ct = 0;
            //po kol chelovek
            foreach ($tariffs as $id => $name) {
                $kol = (int)$street['kol'][$id]['kol'];
                $ct += $kol;
                $countAll['kol'][$id] += $kol;
                echo "<td>" . $kol . "</td>";
            }
            echo "<td>$ct</td>"
            ?>
        </tr>
        <?
        $i++;
    }
    ?>
    <tr>
        <th colspan="2">
            Всего
        </th>
        <?php
        $ct = 0;
        //po abonentam
        foreach ($tariffs as $id => $name) {
            $kol = (int)$countAll['ab'][$id];
            $ct += $kol;
            echo "<th>" . $kol . "</th>";
        }
        echo "<th>$ct</th>";
        $ct = 0;
        //po kol chelovek
        foreach ($tariffs as $id => $name) {
            $kol = (int)$countAll['kol'][$id];
            $ct += $kol;
            echo "<th>" . $kol . "</th>";
        }
        echo "<th>$ct</th>"
        ?>
    </tr>
</table>
<?php
if (false){
    empty_data:
    echo '<h3>Ничего не нашлось</h3>';
}
back_url();
