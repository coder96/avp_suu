<?php
$type = __get('type');

function history()
{
    ?>
    <div class="clearfix"></div>
    <div class="form-group col-md-2">
        <label for="fio">ФИО:</label>
        <input class="form-control" type="text" name="fio" id="fio" autocomplete="off">
    </div>
    <div class="form-group col-md-2">
        <label for="ls">Лиц. счет:</label>
        <input class="form-control" type="number" name="ls" id="ls" required autocomplete="off">
    </div>
    <div class="clearfix"></div>
    <div class="inline-block">
        <?php Form::monthYearSelect('from_', 0, date('Y', strtotime('-1 year'))) ?>
    </div>
    <div class="inline-block">
        <h4 class="col-md-1">до:</h4>
        <?php Form::monthYearSelect('to_') ?>
    </div>
    <div class="clearfix"></div>
    <?php
    don_ls_fio_autocompleteJs();
}

?>
<div class="box box-primary">
    <div class="box-body">
        <form action="" method="get">
            <?php
            if ($type !== 'history' && $type != 'oroshenii') {
                Form::F1_select($type === 'reestr');
            }
            ?>
            <div class="col-md-4">
                <?php
                switch ($type) {
                    case 'oroshenii':
                        ?>
                        <div class="form-group">
                            <label>Выберите канал</label>
                            <?php Form::drawStreetSelect(null, true); ?>
                        </div>
                        <div class="form-group row">
                            <?php Form::yearSelect('', date('Y', strtotime('-1 year')), 'col-md-6') ?>
                        </div>
                        <?php
                    break;
                    case 'form1':
                        ?>
                        <div class="form-group">
                            <label>Выберите канал</label>
                            <?php Form::drawStreetSelect(); ?>
                        </div>
                        <h4>Остаток</h4>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <h5>от:</h5>
                                <input class="form-control " name="dolg_ot">
                            </div>
                            <div class="col-md-6">
                                <h5>до:</h5>
                                <input class="form-control " name="dolg_do">
                            </div>
                        </div>
                        <?php
                        break;
                    case 'history':
                        echo '</div>';
                        history();
                        echo '<div class="col-md-4">';
                        break;
                }

                Form::checkForPrint();
                ?>
                <div class="form-group">
                    <?php Form::buttonPrimary('Показать') ?>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
        <?php back_url('/') ?>
    </div>
</div>