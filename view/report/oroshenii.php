<?php
$result = __get('reportData');

function getOroshClass($sutki) {
    if ($sutki <= 1) {
        $class = 'darkred';
    } elseif ($sutki <= 4) {
        $class = 'red';
    } elseif ($sutki <= 7) {
        $class = 'yellow';
    } elseif ($sutki <= 10) {
        $class = 'lightGreen';
    } else {
        $class = 'green';
    }
    return $class;
}
if (don_is_draw_header()) {
    echo '<div class="box box-primary"><div class="box-body">';
} else {
    echo '<h3>' . __get('title') . '</h3>';
}
?>
    <style>
        .darkred {
            background-color: #f56c6c;
        }
        .red {
            background-color: #f7bebe;
        }
        .yellow {
            background-color: #f0f283;
        }

        .lightGreen {
            background-color: #94d4a6;
        }

        .green {
            background-color: #4c9c63;
        }
    </style>
    <h4><?=Street::newInstance()->findByPrimaryKey(__getParam('ul'))['name']?></h4>
    <table border="1" cellspacing="0">
        <tr>
            <th>
                №
            </th>
            <th>
                л/с
            </th>
            <th>
                ФИО
            </th>
            <th>
                Урожай
            </th>
            <th>
                га
            </th>
            <th>
                кол.<br/>оро-<br>шений
            </th>
            <th>
                сутки
            </th>
        </tr>
        <?php
        $i = 0;
        $prevLs = 0;
        $ctAll = [];
        foreach ($result as $row) {
            ++$i;
            if ($prevLs == $row['ls']) {
                $n = $row['ls'] = $row['fio'] = '';
            } else {
                $prevLs = $row['ls'];
            }
            $ctOrosh = $row['ct_orosh'];
            $sutki = round( $row['sutki'] );
            ++$ctAll[$sutki];
            $class = getOroshClass($sutki);
            ?>
            <tr class="data <?= $class ?>">
                <td>
                    <?= $i ?>
                </td>
                <td>
                    <?= $row['ls'] ?>
                </td>
                <td>
                    <?= $row['fio'] ?>
                </td>
                <td>
                    <?= $row['name'] ?>
                </td>
                <td>
                    <?= $row['i_ga'] ?>
                </td>
                <td>
                    <?= $ctOrosh ?>
                </td>
                <td>
                    <?= $sutki ?>
                </td>
            </tr>
            <?php
        }
        ?>
        <tr>
            <th colspan="7">
                Итого
            </th>
        </tr>
        <?php
        ksort($ctAll);
        foreach ($ctAll as $sutki => $ctEkin) {
            $class = getOroshClass($sutki);
            ?>
            <tr class="<?=$class?>">
                <th colspan="6">
                    <?=$sutki?>-сут.
                </th>
                <th>
                    <?=$ctEkin?>
                </th>
            </tr>
            <?php
        }
        ?>
    </table>
<?php
back_url();
if (don_is_draw_header()) {
    echo '</div></div>';
}