<div class="box box-danger">
    <h3 class="box-header"><?=__get('title')?></h3>
    <div class="box-body">
        <h3 style="color:red">После закрытия отчета не можете изменить данные. Вы уверены?</h3>
        <form action="" method="post">
            <table>
                <tr>
                    <td><input type="submit" class="btn btn-danger" name="sbt" value="Подтвердить"></td>
                    <td><input type="submit" class="btn btn-primary" name="sbt" value="Отменить"></td>
                </tr>
            </table>
        </form>
        <br/>
        <a href="..">Назад</a>
    </div>
</div>
