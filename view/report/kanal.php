<?php
$result = __get('reportData');

$ga = $kolAb = $kolChel = $ostNaNach = $nachislPoTarifu =
$nachislVsego = $oplacheno = $penya = $ostNaKonec = $korrekt = $lit_s = 0;
if (don_is_draw_header()){
    echo '<div class="box box-primary"><div class="box-body">';
} else {
    echo '<h3>'.__get('title').'</h3>';
}
?>
    <table border="1" cellspacing="0">
        <tr>
            <th>
                №
            </th>
            <th>
                Канал
            </th>
            <th>
                Мираб
            </th>
            <th>
                Кол-во<br>абонентов
            </th>
            <th>
                га
            </th>
            <th>
                л/сек
            </th>
            <th>
                Остаток на<br>начало месяца
            </th>
            <th>
                Начислено<br>по тарифу
            </th>
            <th>
                Пеня
            </th>
            <th>
                Оплачено
            </th>
            <th>
                Корректировка
            </th>
            <th>
                Остаток на <br>конец месяца
            </th>
        </tr>
        <?php
        $i = 1;
        foreach ($result as $row) {
            ?>
            <tr>
                <td>
                    <?php echo $i ?>
                </td>
                <td>
                    <?php echo $row['ulname'] ?>
                </td>
                <td>
                    <?php echo $row['cname'] ?>
                </td>
                <td>
                    <?php echo $row['ls'] ?>
                </td>
                <td>
                    <?php echo $row['ga'] ?>
                </td>
                <td>
                    <?php echo (int)$row['lit_s'] ?>
                </td>
                <td>
                    <?php echo don_format_price($row['ostNaNach']) ?>
                </td>
                <td>
                    <?php echo don_format_price($row['nachislPoTarifu']) ?>
                </td>
                <td>
                    <?php echo don_format_price($row['penya']) ?>
                </td>
                <td>
                    <?php echo don_format_price($row['oplacheno'] )?>
                </td>
                <td>
                    <?php echo don_format_price($row['korrekt']) ?>
                </td>
                <td>
                    <?php echo don_format_price($row['ostNaKonec']) ?>
                </td>
            </tr>
            <?php
            $i++;
            $kolAb += $row['ls'];
            $kolChel += $row['kol'];
            $ga += $row['ga'];
            $lit_s += $row['lit_s'];
            $ostNaNach += $row['ostNaNach'];
            $nachislPoTarifu += $row['nachislPoTarifu'];
            $nachislVsego += $row['nachislVsego'];
            $oplacheno += $row['oplacheno'];
            $penya += $row['penya'];
            $ostNaKonec += $row['ostNaKonec'];
            $korrekt += $row['korrekt'];
        }
        ?>
        <tr>
            <th colspan="3">
                Итого
            </th>
            <th>
                <?php echo $kolAb ?>
            </th>
            <th>
                <?php echo don_format_price($ga) ?>
            </th>
            <th>
                <?php echo don_format_price($lit_s,0) ?>
            </th>
            <th>
                <?php echo don_format_price($ostNaNach) ?>
            </th>
            <th>
                <?php echo don_format_price($nachislPoTarifu) ?>
            </th>
            <th>
                <?php echo don_format_price($penya)  ?>
            </th>
            <th>
                <?php echo don_format_price($oplacheno)  ?>
            </th>
            <th>
                <?php echo don_format_price($korrekt)  ?>
            </th>
            <th>
                <?php echo don_format_price($ostNaKonec)  ?>
            </th>
        </tr>
    </table>
<?php
back_url();
if (don_is_draw_header()){
    echo '</div></div>';
}