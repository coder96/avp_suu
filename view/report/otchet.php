<?php

use Frm\FormReport;

$history = __get('history');
$title = __get('title');

if (__get('drawHeader') !== false) {
    echo '<div class="box box-primary"><div class="box-body">';
}
if (empty($history)) {
    echo "ничего не нашлось";
} else {
    FormReport::draw_f1($history);
}
back_url();
if (!__get('drawHeader') !== false) {
    echo '</div></div>';
}