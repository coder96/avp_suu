<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                <form id="abonentForm">
                    <table>
                        <tr>
                            <td>Выберите контроллера:</td>
                            <td>
                                <select class="form-control" name="contrId">
                                    <option value="">-</option>
                                    <option value="">Все контроллеры</option>
                                    <?php
                                    $r1 = Controller::newInstance()->listAll();
                                    foreach ($r1 as $row) {
                                        $selected = $_GET['contrId'] == $row['id'] ? 'selected' : '';
                                        echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['fio'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Выберите участок:</td>
                            <td>
                                <select class="form-control" name="ulId">
                                    <option value="">-</option>
                                    <option value="">Все участки</option>
                                    <?php
                                    $r1 = Street::newInstance()->listAll();
                                    foreach ($r1 as $row) {
                                        $selected = $_GET['ulId'] == $row['id'] ? 'selected' : '';
                                        echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Поиск по л/с:</td>
                            <td>
                                <input type="number" id="ls" name="ls" placeholder="введите л/с" oninput="$(this).change()">
                            </td>
                        </tr>
                    </table>
                </form>
                <p>&nbsp;</p>
                <div id="abonent" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                    <abonent
                            v-bind:abonents="abonents"
                            v-bind:editabonent="edit"
                    ></abonent>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                показано {{abonents.length}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    Vue.component('abonent', {
        props: ['abonents', 'editabonent', 'delete'],
        template:
            '<div class="row">' +
            '    <div class="col-sm-12">' +
            '        <table id="example2" class="table table-bordered table-hover dataTable" role="grid">' +
            '            <thead>' +
            '            <tr role="row">' +
            '                <th>л/с</th>' +
            '                <th>Ф.И.О</th>' +
            '                <th>паспорт №</th>' +
            '                <th>тел №</th>' +
            '                <th>канал</th>' +
            '                <th>ГА</th>' +
            '                <th>долг</th>' +
            '                <th>обновлено</th>' +
            '                <th></th>' +
            '                <th></th>' +
            '            </tr>' +
            '            </thead>' +
            '            <tbody>' +
            '            <tr v-for="(abonent, k) in abonents" v-bind:key="abonent.ls">' +
            '                <td>{{abonent.ls}}</td>' +
            '                <td>{{abonent.fio}}</td>' +
            '                <td>{{abonent.s_passport}}</td>' +
            '                <td>{{abonent.s_phone}}</td>' +
            '                <td>{{abonent.ulname}}</td>' +
            '                <td>{{abonent.ga}}</td>' +
            '                <td>{{abonent.balance}}</td>' +
            '                <td>{{abonent.updated_at}}</td>' +
            '                <td><a  :href="\'<?=don_url_abonent()?>dogovor/\' + abonent.ls">договор</a></td>' +
            '                <td><a  :href="\'<?=don_url_abonent_edit('')?>\' + abonent.ls"><i class="glyphicon glyphicon-pencil"></i></a></td>' +
            '            </tr>' +
            '            </tbody>' +
            '        </table>' +
            '    </div>' +
            '</div>'
    });

    var app = new Vue({
        el: '#abonent',
        data: {
            abonents: [],
            abonent : {},
            frmEditShow : false
        },
        methods : {
            edit : function (key){
                this.frmEditShow = false;
                if (this.abonents[key] !== undefined){
                    this.abonents.key = key;
                    this.abonent = this.abonents[key];
                }
            },
            update : function (key){
                this.abonents[key] = this.abonent;
            }
        },
        created: function () {
        },
        updated: function () {
        },
    });

    $('#abonentForm').on('change', 'select, input', function (){
        var formdata = $('#abonentForm').serialize();
        $.get('/ajax/getAbonent', formdata, function (data) {
            // console.log(data);
            if (data.length){
                app.abonents = data;
            } else {
                app.abonents = [];
            }
        })
    });

    var change = false;
    $.each($('input, select'), function () {
        var name = $(this).attr('name');
        var val = window.localStorage.getItem('abonent_'+name);
        if (val){
            change = true;
            $(this).val(val);
        }
    });
    if (change){
        //для отправки/применения формы
        $('#ls').change();
    }

    $('input, select').change(function () {
        var name = $(this).attr('name');
        var val = $(this).val();
        window.localStorage.setItem('abonent_'+name, val);
    });
</script>