<?php
if ($_SESSION['loggedIn']) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/access.inc.php';
} else {
    header('Location: ' . $_SERVER['HTTP_ORIGIN']);
    exit();
}
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.inc.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/functions.inc.php';
$ls = don_get_insert_ls();
$show_balance_2 = don_get_preference('show_balance_2');
$balance_2_name = don_get_preference('balance_2_name');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Добавить абонента оплату</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="../scripts/jquery-min.js"></script>
        <style>
            .abonent:nth-child(odd) {
                background-color: cadetblue;
            }
            .abonent.red{
                background-color: #cf8095 !important;
            }
            #last_abonent td {
                white-space: nowrap;
            }
        </style>
    </head>
    <body>
        
        <h1>Добавление новых абонентов</h1>
        <div style="color:red" id="errors"></div>
            <table>
                <tr class="ul">
                    <td>Улица:<br>
                        <select id="ul" name="ul" required>
                            <option value="" hidden>Выберите улицу</option>
                            <?php
                            $sql = "select * from ul ORDER  BY name";
                            $result = sql_in($sql);
                            echo 'asdf';
                            foreach ($result as $ul) {
                                echo '<option value="' . $ul['id'] . '">' . $ul['name'] . '</option>'; //Вывод списка улиц
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        Тип начисления:
                    </td>
                    <td>
                            <label>
                                <input class="b_counter" type = "radio" name = "b_counter" value="0" required>
                                По кол. человек
                            </label>
                    </td>
                    <td>
                            <label>
                                <input class="b_counter" type = "radio" name = "b_counter" value="1" required>
                                По счетчику
                            </label>
                    </td>
                </tr>
            </table>
            <table class="abonents" >
                <tr class="abonent">
                    <td>Лс:<br>
                        <input type="number" name="ls" id="ls" required  style="width: 120px;" value="<?=$ls?>"></td>
                    <td>ФИО:<br>
                        <input type="text" name="fio" id="fio" required style="width: 270px;"></td>
                    <td>Дом №:<br>
                        <input type="hidden" name="domNom" >
                    </td>
                    <td>Кол. чел.:<br>
                        <input type="number" name="kol" id="kol"  value="1" style="width: 70px;"></td>
                    <td>Тариф:<br>
                        <?php
                        $result = sql_in("select * from tariffs WHERE name NOT LIKE '%~~%' ");
                        foreach ($result as $key => $tarif) {
                            ?>
                            <label>
                                <input type = "radio" name = "tariff" <?=$key == 0 ? 'checked' : ''?> value="<?php echo $tarif['id'] ?>">
                                <?php echo $tarif['name']; ?>
                            </label>
                            <?php
                        }
                        ?>
                    </td>
                    <td>Долг:<br>
                        <input type="text" name="balance" value="0" style="width: 120px;"></td>
                    <?php if ($show_balance_2){ ?>
                    <td><?=$balance_2_name?>:<br>
                        <input type="text" name="balance_2" value="" style="width: 120px;"></td>
                    <?php } ?>
                    <td>
                        Текущее показание<br>
                        <input class="i_counter" type = "number" name = "i_counter" value="0" />
                    </td>
                </tr>
            </table>
        <p>
        <button id="add">Сохранить</button>
        </p>
        <table style="display:block;float: right ">
            <tr>
                <td>кол: </td><td id="count_all"> 0 </td><td> сумма:</td><td id="summ_all"> 0</td>
            </tr>
        </table>
        <table id="last_abonent" border="1" collspacing="0">
            
        </table>
        <br>
                <a href="..">Назад</a>

        <script type="text/javascript">
            $('input[name=tariff]').eq(2).prop('checked', true);
//            var length = -1;
            var abonents = [];
            var count = 0;
            var sum = 0;
//            $('#ls').change(function(){
//                if (length == -1){
//                    length = $(this).val().length;
//                }
//            });
//            $('#ls').on('input',function(){
//                if ($(this).val().length == length){
//                    $('#fio').focus();
//                }
//            });
            $('#add').click(function(){
                send();
            });
            function send(){
                var data = {};
                data.ajax = 1;
                data.sb = 'Подтвердить';
                data.ul = $('#ul').val();
                data.b_is_commerce = 0;
                data.b_counter = $('.b_counter:checked').val();
                if ($('#fio').val().length < 1){
                    alert('Заполните имя');
                    return;
                }
                if ($('.b_counter:checked').length == 0){
                    alert('Выберите тип начисления');
                    return;
                }
                if (data.ul < 1){
                    alert('Выберите улицу');
                    return;
                }
                var fields = [];
                if ($('.abonents .abonent').length > 0){
                    var inputs = $('.abonents .abonent').eq(0).find('input');
                    $.each(inputs,function(){
                        var type = $(this).attr('type');
                        var name = $(this).attr('name');
                        if (type == 'radio'){
                            if ($(this).prop('checked')){
                                data[name] = $(this).val();
                                fields.push($(this).closest('label').text());
                            }
                        } else {
                            if ($(this).val())
                            fields.push($(this).val());
                            data[name] = $(this).val();
                        }
                    });
                    var cb = parseInt(data.balance);
                    $.post('/abonent/add_post',data,function(data){
                        if (data == '1'){
                            $('#errors').html('');
                            $('.abonents .abonent.red').eq(0).removeClass('red');
                            count++;
                            sum += cb;
                            $('#count_all').text(count);
                            $('#summ_all').text(sum);
                            ab = '<tr><td>'+count+'</td>';
                            $.each(fields,function(i,v){
                                ab += '<td>'+v+'</td>';
                            });
                            ab += '</tr>';
                            $('#last_abonent').prepend(ab);
                            $('.abonent').eq(0).find('input[type=text],input[type=number]').val('');
                            $('.abonents').append($('.abonent').eq(0).clone());
                            $('.abonents .abonent:not(.red)').eq(1).remove();
                            $('#kol').val('1');
                            $.get('/abonent/get_isert_id',function(data){
//                                $('#ls').val(data);
                                $('#ls').val(80);
                                $('input[name=tariff]').eq(0).prop('checked', true);
                                $('#ls').focus();
                            });
//                            $('.abonents .abonent:not(.red) input').eq(0).focus();
                        } else {
                            console.log(data);
                            $('#errors').html(data);
                            $('.abonents .abonent:not(.red)').eq(0).addClass('red');
                        }
                    });
                }
            }
            function ctrlEnter(event)
            {
            if((event.ctrlKey) && ((event.keyCode == '0xA')||(event.keyCode == '0xD')))
                {
                    $('#add').click();
                }
            }
            $(document).keypress(function(event){
                if((event.ctrlKey) && ((event.keyCode == '0xA')||(event.keyCode == '0xD')))
                {
                    $('#add').click();
                }
            });
        </script>
    </body>
</html>