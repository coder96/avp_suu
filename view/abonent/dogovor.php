<?php
/**
 * @var $abonent \Skeletion\AbonentSkeletion
 */
$abonent = __get('abonent');
$ekin = \Models\Abonent_ekin::newInstance()->findByLs($abonent->getLs());
$ga = 0;
while ($ekin->next()){
    $ga += $ekin->getGa();
}
?>
<style>
    body {
        font-size: 10pt;
        padding-bottom: 50px;
    }
</style>
<div class="text-center text-bold">КЕЛИШИМ №<?=$abonent->getLs()?></div>
<div>Кашкар-Кыштак айылы <span style="float: right; font-weight: bold"><?='«'.date('d').'» ' . mb_strtolower(monthName(date('m'), false, true)) .  ' '.date('Y')?>-жыл</span></div>
<div class="clear"></div>
<div>1-тарап  «С.Жалалдинов» атындагы СПАнын жетекчиси Халилов Мирпозил уставтын негизинде иш алып баруучу,
    2-тарап       атындагы дыйкан чарбанын  жетекчиcи  <b><u><?=$abonent->getFio()?></u></b>
жер аянты   <b><?=don_format_price($ga)?> <u>га</u></b> уставтын негизинде иш алып баруучу болуп ушул келишим тузулду. Келишимдин максаты оз ара кызмат корсотуу.</div>
<div class="text-center text-bold">I.СПАнын МИЛДЕТТЕРИ</div>
<ol>
    <li>1-тарап «С.Жалалдинов» СПАсы дыйкан чарбанын жазуу турундо кайрылуусунун негизинде сугат суусун жеткирип беруу  учун кызмат корсотот.</li>
    <li>Корсоткон кызматы учун ар бир  м3 суу  учун  IV-I кварталда <b>6.95 тыйын</b>,  II-III кварталда <b>8.95 тыйын</b> кызмат акы алат.</li>
    <li>СПАнын мурабы дыйкан чарба жетекчиси суунун акысын алдын ала тологондон кийин  суу  берет. Заявканын негизинде сууну жеткирип берет.</li>
    <li>Суу берилгенден кийин СПАнын суу олчомун контроль кылат жана сууну жеткирип берууго милдетуу.</li>
    <li>Дыйкан чарба  жер кыртышын бузуп, оз алдынча суунун коломун озгортуусуно тыюу салынат. Алынган же ашыкча кетирген суу учун Кыргыз Республикасынын №233 28.04.99-ж токтомунун 5.4 пунктуна негиз жана СПАнын советинин токтомуна негиз   дыйкандар сууну оз башымчалык менен уурулук кылгандарга 1000 сомдон 3000 сомго дейре жазана колдонулат.</li>
    <li>Эгерде СПАнын куносу менен дыйкан чарбага сууну оз убагында жеткирбесе акттын негизинде расчет эсептелинип, СПА тарабынан толонуп берилет.</li>
    <li>Кыргыз Республикасынын Окмоту жана Дуйнолук Онуктуруу Ассосациясы тарабынан реаблитация иштери буткондугуно байланыштуу дыйкан чарба пайдаланган ар бир гектар жерине 2009-2015-жылдарга
        <b>АКТ СВЕРКА жасоого милдеттенет.</b></li>
    <li>Каналдардын жээгиндеги тал-теректерди 3-4метр аралыкта эгууго тыйуу салат жана эгилген тал-теректерди алып таштайт.</li>
</ol>
<div class="text-center text-bold">II. ДЫЙКАН ЧАРБАНЫН МИЛДЕТТЕРИ:</div>
<ol>
    <li>Дыйкан чарба озунун аймагындагы арык жана пайнап арыктарды чаап, тазалап суу алууга даярдайт, чабылбаган арыктарга суу берилбейт жана чабылбаган арыктарга штраф толонот.</li>
    <li>Суу алуудан мурун 5 кун мурун алдын ала мурабга жазуу турундо заявка берилет жана алынуучу суунун толомун толонгондугу туралуу квитанция корсотулгондон кийин кезек- кезеги менен суу берилет.</li>
    <li>Дыйкан чарбалар башка дыйкан чарбанын жерин арендага же сатып алса ошол чарбанын карызы жок деген тактама корсотуусу тийиш.  Ошондой эле дыйкан чарбанын сугат сууга эски карызы болсо, толомду толук тологондон кийин гана келишим тузулуп суу берилет.</li>
    <li>Ар бир дыйкан эгилген эгиндин ок-арыктары 100-150 метр кыркып суу коюуга милденттендирилет.</li>
    <li>Дыйкан чарба экинчи эгинди эгуудон мурун СПАнын жетекчисине кайрылуу керек.</li>
    <li>Дыйкан суу кызмат акысын суу алган кундон 30 кун ичинде толобосо, бир кечиккен кун учун толобогон сумманын 0.5% олчомундо айып толойт.</li>
    <li>Келишим тузуп жаткан учурда чарбалар  аралык  каналдарды ремонтоо учун дыйкан чарбалар алдын- ала суу акысынын 30% тин толосу керек.</li>
    <li>Каналдардын жээгиндеги тал-теректерди 3-4метр аралыкта эге албайт жана эгилген тал-теректерин алып таштайт</li>
</ol>
<div class="text-center text-bold">III.  ТАРАПТАРДЫН ЖООПКЕРЧИЛИГИ.</div>
<ol>
    <li>Келишимде корсотулгон милдеттенмелер аткарылбай калса, эки тараптын макулдашуусу менен чечилет. Соттошу учун кеткен чыгымды айыптуу  тарап толоп берет.</li>
    <li>Келишимге оз башымчалык менен озгортуулор киргизууго мумкун эмес.</li>
    <li>Келишим эки нускадан тузулуп бирдей юридикалык кучко ээ.</li>
    <li>Келишим тараптар кол койгон кундон баштап оз ара эсептешуу бутконго чейин өз күчүнө ээ.</li>
</ol>

<div class="text-center text-bold">КОШУМЧА ШАРТТАР.</div>
<ol>
    <li>Мурдагы жылдар учун дыйкан чарбалардын сугат сууга карызы болсо, СПА карыз толук толонгонго чейин суу берууну токтотуп кое алат.</li>
    <li>Кандайдыр бир форс- мажордук кырдаалдар (орт, суу ташкыны, жер титироо, бунт) келип чыккан учурларда беруулордун макулдашылган мооноттору, бул кырдаалдар болуп отмойунчо, бирок анда абал келишимдин шарттарын же анын болугун аткарууга туздон туз таасир эткен мезгилдерде гана узартылат.</li>
    <li>«Аткаруучу» форс-мажордук кырдаалдардын башталышы жана аяктагандыгы жонундо «Заказчыга»  5 кундон кечиктирбестен дароо билдирет.</li>
</ol>
<div class="text-center text-bold">ТАРАПТАРДЫН ДАРЕГИ:</div>
<div style="float: left;">
    «С.Жалалдинов» атындагы<br/>
    СПАнын жетекчиси:<br/>
    Халилов М.<br/>
    Мураб:  ______________<br/>
    Тел.№<br/>
    Колу:  ________________<br/>
</div>
<div style="float: right;">
Аты жону: <b><?=$abonent->getFio()?></b><br/>
Паспорт:  <b><?=$abonent->getPassport()?></b><br/>
Дареги:      _________________________<br/>
Тел.№:   <b><?=$abonent->getPhone()?></b><br/>
Колу:   ____________________________<br/>
</div>
<p style="clear: both">&nbsp;</p>