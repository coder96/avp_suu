<?php

use \Skeletion\AbonentSkeletion;
use \Skeletion\Abonent_ekinSkeletion;


$ls = don_get_insert_ls();
/**
 * @var $abonent AbonentSkeletion
 */
$abonent = __get('abonent');
if (!$abonent instanceof AbonentSkeletion){
    $abonent = AbonentSkeletion::build();
}
/**
 * @var $ekin Abonent_ekinSkeletion
 */
$ekin = __get('ekin');
if (!$ekin instanceof Abonent_ekinSkeletion){
    $ekin = Abonent_ekinSkeletion::build();
}

$action = __get('form_action');
$edit = stristr($action, 'edit');
$sb = $edit ? 'Обновить' : 'Добавить';
$class = stristr($action, 'edit') ? 'primary' : 'success';
?>
<div class="box box-<?=$class?>">
    <div class="box-body">
        <form method="post" action="" id="frm_add_abonent">
            <input type="hidden" name="controller" value="abonent"/>
            <input type="hidden" name="action" value="<?=$action?>"/>
            <table>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td>Канал:</td>
                    <td>
                        <?php Form::drawStreetSelect($abonent->getUl(), true) ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td>Лс:</td>
                    <td><input type="number" class="form-control" autocomplete="off" name="ls" value="<?= $abonent->getLs() ? $abonent->getLs() : $ls ?>" required <?=$abonent->getLs() ? 'readonly' : ''?>></td>
                </tr>
                <tr>
                    <td>ФИО:</td>
                    <td><input type="text" class="form-control" autocomplete="off" name="fio" value="<?=$abonent->getFio()?>" required></td>
                </tr>
                <tr>
                    <td>Паспорт №:</td>
                    <td><input type="text" class="form-control" autocomplete="off" id="passport" name="s_passport" value="<?=$abonent->getPassport()?>"></td>
                </tr>
                <tr>
                    <td>Номер телефона:</td>
                    <td><input type="text" class="form-control" autocomplete="off" id="phone" name="s_phone" value="<?=$abonent->getPhone()?>"></td>
                </tr>
                <tr class="dogovor_tr">
                    <td>Номер документа:</td>
                    <td><input type="text" class="form-control" autocomplete="off" name="s_dogovor" value="<?=$abonent->getDogovor()?>"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td>Корректировка:</td>
                    <td><input type="text" class="form-control" autocomplete="off" name="balance" value="0"></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Тарификация:</td>
                </tr>
                <tr>
                    <td><label><input type="radio" name="b_counter" value="1" <?=!$edit && don_logged_user_id() == 22 || $edit && $abonent->getBCounter() ? 'checked' : ''?>> Литр/сек</label></td>
                    <td><label><input type="radio" name="b_counter" value="0" <?=!$edit && don_logged_user_id() != 22 || $edit && !$abonent->getBCounter() ? 'checked' : ''?>> Годовой</label></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="ekin">
                            <tr>
                                <th colspan="2">посев</th>
                                <th>га</th>
                            </tr>
                            <?php
                            while ($ekin->next()){
                                $oroshenie = \Models\Orosheniye::newInstance()->listWhere(['fk_abonent_ekin_id' => $ekin->getId()])->getAll();
                                //ekinlar
                                $id = $ekin->getId();
                                echo '<tr>';
                                Form::ekin("e_$id", $ekin->getEkinId(), $ekin->getGa(), $ekin->getNum(), count($oroshenie) > 0);
                                echo '</tr>';
                                ?>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="add_ekin btn btn-primary"><i class="glyphicon glyphicon-plus"></i> Земля</button>
                        <hr>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?php Form::button($class)?>
                    </td>
                </tr>
            </table>
        </form>
        <table>
            <tr style="display: none;" id="ekin_add">
                <?php Form::ekin()?>
            </tr>
        </table>
        <br/>
        <br/>
        <a href=".." class="btn">Назад</a>
    </div>
</div>
<script src="<?=don_url_js('inputmask.min.js')?>"></script>
<script type="text/javascript">
    var selector_add_ekin = '.add_ekin';
    $('#phone').inputmask('0 (999) 99-99-99');
    $('.b_counter').change(function () {
        console.log($(this).val());
    });
    $(selector_add_ekin).click(function(e) {
        e.preventDefault();
        $('.ekin').append($('#ekin_add').clone().show());
        $('.ekin_ga').inputmask({regex:'[0-9]+[\.\,][0-9]{2}'});
    });
    <?php if ($action !== 'edit') { ?>
    $(selector_add_ekin).click();
    <?php } ?>
    $('input').on('keypress', function (e) {
        if (e.key === 'Enter'){
            e.preventDefault();
            $(this).closest('form').submit();
            return false;
        }
    });
    $('#frm_add_abonent').on('click','.cancel_ekin', function (e) {
        e.preventDefault();
        if (confirm('Удалить землю?')) $(this).closest('tr').remove();
    });
</script>