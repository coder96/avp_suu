<?php
$title = __get('title');
$drawHeader = __get('drawHeader') === '' ? TRUE : FALSE;
?><!DOCTYPE html>
<html>
<head>
    <title><?= $title ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="theme-color" content="#428bca"/>
    <script type="text/javascript" src="<?= don_url_js('jquery-min.js') ?>"></script>
    <script type="text/javascript" src="<?= don_url_js('jquery-ui-1.12.1/jquery-ui.min.js') ?>"></script>
    <script type="text/javascript" src="<?= don_url_js('bootstrap.min.js') ?>"></script>
    <link rel="stylesheet" href="<?= don_url_js('jquery-ui-1.12.1/jquery-ui.min.css') ?>"/>
    <link rel="stylesheet" href="<?= don_url_js('jquery-ui-1.12.1/jquery-ui.theme.min.css') ?>"/>
    <link rel="stylesheet" href="<?= don_url_js('jquery-ui-1.12.1/jquery-ui.structure.min.css') ?>"/>
    <link rel="stylesheet" href="<?= don_url_css('bootstrap.min.css') ?>"/>
    <link rel="stylesheet" href="<?= don_url_css('main.css') . '?' . filemtime(don_path_css('main.css')) ?>"/>
    <script type="text/javascript"
            src="<?= don_url_js('main.js') . '?' . filemtime(don_path_js('main.js')) ?>"></script>
    <meta name="viewport" content="width=device-width, user-scalable=1"/>
    <style>
        .messages {
            list-style: none;
            padding: 0;
            padding-left: 20px;
            font-size: 30px;
        }

        .message_ok {
            color: green;
        }

        .message_error {
            color: red;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <header class="main-header">
    </header>
    <?php
    $messages = osc_get_flash_message();
    if (!empty($messages)) {
        foreach ($messages as $m) {
            $class = $m['type'] == 'error' ? 'danger' : 'success';
            ?>
            <div class="alert alert-<?= $class ?>" role="alert">
                <strong><?= $m['msg'] ?></strong>
            </div>
            <?php
        }
    }
    ?>
    <div class="container">
        <style>
            html,
            body {
                height: 100%;
            }

            body {
                display: -ms-flexbox;
                display: -webkit-box;
                display: flex;
                -ms-flex-align: center;
                -ms-flex-pack: center;
                -webkit-box-align: center;
                align-items: center;
                -webkit-box-pack: center;
                justify-content: center;
                padding-top: 40px;
                padding-bottom: 40px;
                background-color: #f5f5f5;
            }

            .form-signin {
                width: 100%;
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }

            .form-signin .checkbox {
                font-weight: 400;
            }

            .form-signin .form-control {
                position: relative;
                box-sizing: border-box;
                height: auto;
                padding: 10px;
                font-size: 16px;
            }

            .form-signin .form-control:focus {
                z-index: 2;
            }

            .form-signin input[type="email"] {
                margin-bottom: -1px;
                border-bottom-right-radius: 0;
                border-bottom-left-radius: 0;
            }

            .form-signin input[type="password"] {
                margin-bottom: 10px;
                border-top-left-radius: 0;
                border-top-right-radius: 0;
            }

            .mt-5, .my-5 {
                margin-top: 3rem !important;
            }
        </style>
        <script>
            $('body').addClass('text-center');
        </script>
        <form class="form-signin" action="" method="post">
            <?php
            echo osc_csrf_token_form();
            $messages = osc_get_flash_message();
            if (!empty($messages)) {
                foreach ($messages as $m) {
                    $class = $m['type'] == 'error' ? 'danger' : 'success';
                    ?>
                    <div class="alert alert-<?= $class ?>" role="alert">
                        <strong><?= $m['msg'] ?></strong>
                    </div>
                    <?php
                }
            }
            if (__get('wait') == '') {
                ?>
                <img class="mb-4" src="<?= don_url_images('logo.png') ?>" alt="" width="72" height="72">
                <h1 class="h3 mb-3 font-weight-normal">Пожалуйста войдите в систему</h1>
                <label for="inputEmail" class="sr-only">Логин</label>
                <input type="text" name="name" id="inputEmail" class="form-control" placeholder="логин"
                       value="<?php echo $login; ?>" required="" autofocus="">
                <label for="inputPassword" class="sr-only">Пароль</label>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password"
                       required="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
                <?php
            } ?>
            <p class="mt-5 mb-3 text-muted">©<?=date('Y')?></p>
        </form>
    </div>
</div>
<!-- footer end -->
</body>
</html>
