<?php 
$form = __getSavedFrom();

$title = 'Добавить пользователя';
$action = 'add_post';
$submit_text = 'Добавить';
$edit = false;
    
if (__exist('user')){
    $edit = TRUE;
    
    $user = __get('user');
    if (empty($form)){
        $form = $user;
        $form['name'] = $user['s_name'];
        $form['i_rule'] = [];
        $form['i_rule'] = array_keys($user['rules']);
    }
    
    $title = 'Обновить данные пользователя';
    $action = 'edit_post';
    $submit_text = 'Обновить';
}

//prepare data
if (is_array($form)){
    foreach ($form as $k => $v) {
        $$k = $v;
    }
}

if (!is_array($i_rule)){
    $i_rule = [];
}
?>
<h3><?=$title?></h3>
<form method="post" action="/users/">
    <input type="hidden" name="action" value="<?=$action?>"/>
    <input type="hidden" name="pk_i_id" value="<?=$form['pk_i_id']?>" />
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">ФИО</h3>
            </div>
            <div class="panel-body">
                <input type="text" name="fio" class="form-control" value="<?= $fio ?>" required/>
            </div>
            <div class="panel-heading">
                <h3 class="panel-title">Должность</h3>
            </div>
            <div class="panel-body row">
                <div class="col-md-4 text-center">
                    <label><input class="form-control" type="checkbox" name="i_rule[]" value="3" <?= in_array(3, $i_rule) ? 'checked' : ''?> >Контроллер</label>
                </div>
                <div class="col-md-4 text-center">
                    <label><input class="form-control" type="checkbox" name="i_rule[]" value="4" <?= in_array(4, $i_rule) ? 'checked' : ''?>>Кассир</label>
                </div>
                <div class="col-md-4 text-center">
                    <label><input class="form-control" type="checkbox" name="i_rule[]" value="5" <?= in_array(5, $i_rule) ? 'checked' : ''?>>Бухгалтер</label>
                </div>
            </div>
            <div class="panel-heading">
                <h3 class="panel-title">Блокировка</h3>
            </div>
            <div class="panel-body row text-center">
                <label><input class="form-control" type="checkbox" name="i_block" value="1" <?= $i_block ? 'checked' : ''?> ><?= $i_block ? 'Вход заблокирован' : 'Заблокировать вход'?></label>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Данные входа</h3>
            </div>
            <div class="panel-body">
                логин:
                <input type="text" name="name" class="form-control" required value="<?= $name ?>"/>
            </div>
            <div class="panel-body">
                пароль(должен состоять из латинских букв и цифр, не менее 8 символов):
                <input type="password" name="password_1" <?=$edit ? '' : 'required'?> class="form-control"/>
                повторите пароль:
                <input type="password" name="password_2" <?=$edit ? '' : 'required'?> class="form-control"/>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-4">
        <input class="btn btn-primary" type="submit" value="<?=$submit_text?>" />
    </div>
</form>

<div class="clearfix"></div>
<br/>
<?php
back_url();
