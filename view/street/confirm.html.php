<?php
if ($_SESSION['loggedIn']) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/access.inc.php';
} else {
    header('Location: ' . $_SERVER['HTTP_ORIGIN']);
    exit();
}
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.inc.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/functions.inc.php';

$readonly = "readonly";
if (isset($_GET['ls'])) {
    $title = "Обновлении данных абонента";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Улица</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            input[readonly]{
                background-color: darkcyan;
                color:white;
            }
            input:not([type='submit']):not([type='radio']){
                width: 250px;
            }
        </style>
    </head>
    <body>
        <h1><?php echo $title ?></h1>
        <form action="<?php echo $action ?>" method="get">
            <table>
                <?php
                if (isset($_GET['id'])) {
                    if (is_numeric($_GET['id'])) {
                        $result = sql_in('select * from ul where id=' . $_GET['id']);
                        $row = $result->fetch();
                        if ($row) {
                            $_GET['name'] = $row['name'];
                            $_GET['id'] = $row['id'];
                            $_GET['contrId'] = $row['contrId'];
                            $sb = 'Обновить';
                            $readonly = "";
                        } else {
                            echo 'Abonent ne nayden!';
                            exit();
                        }
                    } else {
                        echo 'Nepravilniy id ulitsy!';
                        exit();
                    }
                }
                ?>
                <tr>
                    <td>Название канала:</td>
                    <td><input type="text" name="name" value="<?php echo $_GET['name'] ?>" <?php echo $readonly ?>>
                        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" <?php echo $readonly ?>></td>
                </tr>
                <tr>
                    <td>Мираб:</td>
                    <td>
                        <select name="contrId" required>
                            <?php
                            $sql = "select * from controllers";
                            $result = sql_in($sql);
                            foreach ($result as $ul) {
                                $selected = "";
                                if ($_GET['contrId'] == $ul['id']) {
                                    $selected = "selected";
                                }
                                echo '<option value="' . $ul['id'] . '" ' . $selected . ' >' . $ul['fio'] . '</option>'; //Вывод списка улиц
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="sb" value="<?php echo $sb ?>">
                        <br><br>
                        <input type="submit" onclick="history.back()" value=" Назад ">
                    </td>
                    <td></td>
                </tr>
            </table>
        </form><br>
    </body>
</html>
