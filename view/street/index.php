<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <div id="street" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                    <street v-bind:streets="streets"></street>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                показано {{streets.length}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    Vue.component('street', {
        props: ['streets'],
        template:
            '<div class="row">' +
            '    <div class="col-sm-12">' +
            '        <table id="example2" class="table table-bordered table-hover dataTable" role="grid">' +
            '            <thead>' +
            '            <tr role="row">' +
            '                <th>id</th>' +
            '                <th>имя</th>' +
            '                <th colspan="2">контроллер</th>' +
            '            </tr>' +
            '            </thead>' +
            '            <tbody>' +
            '            <tr v-for="street in streets">' +
            '                <td>{{street.id}}</td>' +
            '                <td>{{street.name}}</td>' +
            '                <td>{{street.cname}}</td>' +
            '                <td><a  :href="\'<?=don_url_street_edit()?>\' + street.id"><i class="glyphicon glyphicon-pencil"></i></a></td>' +
            '            </tr>' +
            '            </tbody>' +
            '        </table>' +
            '    </div>' +
            '</div>'
    });

    var app = new Vue({
        el: '#street',
        data: {
            streets: [],
        },
        created: function () {
        },
        updated: function () {
        },
        methods: {}
    });

    app.streets =<?=json_encode(Street::newInstance()->listAll())?>;
</script>