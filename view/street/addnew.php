<?php 
$title = __get('title');
$action = __get('form_action');
$street = __get('street');
if (!is_array($street)){
    $street = [];
}
$sb = stristr($action, 'edit') ? 'Обновить' : 'Добавить';
$class = stristr($action, 'edit') ? 'primary' : 'success';
?>

<div class="row">
    <div class="col-lg-6">
        <div class="box box-<?=$class?>">
            <div class="box-body">
                <h3><?=$title?></h3>
                <form action="" method="post">
                    <table>
                        <tr>
                            <td>Название улицы:</td>
                            <td>
                                <input type="text" name="name" required value="<?=$street['name']?>" />
                                <input type="hidden" name="id" value="<?=$street['id']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Мираб:</td>
                            <td>
                                <?php Form::DrawControllersSelect($street['contrId'],TRUE) ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><button class="btn btn-<?=$class?>"><?=$sb?></button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<?php back_url()?>
