<?php
$result = __get('reestr');
?>
        <h3><?php echo 'Реестр оплат сводная' ?></h3>
        <table border='1' cellspacing='0'>
            <tr>
                <th>
                    №
                </th>
                <th>
                    № пач.
                </th>
                <th>
                    Кол-во док.
                </th>
                <th>
                    Сумма
                </th>
            </tr>
            <?php
            $summa = 0;
            $count_dok = 0;
            foreach ($result as $i => $row) {
                ?>
                <tr>

                    <td>
                        <?php echo $i+1; ?>
                    </td>
                    <td>
                        <?php echo $row['numPachki'] ?>
                    </td>
                    <td>
                        <?php echo $row['numDok'] ?>
                    </td>
                    <td>
                        <?php echo don_format_price($row['summa']) ?>
                    </td>
                </tr>
            <?php 
                $summa += $row['summa'];
                $count_dok += $row['numDok'];
            }
            ?>
                <tr>

                    <th colspan="2">
                        Итого:
                    </th>
                    <td>
                        <?= $count_dok?>
                    </td>
                    <td>
                        <?= don_format_price($summa) ?>
                    </td>
                </tr>
        </table>
        <a href="<?= don_url_reestr()?>/?type=svodniy">Назад</a>
