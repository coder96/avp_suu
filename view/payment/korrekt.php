<?php
?>
    <div class="box box-primary">
        <div class="box-body">
            <form action="<?= don_url_payment() ?>korrekt" method="post">
                <input type="hidden" name="action" value="korrekt_post"/>
                <table>
                    <tr>
                        <td>Лиц. счет:</td>
                        <td><input class="form-control" type="number" name="ls" id="ls" value="" autofocus required>
                            <input type="hidden" name="type" value="korrekt" required></td>
                    </tr>
                    <tr>
                        <td>ФИО:</td>
                        <td><input class="form-control" type="text" name="fio" id="fio" value="<?php echo $ls; ?>" autocomplete="off"></td>
                    </tr>
                    <tr>
                        <td>Сумма корректирования:</td>
                        <td><input class="form-control" type="number" name="summa" id="summa" value="" required></td>
                    </tr>
                    <tr>
                        <td>Текущий баланс:</td>
                        <td id="current_balance"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><?php Form::buttonPrimary('Корректировать') ?></td>
                    </tr>
                </table>
            </form>
            <a href="..">Назад</a>
        </div>
    </div>
<?php don_ls_fio_autocompleteJs('summa'); ?>