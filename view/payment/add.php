<?php
$show_balance_2 = don_get_preference('show_balance_2');
$balance_2_name = don_get_preference('balance_2_name');
$date = __get('date');
$np = __get('np');
$last_payments = __get('last_payments');

if (!user_has_rule(RULE_GLAVNIY)) {
    ?>
    <p>Ваш текущий баланс: <?= get_current_user_balance() ?></p>
    <?php
}
?>
<div class="row col-lg-8">
    <div class="box box-primary">
        <div class="box-body">
            <h3>Внести оплату</h3>
            <form action="<?= don_url_payment() ?>" method="post">
                <table>
                    <tr>
                        <td>Дата:</td>
                        <td><input class="form-control" type="datetime-local" name="date" value="<?= $date ?>" required>
                        </td>
                    </tr>
                    <tr>
                        <td>№ пачки:</td>
                        <td><input class="form-control" type="number" name="numPachki" value="<?= $np ?>" required></td>
                    </tr>
                    <tr>
                        <td>Лиц. счет:</td>
                        <td><input class="form-control" type="number" name="ls" id="ls" autofocus required
                                   autocomplete="off" value="<?= __getSess('ls') ?>"/></td>
                    </tr>
                    <tr>
                        <td>ФИО:</td>
                        <td><input class="form-control" type="text" name="fio" id="fio" autocomplete="off"></td>
                    </tr>
                    <tr>
                        <td>Сумма оплаты:</td>
                        <td><input class="form-control" type="number" name="summa" id="summa"
                                   value="<?= __getSess('summa') ?>" required min="0" step="0.01"></td>
                    </tr>
                    <tr>
                        <td>Текущий баланс:</td>
                        <td id="current_balance"></td>
                    </tr>
                    <?php if ($show_balance_2) { ?>
                        <tr>
                            <td><?= $balance_2_name ?> долг:</td>
                            <td id="current_balance_2"></td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="2"><br><input class="btn btn-primary" type="submit" name="sb" value="Оплатить"/>
                        </td>
                    </tr>
                </table>
            </form>
            <br><br>
            <a href="..">Назад</a>
            <?php
            if (is_array($last_payments)) {
            $n = count($last_payments);
            $show_cancel = TRUE;
            reestr_header($last_payments[0]['numPachki'], '', true);
            foreach ($last_payments as $i => $row) {
                $count_dok++;
                $summ += $row['summa'];
                ?>
                <tr>

                    <td>
                        <?php echo $n; ?>
                    </td>
                    <td><a <?= href($row['pk_i_id'], $show_cancel) ?>>
                            <?php echo $row['ls'] ?></a>
                    </td>
                    <td>
                        <?php echo $row['fio'] ?>
                    </td>
                    <td>
                        <?php echo $row['summa'] ?>
                    </td>
                    <td>
                        <?php echo $row['date'] ?>
                    </td>
                    <td>
                        <?php echo $row['name'] ?>
                    </td>
                    <td>
                        <a href="/payment/check/<?= $row['pk_i_id'] ?>" target="_blank">чек</a>
                    </td>
                </tr>
                <?php
                $n--;
            }
            reestr_footer($count_dok, $summ, true);
            ?>
            </table>
        </div>
    </div>
</div>
    <script>
        $('.cancel').click(function () {
            var name = $(this).closest('td').next('td').text().trim();
            var sum = $(this).closest('td').nextAll('td').eq(1).text().trim();
            if (!confirm(name + ' - ' + sum + '. Отменить оплату? '))
                return false;
        });
    </script>
<?php
}
?>
<?php don_ls_fio_autocompleteJs(); ?>
