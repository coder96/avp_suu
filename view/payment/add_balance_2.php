<?php
session_start();
if ($_SESSION['loggedIn']) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/access.inc.php';
} else {
    header("Location: http://".$_SERVER['HTTP_HOST']);
    exit();
}
if ($message == ''){
    $message = $_SESSION['message'];
    $_SESSION['message'] = '';
}
$balance_2_name = don_get_preference('balance_2_name');
?>
	<span style="color:red;font-size:16pt;"><?=$message?></span>
        <h3>Добавление "<?=$balance_2_name?>" абоненту</h3>
        <form action="<?= don_url_payment()?>" method="post">
            <input type="hidden" name="action" value="add_balance_2_post" />
            <table>
                <tr>
                    <td>Лиц. счет:</td>
                    <td><input type="text" name="ls" id="ls" autofocus value="<?php echo $ls; ?>" required autocomplete="off"></td>
                </tr>
                <tr>
                    <td>ФИО:</td>
                    <td><input type="text" name="fio" id="fio" autofocus value="<?php echo $ls; ?>" autocomplete="off"></td>
                </tr>
                <tr>
                    <td><?=$balance_2_name?>:</td>
                    <td><input type="text" name="summa_balance_2" id="balance_2"  value=""></td>
                </tr>
                <tr>
                    <td>Текущий баланс:</td>
                    <td id="current_balance"></td>
                </tr>
                <tr>
                    <td><?=$balance_2_name?> долг:</td>
                    <td id="current_balance_2"></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="sb" value="Оплатить"></td>
                </tr>
            </table>
        </form>
        <br><br>
        <a href="..">Назад</a>
        <?php don_ls_fio_autocompleteJs('balance_2'); ?>