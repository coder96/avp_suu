<div class="box box-primary">
    <form>
        <div class="box-body">
            <?php Form::F1_select(); ?>
            <div class="form-group">
                <label class="text-bold"><input type="checkbox" name="svodniy" value="1"
                        <?=Session::newInstance()->_getForm('svodniy') ? 'checked' : ''?>>
                    Сводный
                </label>
            </div>
            <?php Form::checkForPrint() ?>
        </div>
        <div class="box-footer">
            <?php Form::buttonPrimary('Показать') ?>
        </div>
    </form>
</div>