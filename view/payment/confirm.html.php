<?php
session_start();
if ($_SESSION['loggedIn']) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/access.inc.php';
} else {
    header("Location: http://" . $_SERVER['HTTP_HOST']);
    exit();
}
if (!isset($_POST['ls'])) {
    header("Location: .");
}
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/functions.inc.php';

$show_balance_2 = don_get_preference('show_balance_2');
$balance_2_name = don_get_preference('balance_2_name');

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Подтвердить оплату</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            input[readonly]{
                background-color: darkcyan;
                color:white;
            }
        </style>
    </head>
    <body>
        <h2><?php
            if (isset($message)) {
                echo $message;
            }
            ?></h2>
        <form action="?pay" method="post">
            <table>
<?php if (isset($_POST['np'])) {
    ?>
                    <tr>
                        <td>№ пачки:</td>
                        <td><input type="number" name="np" value="<?php echo $_POST['np']; ?>" readonly></td>
                    </tr>
                    <tr>
                        <td>№ документа:</td>
                        <td><input type="number" name="nd"  value="<?php echo $_POST['nd']; ?>" readonly>
                            <input type="hidden" name="id_otcheta" value="<?php echo $_GET['otchetid'] ?>"</td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td>Лиц. счет:</td>
                    <td><input type="number" name="ls" value="<?php echo $_POST['ls']; ?>" readonly></td>
                </tr>
                <tr>
                    <td>ФИО:</td>
                    <td><input type="text" value="<?php
                        if (strlen($_POST['fio']) > 0) {
                            echo $_POST['fio'];
                        } else {
                            htmlout($row['fio']);
                        }
                        ?>" readonly></td>
                </tr>
                <tr>
                    <td><?=$row['balance']>0 ? 'Текущий долг:' : 'Текущая переплата:'?></td>
                    <td><input type="number" value="<?php htmlout($row['balance']) ?>" readonly></td>
                </tr>
                <tr>
                    <td>Сумма <?=isset($_POST['np']) ? 'оплаты':'корректировки'?>:</td>
                    <td><input type="number" name="summa" value="<?php echo $_POST['summa']; ?>" readonly></td>
                </tr>
                <?php if ($show_balance_2) { ?>
                <tr>
                    <td><?=$balance_2_name?>:</td>
                    <td><input type="text" name="summa_balance_2"  value="<?php echo $_POST['summa_balance_2']; ?>" readonly></td>
                </tr>
                <?php } ?>
                <tr>
                    <td><input type="submit" name="sb" value="<?php echo $action ?>"></td>
                    <td><?php if ($action!='Отменить' && isset($_POST['np'])){ ?><input type="submit" name="sb" value="Изменить"><?php } ?></td>
                </tr>
            </table>
        </form>
        <br><br>
        <a href="." >Назад</a>
    </body>
</html>
