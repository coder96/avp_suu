<?php
$payment = __get('payment');
$inn = don_get_preference('iskakb_inn');
$iskakbName = don_get_preference('iskakb_name');
$iskakbPhone = don_get_preference('iskakb_phone');
?>
<html>
<head>
    <meta charset="utf-8">
    <meta content="text/html;charset=utf-8" http-equiv="content-type">
    <meta content="AGSM" name="description">
    <title>Чек <?=$payment['fio']?></title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .receipt1 {
            position: absolute;
            left: 110px;
            top: 30px;
            width: 250px;
            font-size: 8pt;
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }
    </style>
</head>
<body>
<div id="receipt-body">
    <table border="0" cellspacing="0" cellpadding="0" style="width:250px;font-size:8pt;">
        <tbody>
        <tr>
            <td colspan="2" style="border-bottom:2px solid black;vertical-align:top;">
                <h2><?= $iskakbName ?></h2>
                <?php if ($inn != '') { ?>
                    <span style="font-size:8pt;">ИНН <?= $inn ?></span><br>
                    <?php
                } ?>
                <!--                <span style="font-size:8pt;">suu.net.kg</span>-->
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold;padding-top:5px;" colspan="2"><span
                        style="display:inline-block;width:90px;margin-right:15px;">№ пачки:</span><b><?= $payment['numPachki'] ?></b>
            </td>
        </tr>
        <tr>
            <td style="font-weight:bold;padding-top:5px;" colspan="2"><span
                        style="display:inline-block;width:90px;margin-right:15px;">ID платежа:</span><b><?= $payment['pk_i_id'] ?></b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <span style="display:inline-block;width:90px;margin-right:15px;">Чек:</span><?= date('Y-m-d', strtotime($payment['date'])) . '-' . $payment['numPachki'] . '-' . $payment['pk_i_id'] ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <span style="display:inline-block;width:90px;margin-right:15px;">От:</span><?= $payment['fio'] ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <span style="display:inline-block;width:90px;margin-right:15px;">Лицевой счет:</span><?= $payment['ls'] ?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><span
                        style="display:inline-block;width:90px;margin-right:15px;">Сумма:</span><?= $payment['summa'] ?>
                сом
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <span style="display:inline-block;width:90px;margin-right:15px;">Дата платежа:</span><?= date('d.m.Y H:i',strtotime($payment['date'])) ?>
            </td>
        </tr>
        <?php if ($iskakbPhone != ''){ ?>
            <tr>
                <td colspan="2">
                    <span style="display:inline-block;width:90px;margin-right:13px;">Справочная:</span>
                    <?=$iskakbPhone?>
                </td>
            </tr>
        <?php
        } ?>
        <tr>
            <td colspan="2">
                <span style="display:inline-block;width:90px;margin-right:13px;">Кассир:</span>
                <?=$_SESSION['fio']?>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-bottom: 1px solid black">
                &nbsp;
            </td>
        </tr>
        </tbody>
    </table>
    <img src="//suu.net.kg/uploads/images/oplacheno.png" style="position: absolute;top: 170px; left: 40px;"></div>
<script>
    window.print();
    window.addEventListener('afterprint', e => window.close());
</script>
</body>
</html>