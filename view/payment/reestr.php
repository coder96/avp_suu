<?php
$res = __get('reestr');
$users = __get('users');
$show_cancel = __get('show_cancel');
$count_dok = 0;
$summ = 0;
$n = 0;
$p = $res[0];
$kassir = isset($users[$p['fk_i_user_id']]) ? $users[$p['fk_i_user_id']]['fio'] : '';
$draw_header = don_is_draw_header();
if ($draw_header){
    echo '<div class="box box-primary"><div class="box-body">';
}
reestr_header($p['numPachki'], $kassir, $draw_header);
foreach ($res as $i => $row) {
    $count_dok++;
    $summ += $row['summa'];
    $n++;
    ?>
    <tr>

        <td>
            <?php echo $n; ?>
        </td>
        <td><span data-<?= href($row['pk_i_id'], $show_cancel) ?>>
                <?php echo $row['ls'] ?></span>
        </td>
        <td>
            <?php echo $row['fio'] ?>
        </td>
        <td>
            <?php echo $row['summa'] ?>
        </td>
        <td>
            <?php echo substr($row['date'],0,10); ?>
        </td>
        <td>
            <?php echo $row['name'] ?>
        </td>
        <?php if ($draw_header) { ?>
        <td>
            <a href="/payment/check/<?= $row['pk_i_id'] ?>" target="_blank">чек</a>
        </td>
        <?php } ?>
    </tr>
    <?php
    $p = $res[$i + 1];
    if ($row['numPachki'] != $p['numPachki']) {
        reestr_footer($count_dok, $summ, $draw_header);
        $count_dok = 0;
        $summ = 0;
        $n = 0;
        if (isset($p)) {
            $kassir = isset($users[$p['fk_i_user_id']]) ? $users[$p['fk_i_user_id']]['fio'] : '';
            reestr_header($p['numPachki'], $kassir, $draw_header);
        }
    }
}
?>
</table>
<?php
back_url();
if ($draw_header){
    echo '</div></div>';
}
?>
<script>
    $('.cancel').click(function () {
        var name = $(this).closest('td').next('td').text().trim();
        var sum = $(this).closest('td').nextAll('td').eq(1).text().trim();
        if (!confirm(name + ' - ' + sum + '. Отменить оплату? ')){
            return false;
        } else {
            window.location.href = $(this).attr('data-href');
        }
    })
</script>
