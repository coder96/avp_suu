<?php
$title = __get('title');
$action = __get('form_action');
$tariff = __get('tariff');
if (!is_array($tariff)) {
    $tariff = [];
}
$sb = stristr($action, 'edit') ? 'Обновить' : 'Добавить';
$class = stristr($action, 'edit') ? 'primary' : 'success';
?>
<div class="row">
    <div class="col-lg-6">
        <div class="box box-<?=$class?>">
            <div class="box-body">
                <form action="" method="post">
                    <table>
                        <tr>
                            <td>Название тарифа:</td>
                            <td>
                                <input class="form-control" type="text" name="name" required value="<?=$tariff['name']?>" />
                                <input class="form-control" type="hidden" name="id" value="<?=$tariff['id']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Цена:</td>
                            <td>
                                <input class="form-control" type="number" name="price" step="0.01" value="<?=$tariff['price']?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><button class="btn btn-<?=$class?>"><?=$sb?></button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <?php back_url()?>
    </div>
</div>

