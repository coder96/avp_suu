<div class="row">
    <div class="col-lg-6">
        <div class="box box-primary">
            <div class="box-body">
                <div id="tariff" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                    <tariff v-bind:tariffs="tariffs"></tariff>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                показано {{tariffs.length}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    Vue.component('tariff', {
        props: ['tariffs'],
        template:
            '<div class="row">' +
            '    <div class="col-sm-12">' +
            '        <table id="example2" class="table table-bordered table-hover dataTable" role="grid">' +
            '            <thead>' +
            '            <tr role="row">' +
            '                <th>имя</th>' +
            '                <th colspan="2">цена</th>' +
            '            </tr>' +
            '            </thead>' +
            '            <tbody>' +
            '            <tr v-for="tariff in tariffs">' +
            '                <td>{{tariff.name}}</td>' +
            '                <td>{{tariff.price}}</td>' +
            '                <td><a  :href="\'<?=don_url_tariff_edit()?>\' + tariff.id"><i class="glyphicon glyphicon-pencil"></i></a></td>' +
            '            </tr>' +
            '            </tbody>' +
            '        </table>' +
            '    </div>' +
            '</div>'
    });

    var app = new Vue({
        el: '#tariff',
        data: {
            tariffs: [],
        },
        created: function () {
        },
        updated: function () {
        },
        methods: {}
    });

    app.tariffs = <?=json_encode([
        [
            'id' => 'II-III',
            'name' => 'II-III',
            'price' => don_get_tarif_23()
        ],
        [
            'id' => 'IV-I',
            'name' => 'IV-I',
            'price' => don_get_tarif_41()
        ]
    ])?>;
</script>