<style>
    fieldset {
        display: inline-block;
        width: 250px;
        min-height: 150px;
        vertical-align: top;
    }

    fieldset > div > div {
        margin-bottom: 15px;
    }
</style>
<div class="form box box-primary">
    <div class="box-body">
        <form method="post" id="form">
            <fieldset>
                <legend>Основные</legend>
                <div>
                    <h3>Настройки извещений</h3>
                    <div>
                        Вид извещения:<br>
                        <label>
                            <input type="radio"
                                   name="file_print" <?php echo don_get_preference('file_print') == 1 ? 'checked' : '' ?>
                                   value="1"/>
                            старый 4 х 1
                        </label><br>
                        <label>
                            <input type="radio"
                                   name="file_print" <?php echo don_get_preference('file_print') == 2 ? 'checked' : '' ?>
                                   value="2"/>
                            новый 4 х 1 (как на э-энергию)
                        </label><br>
                        <label>
                            <input type="radio"
                                   name="file_print" <?php echo don_get_preference('file_print') == 3 ? 'checked' : '' ?>
                                   value="3"/>
                            3 х 1
                        </label><br>
                    </div>
                    <div>
                        <label>
                            <input type="checkbox"
                                   name="izveshenie_pereplatam" <?php echo don_get_preference('izveshenie_pereplatam') ? 'checked' : '' ?> />
                            Не выводить для "Переплата"(у которых нет долга)
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="checkbox"
                                   name="izveshenie_toktotulgan" <?php echo don_get_preference('izveshenie_toktotulgan') ? 'checked' : '' ?> />
                            Не выводить для "Токтотулган"
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="checkbox"
                                   name="show_controller" <?php echo don_get_preference('show_controller') ? 'checked' : '' ?> />
                            Показать телефонный номер контроллера в извещений
                        </label>
                    </div>
                    <div>
                        <label>
                            Срок оплаты абонентам (чейин толонуз), на днях
                            <input type="number" name="srok"
                                   value="<?php echo don_get_preference('srok') == '' ? '15' : don_get_preference('srok') ?>"/>
                        </label>
                    </div>

                    <div>
                        <label>
                            Текст в извещении
                            <textarea name="text" rows="5"
                                      cols="40"><?php echo don_get_preference('text') == '' ? 'Өз убактында төлөөңүз үчүн ИСКАКБ сизге ыраазычылык билдирет' : don_get_preference('text') ?></textarea>
                        </label>
                    </div>
                    <h3>Сортировка извещений</h3>
                    <div>
                        <div>
                            <label>
                                <input type="radio"
                                       name="sorting_izv" <?php echo don_get_preference('sorting_izv') == '' || don_get_preference('sorting') == 'fio' ? 'checked' : '' ?>
                                       value="fio"/>
                                По алфавиту
                            </label>
                        </div>
                        <div>
                            <label>
                                <input type="radio"
                                       name="sorting_izv" <?php echo don_get_preference('sorting_izv') == 'ls' ? 'checked' : '' ?>
                                       value="ls"/>
                                По л/с
                            </label>
                        </div>
                        <div>
                            <label>
                                <input type="radio"
                                       name="sorting_izv" <?php echo don_get_preference('sorting_izv') == 'domNom' ? 'checked' : '' ?>
                                       value="domNom"/>
                                По дом №
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <h3>Настройки отчета</h3>
                    <div>
                        <div>
                            <label>
                                <input type="checkbox"
                                       name="show_dom_num" <?php echo don_get_preference('show_dom_num') ? 'checked' : '' ?> />
                                Показать номер дома в Ф-№1
                            </label>
                        </div>
                        <div>
                            <label>
                                <input type="checkbox"
                                       name="show_phone" <?php echo don_get_preference('show_phone') ? 'checked' : '' ?> />
                                Пок-ть н-р телефона абонентов в Ф-№1
                            </label>
                        </div>
                    </div>
                    <div>
                        <div>
                            <label>
                                <input type="checkbox"
                                       name="show_balance_2" <?php echo don_get_preference('show_balance_2') ? 'checked' : '' ?> />
                                Показать второй счет в отчете Ф-№1, в извещении и в оплате
                            </label>
                        </div>
                    </div>
                    <div>
                        <label for="balance_2_name">Название второго счета</label>
                        <div>
                            <input type="text" name="balance_2_name"
                                   value="<?= don_get_preference('balance_2_name') ?>"/>
                        </div>
                    </div>
                </div>
                <div>
                    <h3>Пеня</h3>
                    <div>
                        <div>
                            <label>
                                <input type="checkbox"
                                       name="penya" <?php echo don_get_preference('penya') ? 'checked' : '' ?> />
                                Включить пеню
                            </label>
                        </div>
                    </div>
                    <div>
                        <label>% - пени </label>
                        <div>
                            <input type="number" name="penya_proc" step="0.01"
                                   value="<?= don_get_preference('penya_proc') ?>"/>
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Функционал программы</legend>
                <h3>Сортировка в отчетах</h3>
                <div>
                    <div>
                        <label>
                            <input type="radio"
                                   name="sorting" <?php echo don_get_preference('sorting') == '' || don_get_preference('sorting') == 'fio' ? 'checked' : '' ?>
                                   value="fio"/>
                            По алфавиту
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="radio"
                                   name="sorting" <?php echo don_get_preference('sorting') == 'ls' ? 'checked' : '' ?>
                                   value="ls"/>
                            По л/с
                        </label>
                    </div>
                    <div>
                        <label>
                            <input type="radio"
                                   name="sorting" <?php echo don_get_preference('sorting') == 'domNom' ? 'checked' : '' ?>
                                   value="domNom"/>
                            По дом №
                        </label>
                    </div>
                </div>
                <h3>Лицевой счет и Ф.И.О.</h3>
                <div>
                    <div>
                        <label>
                            <input type="checkbox"
                                   name="autocomplete_ls" <?php echo don_get_preference('autocomplete_ls') ? 'checked' : '' ?> />
                            Подсказать лицевой счет
                        </label>
                    </div>
                </div>
                <div>
                    <div>
                        <label>
                            <input type="checkbox"
                                   name="autocomplete_fio" <?php echo don_get_preference('autocomplete_fio') ? 'checked' : '' ?> />
                            Подсказать ФИО
                        </label>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Организация</legend>
                <div>
                    <label>Название</label>
                    <div>
                        <input type="text" name="iskakb_name" value="<?= don_get_preference('iskakb_name') ?>"/>
                    </div>
                </div>
                <div>
                    <label>ИНН</label>
                    <div>
                        <input type="text" name="iskakb_inn" value="<?= don_get_preference('iskakb_inn') ?>"/>
                    </div>
                </div>
                <div>
                    <label>Адрес</label>
                    <div>
                        <input type="text" name="iskakb_address" value="<?= don_get_preference('iskakb_address') ?>"/>
                    </div>
                </div>
                <div>
                    <label>Телефон</label>
                    <div>
                        <input type="text" name="iskakb_phone" value="<?= don_get_preference('iskakb_phone') ?>"/>
                    </div>
                </div>
            </fieldset>
            <br>
            <br>
            <div>
                <div>
                    <input type="submit" name="sb" value="Сохранить"/>
                </div>
            </div>
        </form>
        <br>
        <br>
        <a href="..">Назад</a>
    </div>
</div>

<script>
    setTimeout(function () {
        if ($('#message').text() != '') {
            $('#message').slideUp();
        }
    }, 2000);
    $('#form').submit(function () {
        $.each($('input[type=checkbox]:not(:checked)'), function () {
            var name = $(this).attr('name');
            $('#form').append('<input type="hidden" name="' + name + '" value="0" />')
        });
    });
</script>