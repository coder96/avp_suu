<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.11.2018
 * Time: 20:11
 */

$action = __get('form_action');

?>
<style>
    .delete {
        display: inline-block;
        width: 20px;
        text-align: center;
        cursor: pointer;
        float: right;
        color: white;
        background-color: #d2322d;
    }
</style>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#for_street">Для улицы</a></li>
        <li><a data-toggle="tab" href="#for_abonent">Для абонента</a></li>
        <li><a data-toggle="tab" href="#other">Другое</a></li>
    </ul>
    <div class="tab-content">
        <div id="for_street" class="tab-pane fade in active">
            <form action="<?=$action?>street" method="get">
                <h4>&nbsp;</h4>
                <div class="form-group row">
                    <div class="col-xs-2">
                        Выберите месяц
                    </div>
                    <div class="col-xs-4">
                        <input type="month" name="date" required value="<?= date('Y-m') ?>">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-2">
                        Выберите улицу
                    </div>
                    <div class="col-xs-4">
                        <?php
                        Form::drawStreetSelect(null, false, 'ul[]', '<option value="0">Все улицы</option>', 'multiple style="height: 250px; width: 200px;"');
                        ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-2">
                        <input type="submit" name="sb" value="Найти">
                    </div>
                </div>
            </form>
        </div>
        <div id="for_abonent" class="tab-pane fade">
            <form action="<?=$action?>abonent" method="get">
                <h4>&nbsp;</h4>
                <div class="form-group row">
                    <div class="col-xs-2">
                        Выберите месяц
                    </div>
                    <div class="col-xs-4">
                        <input type="month" name="date" required value="<?= date('Y-m') ?>">
                    </div>
                </div>
                <?php
                Form::drawAbonentLsAndFio(
                        <<<here
$('#ul_abonents').append("<li style='display: none' class='new_row'><input type='hidden' name='ls[]' value='"+ls+"'>"+ls+": "+fio+" <span class='delete'>✖</span></li>");
row_pasted();
here
                );
                ?>
                <div class="form-group row">
                    <div class="col-xs-4">
                        <ul id="ul_abonents">
                        </ul>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-2">
                        <input type="submit" name="sb" value="Найти">
                    </div>
                </div>
            </form>
        </div>
        <div id="other" class="tab-pane fade">
            <h4>&nbsp;</h4>
            <div class="form-group row">
                <a class="btn btn-primary" href="/izvesheniye/empty" role="button">Печать пустой извещении</a>
            </div>
            <div class="form-group row">
                <a class="btn btn-primary" href="/izvesheniye/kvitantsiya" role="button">Печать квитанции</a>
            </div>
        </div>
    </div>
    <script>
        $('#ls').attr('required', false);
        $('#ls').change(function () {
            // $(this).closest('form').submit();
        });

        $('#ul_abonents').on('click','.delete', function(){
            var li = $(this).closest('li');
            li.slideUp();
            setTimeout(function(){
                li.remove();
            },500);
        });

        function row_pasted(){
            $.each($('.new_row'), function(){
                $(this).slideDown().removeClass('new_row');
            })
            $('#ls').val('');
        }
    </script>
<?php
back_url();