<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.12.2018
 * Time: 11:36
 */
$iskakb_name = don_get_preference('iskakb_name');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Печать извещении</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        ul {
            list-style: none;
            padding: 0;
        }

        div {
            overflow: hidden;
        }

        table {
            background-color: black;
        }
        td {
            background-color: white;
            font-size: 8pt;
        }

        .order {
            height: 9cm;
            width: 20cm;
            margin: 5px;
            border-bottom: 2px dashed #2b2b2b;
        }

        .block {
            display: inline-block;
            vertical-align: top;
        }

        .block-1 {
            width: 11cm;
        }

        .block-2 {
            overflow: visible;
            width: 0.5cm;
            height: 9cm;
            margin-left: 20px;
            margin-right: -20px;
        }

        .block-2 div {
            overflow: visible;
            transform: rotate(90deg);
            transform-origin: left top 0;
        }

        .block-2 span {
            font-size: 8pt;
        }

        .block-3 {
            width: 8.0cm;
        }
    </style>
</head>
<body>
<?php
for ($i = 1; $i <= 3; $i++) {
    ?>
    <div class="order">
        <div class="block block-1">
            <ul>
                <li class="name">
                    <?= $iskakb_name ?>
                </li>
                <li class="title">
                    КИРИШ КАССАЛЫК ОРДЕРИ №
                </li>
                <li class="table">
                    <table border="0" cellspacing="1">
                        <tr>
                            <td style="width: 60px"></td>
                            <td>Кабарлоочу эсеп,<br>субэсеп</td>
                            <td>Аналитикалык<br> эсептин шифери</td>
                            <td>Суммасы</td>
                            <td>Максаттуу<br>дайындоонун<br>шифери</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </li>
                <li class="ls">
                    Э~эсеп № __________________
                </li>
                <li class="from">
                    <div class="block" style="width: 8cm; word-break: break-all">
                        <div class="block">Кабыл алынды </div>
                        <div class="block" style="width: 196px; border-bottom: 1px solid black">&nbsp;</div>
                        <div style="width: 100%; border-bottom: 1px solid black">&nbsp;</div>
                    </div>
                    <div class="block" style="width: 2.8cm">
                        <table border="0" cellspacing="1" style="width:100%">
                            <tr>
                                <td>&nbsp;күн&nbsp;</td>
                                <td>&nbsp;&nbsp;ай&nbsp;</td>
                                <td>жыл</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><?= substr(date('Y'), 0, 3) . '_ -ж.' ?></td>
                            </tr>
                        </table>
                    </div>
                </li>
                <li class="description">
                    Негизи: Таза ичимдик суу үчүн
                </li>
                <li class="summa">
                    _________ сом<br>
                    ______________________________________________________
                </li>
                <li class="tirkeme">
                    Тиркеме: _____________________________________________
                </li>
                <li class="kassir">
                    б. эсепчи __________________________________________
                    Көзөмөлдөөчү ____________________________
                </li>
            </ul>
        </div>
        <div class="block block-2">
            <div style="white-space: nowrap">✂- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            </div>
        </div>
        <div class="block block-3">
            <ul>
                <li class="name">
                    <div><?= $iskakb_name ?></div>
                </li>
                <li>
                    Кириш кассалык ордеринин
                </li>
                <li>
                    ДҮМӨРЧӨГҮ №
                </li>
                <li>
                    &laquo;____&raquo; _______________ <?= substr(date('Y'), 0, 3) . '_ -ж.' ?>
                </li>
                <li>
                    Э~эсеп № __________________________
                </li>
                <li>
                    Кабыл алынды ______________________<br>_______________________
                </li>
                <li>
                    Негизи: Таза ичимдик суу үчүн
                </li>
                <li>
                    _____________ СОМ
                </li>
                <li>________________________________</li>
                <li>М.О.</li>
                <li>
                    Б. эсепчи ____________ ____________________
                </li>
                <li>
                    Көзөмөлдөөчү _________ ____________________
                </li>
            </ul>
        </div>
    </div>
    <?php
}
?>
</body>