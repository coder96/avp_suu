<?php
$users = __get('users');
$result = __get('list');
$title = __get('title');
?>
<h3><?= $title ?></h3>
<div class="row">
    <?php if (!empty($result)) { ?>
        <table border="1" cellspacing="0" class="list">
            <tr>
                <th>
                    №
                </th>
                <th>
                    ФИО
                </th>
                <th>
                    Сумма
                </th>
                <th>
                    Дата
                </th>
                <th>
                    Баланс после оплаты
                </th>
            </tr>
            <?php
            foreach ($result as $i => $row) {
                ?>
                <tr>
                    <td>
                        <?php echo $i + 1 ?> 
                    </td>
                    <td>
                        <?php echo $users[$row['fk_i_user_id']]['fio'] ?>
                    </td>
                    <td>
                        <?php echo $row['d_summ'] ?>
                    </td>
                    <td>
                        <?php echo $row['d_date'] ?>
                    </td>
                    <td>
                        <?php echo $row['d_balance'] ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    <?php
    } else {
        echo 'Нет никаких оплат';
    }
    ?>
</div>
<br/>
<br/>
<?php
    back_url();