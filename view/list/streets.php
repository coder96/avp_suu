<?php
$result = __get('list');
$title = __get('title');
?>
<h3><?= $title ?></h3>
<table border="1" cellspacing="0" class="list">
    <tr>
        <th>
            №
        </th>
        <th>
            Название
        </th>
        <th>
            Контроллер
        </th>
        <th>
            Действие
        </th>
    </tr>
    <?php
    foreach ($result as $row) {
        $i++;
        ?>

        <tr>
            <td>
                <?php echo $i ?>
            </td>
            <td>
                <?php echo $row['name'] ?>
            </td>
            <td>
                <?php echo $row['cname'] ?>
            </td>
            <td>
                <a href="<?= don_url_street_edit($row['id']) ?>" >изменить</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<a href="..">Назад</a>
