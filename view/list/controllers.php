<?php
$result = __get('list');
$title = __get('title');
?>
<h3><?= $title ?></h3>
<table border="1" cellspacing="0" class="list">
    <tr>
        <th>
            №
        </th>
        <th>
            ФИО
        </th>
        <th>
            Тел. №
        </th>
        <th>
            Кол-во<wbr> улиц
        </th>
        <th>
            Действие
        </th>
    </tr>
    <?php
    foreach ($result as $row) {
        $i++;
        ?>

        <tr>
            <td> 
                <?php echo $i ?> 
            </td>
            <td>
                <?php echo $row['fio'] ?>
            </td>
            <td>
                <?php echo $row['phone'] ?>
            </td>
            <td style="text-align: center;">
                <?php echo $row['count'] ?>
            </td>
            <td>
                <a href="<?= don_url_controller_edit($row['id']) ?>" >изменить</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<a href="..">Назад</a>