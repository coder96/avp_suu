<?php
$result = __get('users');
$title = __get('title');
?>
<h3><?= $title ?></h3>
<div class="row">
    <?php if (!empty($result)) { ?>
        <table border="1" cellspacing="0" class="list">
            <tr>
                <th>
                    №
                </th>
                <th>
                    ФИО
                </th>
                <th>

                </th>
                <th>

                </th>
            </tr>
            <?php
            foreach ($result as $row) {
                $i++;
                ?>
                <tr>
                    <td>
                        <?php echo $i ?> 
                    </td>
                    <td>
                        <?php echo $row['fio'] ?>
                    </td>
                    <td>
                        <?php echo $row['rule'] ?>
                    </td>
                    <td>
                        <a href="/users/edit/<?=$row['pk_i_id']?>" >изменить</a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    <?php
    } else {
        echo 'У вас нет подключенных пользователей';
    }
    ?>
</div>
<br/>
<br/>
<?php
    back_url();