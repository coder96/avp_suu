<?php
$result = __get('list');
$title = __get('title');
$ls = __getParam('ls');
?>

<ul class="nav nav-tabs">
    <li class="<?=$ls == '' ? 'active' : ''?>"><a data-toggle="tab" href="#for_street">Список по улицам</a></li>
    <li class="<?=$ls == '' ? '' : 'active'?>"><a data-toggle="tab" href="#for_abonent">л/с или ФИО</a></li>
</ul>
<div class="tab-content">
    <div id="for_street" class="tab-pane fade in <?=$ls == '' ? 'active' : ''?>">
        <h3>&nbsp;</h3>
        <form>
            <table>
                <tr>
                    <td>Выберите тип абонентов:</td>
                    <td>
                        <label>
                            <input class="b_is_commerce" type = "radio" name = "commercial"  value="0" <?= $_GET['commercial'] == 0 ? 'checked' : '' ?> required>
                            Физический
                        </label><br>
                        <label>
                            <input class="b_is_commerce" type = "radio" name = "commercial" value="1" <?= $_GET['commercial'] == 1 ? 'checked' : '' ?> required>
                            Коммерческий
                        </label><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr/></td>
                </tr>
                <tr>
                    <td>Вывод по:</td>
                    <td >
                        <label><input type="radio" name="schetchik" value="0" <?= $_GET['schetchik'] == 0 ? 'checked' : '' ?>/>тарифам</label><br>
                        <label><input type="radio" name="schetchik" value="1" <?= $_GET['schetchik'] == 1 ? 'checked' : '' ?> />счетчикам</label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><hr/></td>
                </tr>
                <tr>
                    <td>Выберите контоллера:</td>
                    <td>
                        <select name="contrId">
                            <option value="">Все контроллеры</option>
                            <?php
                            $r1 = sql_in('select * from controllers');
                            foreach ($r1 as $row) {
                                $selected = $_GET['contrId'] == $row['id'] ? 'selected' : '';
                                echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['fio'] . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Выберите улицу:</td>
                    <td>
                        <select name="ulId">
                            <option value="">Все улицы</option>
                            <?php
                            $r1 = sql_in('select * from ul');
                            foreach ($r1 as $row) {
                                $selected = $_GET['ulId'] == $row['id'] ? 'selected' : '';
                                echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['name'] . '</option>';
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input name="sb" type="submit" value="Поиск"/></td>
                </tr>
            </table>
        </form>
    </div>
    <div id="for_abonent" class="tab-pane fade <?=$ls !== '' ? 'in active' : ''?>">
        <h3>&nbsp;</h3>
            <form>
                <?php
                Form::drawAbonentLsAndFio();
                ?>
                <div class="col-xs-2">
                    <input type="submit" value="Найти">
                </div>
            </form>
        <div class="clearfix"></div>
    </div>
</div>
<h4>&nbsp;</h4>
<?php if (count($result) > 0) { ?>
    <table border="1" cellspacing="0" class="table list">
        <tr>
            <th>
                №
            </th>
            <th>
                Л/с
            </th>
            <th>
                ФИО
            </th>
            <th>
                Дом №
            </th>
            <th>
                тел. №
            </th>
            <th style="writing-mode: vertical-rl">
                кол<br>во<br>чел
            </th>
            <th colspan="2">
                Тариф
            </th>
            <th>
                Текущий<br>баланс
            </th>
            <?php
            if ($_GET['schetchik'] == 1) {
                echo '
                            <th>
                                П
                            </th>
                            <th>
                                пломба
                            </th>';
            }
            ?>
            <th>
                Улица
            </th>
            <th>
                Контроллер
            </th>
            <th>
                Действие
            </th>
        </tr>
        <?php
        foreach ($result as $row) {
            $i++;
            ?>

            <tr>
                <td>
                    <?php echo $i ?>
                </td>
                <td>
                    <?php echo $row['ls'] ?>
                </td>
                <td>
                    <?php echo $row['fio'] ?>
                </td>
                <td>
                    <?php echo $row['domNom'] ?>
                </td>
                <td>
                    <?php echo $row['s_phone'] ?>
                </td>
                <td>
                    <?php echo $row['kol'] ?>
                </td>
                <td>
                    <?php echo $row['b_counter'] ? 'Счетчик' : ($_GET['commercial'] ? 'Коммерч.' : $row['tname']) ?>
                </td>
                <td>
                    <?php echo $row['price'] ?>
                </td>
                <td>
                    <?php echo $row['balance'] ?>
                </td>
                <?php if ($_GET['schetchik'] == 1) { ?>
                    <td>
                        <?= $row['i_counter'] ?>
                    </td>
                    <td>
                        <?= $row['s_plomba'] ?>
                    </td>
                    <?php
                }
                ?>
                <td>
                    <?php echo $row['ulname'] ?>
                </td>
                <td>
                    <?php echo $row['cname'] ?>
                </td>
                <td>
                    <a href="<?= don_url_abonent_edit($row['ls']) ?>" >изменить</a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
<?php } ?>
<script>
    $('#ls').change();
</script>
<a href="..">Назад</a>
