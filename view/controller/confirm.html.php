<?php
if ($_SESSION['loggedIn']) {
    include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/access.inc.php';
} else {
    header('Location: ' . $_SERVER['HTTP_ORIGIN']);
    exit();
}
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.inc.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/inc/functions.inc.php';

$readonly = "readonly";
if (isset($_GET['id'])) {
    $title = "Обновлении данных контроллера";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Контроллер</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            input[readonly]{
                background-color: darkcyan;
                color:white;
            }
            input:not([type='submit']):not([type='radio']):not([type='checkbox']){
                width: 250px;
            }
            .unset{
                background: blue;
                color:white;
            }
        </style>
    </head>
    <body>
        <h1><?php echo $title ?></h1>
        <form action="<?php echo $action ?>" method="get">
            <table>
                <?php
                if (isset($_GET['id'])) {
                    if (is_numeric($_GET['id'])) {
                        $result = sql_in("select fio, phone, controllers.id as id, GROUP_CONCAT(ul.id SEPARATOR ', ') as streets "
                                . 'from controllers '
                                . 'LEFT JOIN ul ON ul.contrId=controllers.id '
                                . 'WHERE controllers.id=' . $_GET['id']);
                        $row = $result->fetch();
                        if ($row) {
                            $_GET['fio'] = $row['fio'];
                            $_GET['id'] = $row['id'];
                            $_GET['phone'] = $row['phone'];
                            $_GET['streets'] = explode(', ', $row['streets']);
                            $sb = 'Обновить';
                            $readonly = "";
                        } else {
                            echo 'Kontroller ne nayden!';
                            exit();
                        }
                    } else {
                        echo 'Nepravilniy id ksontrollera!';
                        exit();
                    }
                }
                ?>
                <tr>
                    <td>ФИО контроллера:</td>
                    <td><input type="text" name="fio" value="<?php echo $_GET['fio'] ?>" <?php echo $readonly ?>>
                        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" <?php echo $readonly ?>></td>
                </tr>
                <tr>
                    <td>Улицы:</td>
                    <td>
                        <?php
                        $sql = "select * from ul";
                        $result = sql_in($sql);
                        foreach ($result as $ul) {
                            $not = 'Не назначено никакой улицы';
                            $checked = "";
                            $style = "";
                            if ($ul['contrId'] == 0) {
                                $style = ' class="unset" title="Не назначено никому" ';
                            }
                            if (isset($_GET['id'])) {
                                if (in_array($ul['id'], $_GET['streets'])) {
                                    $checked = "checked";
                                }
                                echo '<label ' . $style . '><input type="checkbox" value="' . $ul['id'] . '" name="streets[]" ' . $checked . ' readonly>' . $ul['name'] . '</label><br>'; //Вывод списка улиц
                            } else {

                                if (isset($_GET['streets'])) {
                                    if (in_array($ul['id'], $_GET['streets'])) {
                                        echo '<input type="checkbox" value="' . $ul['id'] . '" name="streets[]" checked readonly>' . $ul['name'] . '<br>'; //Вывод списка улиц
                                    }
                                    $not = '';
                                }
                            }
                        }
                        echo $not;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        Тел №:
                    </td>
                    <td>
                        <input type="text" name="phone" value="<?php echo $_GET['phone'] ?>" <?php echo $readonly ?>>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="submit" name="sb" value="<?php echo $sb ?>">
                        <br><br>
                        <input type="submit" onclick="history.back()" value=" Назад ">
                    </td>
                    <td></td>
                </tr>
            </table>
        </form><br>
    </body>
</html>
