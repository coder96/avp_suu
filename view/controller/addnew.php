<?php
$title = __get('title');
$action = __get('form_action');
$contorller = __get('contorller');
if (!is_array($contorller)) {
    $contorller = [];
    $contorller['streets'] = [];
}
$sb = stristr($action, 'edit') ? 'Обновить' : 'Добавить';
$class = stristr($action, 'edit') ? 'primary' : 'success';
?>
<div class="row">
    <div class="col-lg-6">
        <div class="box box-<?=$class?>">
            <div class="box-body">
                <h3><?= $title ?></h3>
                <form action="" method="post">
                    <table>
                        <tr>
                            <td>ФИО мираба:</td>
                            <td>
                                <input type="text" name="fio" required value="<?= $contorller['fio'] ?>">
                                <input type="hidden" name="id" value="<?= $contorller['id'] ?>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Тел №:</td>
                            <td>
                                <input type="text" name="phone" value="<?= $contorller['phone'] ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>За каналы:</td>
                            <td>
                                <?php
                                $sql = "select * from ul";
                                $result = sql_in($sql);
                                foreach ($result as $ul) {
                                    $style = '';
                                    if ($ul['contrId'] == NULL) {
                                        $style = ' class="unset" title="Не назначено никому" ';
                                    }
                                    $checked = in_array($ul['id'], $contorller['streets']) ? 'checked' : '';
                                    echo '<label ' . $style . '><input type="checkbox" name="streets[]" value="' . $ul['id'] . '" ' . $checked . '>' . $ul['name'] . '</label><br>'; //Вывод списка улиц
                                }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><?php Form::button($class)?></td>
                        </tr>
                    </table>
                </form>
                <?php back_url() ?>
            </div>
        </div>
    </div>
</div>
