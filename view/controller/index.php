<div class="row">
    <div class="col-lg-6">
        <div class="box box-primary">
            <div class="box-body">
                <div id="controller" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                    <controller v-bind:controllers="controllers"></controller>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                показано {{controllers.length}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    Vue.component('controller', {
        props: ['controllers'],
        template:
            '<div class="row">' +
            '    <div class="col-sm-12">' +
            '        <table id="example2" class="table table-bordered table-hover dataTable" role="grid">' +
            '            <thead>' +
            '            <tr role="row">' +
            '                <th>л/с</th>' +
            '                <th colspan="2">Ф.И.О</th>' +
            '            </tr>' +
            '            </thead>' +
            '            <tbody>' +
            '            <tr v-for="controller in controllers">' +
            '                <td>{{controller.id}}</td>' +
            '                <td>{{controller.fio}}</td>' +
            '                <td><a  :href="\'<?=don_url_controller_edit('')?>\' + controller.id"><i class="glyphicon glyphicon-pencil"></i></a></td>' +
            '            </tr>' +
            '            </tbody>' +
            '        </table>' +
            '    </div>' +
            '</div>'
    });

    var app = new Vue({
        el: '#controller',
        data: {
            controllers: [],
        },
        created: function () {
        },
        updated: function () {
        },
        methods: {}
    });

    app.controllers = <?=json_encode(Controller::newInstance()->listAll())?>;
</script>