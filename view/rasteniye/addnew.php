<?php
$title = __get('title');
$action = __get('form_action');
/**
 * @var $rasteniye \Skeletion\EkinSkeletion
 */
$rasteniye = __get('rasteniye');
if (!$rasteniye instanceof \Skeletion\EkinSkeletion){
    $rasteniye = new \Skeletion\EkinSkeletion();
}
$sb = stristr($action, 'edit') ? 'Обновить' : 'Добавить';
$class = stristr($action, 'edit') ? 'primary' : 'success';
?>
<div class="row">
    <div class="col-lg-6">
        <div class="box box-<?=$class?>">
            <div class="box-body">
                <form action="" method="post">
                    <table>
                        <tr>
                            <td>Название:</td>
                            <td>
                                <input class="form-control" type="text" name="name" required value="<?=$rasteniye->getName()?>" />
                                <input class="form-control" type="hidden" name="id" value="<?=$rasteniye->getId()?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Литр/сек:</td>
                            <td>
                                <input class="form-control" type="number" name="i_ls" step="0.01" value="<?=$rasteniye->getLs()?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>Цена за год:</td>
                            <td>
                                <input class="form-control" type="number" name="d_sum" step="0.01" value="<?=$rasteniye->getSum()?>" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><button class="btn btn-<?=$class?>"><?=$sb?></button></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <?php back_url()?>
    </div>
</div>

