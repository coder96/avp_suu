<div class="row">
    <div class="col-lg-6">
        <div class="box box-primary">
            <div class="box-body">
                <div id="rasteniye" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"></div>
                    </div>
                    <rasteniya v-bind:rasteniya="rasteniya"></rasteniya>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
                                показано {{rasteniya.length}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    Vue.component('rasteniya', {
        props: ['rasteniya'],
        template:
            '<div class="row">' +
            '    <div class="col-sm-12">' +
            '        <table id="example2" class="table table-bordered table-hover dataTable" role="grid">' +
            '            <thead>' +
            '            <tr role="row">' +
            '                <th>id</th>' +
            '                <th>имя</th>' +
            '                <th >лит/с</th>' +
            '                <th colspan="2">годовая<br/>цена</th>' +
            '            </tr>' +
            '            </thead>' +
            '            <tbody>' +
            '            <tr v-for="r in rasteniya">' +
            '                <td>{{r.id}}</td>' +
            '                <td>{{r.name}}</td>' +
            '                <td>{{r.i_ls}}</td>' +
            '                <td>{{r.d_sum}}</td>' +
            '                <td><a  :href="\'<?=don_url_rasteniye_edit()?>\' + r.id"><i class="glyphicon glyphicon-pencil"></i></a></td>' +
            '            </tr>' +
            '            </tbody>' +
            '        </table>' +
            '    </div>' +
            '</div>'
    });

    var app = new Vue({
        el: '#rasteniye',
        data: {
            rasteniya: [],
        },
        created: function () {
        },
        updated: function () {
        },
        methods: {}
    });

    app.rasteniya = <?=json_encode(\Models\Ekin::newInstance()->listAll()->getAll())?>;
</script>