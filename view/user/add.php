<?php 
$clients = (array)__get('clients');
$action = __get('form_action') != '' ?  __get('form_action') : don_url_user_add_pay();
?>
<h3>Внести оплату</h3>
<form action="<?= $action?>_post" method="post">
    <table>
        <tr>
            <td>ФИО:</td>
            <td>
                <select name="id" id="id" required>
                    <option value="">Выберите клиента</option>
                    <?php
                    foreach ($clients as $c) {
                        echo '<option value="'.$c['pk_i_id'].'">'.$c['s_name'].'</option>';
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Сумма оплаты:</td>
            <td><input type="number" name="summa" id="summa" value="<?php echo $summa; ?>" required step="0.01"></td>
        </tr>
        <tr>
            <td>Текущий баланс:</td>
            <td id="current_balance"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="sb" value="Оплатить"></td>
        </tr>
    </table>
</form>
<br><br>
<a href="..">Назад</a>
<script>
    var clients = <?= json_encode($clients)?>;
    $('#id').change(function(){
        var val = $(this).val();
        $.each(clients,function(i,v){
            if (v.pk_i_id == val){
                $('#current_balance').text(v.d_balance);
            }
        });
    })
</script>
