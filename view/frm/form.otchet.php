<?php
$year = date('Y');

$type = __get('type');
$header = __get('title');
$last_report_date = don_current_report_date();
?>
<div class="box box-primary">
    <div class="box-body">

        <form action="<?php echo $_SERVER['REQUEST_URI'] ?>" method="get">
            <table>
                <?php
                switch ($type) {
                    case 'form-1':
                        ?>
                        <tr>
                            <td>Выберите тип абонентов:</td>
                            <td>
                                <label>
                                    <input class="b_is_commerce" type="radio" name="b_is_commerce" value="0" checked
                                           required>
                                    Физический
                                </label><br>
                                <label>
                                    <input class="b_is_commerce" type="radio" name="b_is_commerce" value="1" required>
                                    Коммерческий
                                </label><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr/>
                            </td>
                        </tr>
                        <tr>
                            <td>Вывод по:</td>
                            <td>
                                <label><input type="radio" name="report_type" value="tariff"
                                              checked/>тарифам</label><br>
                                <label><input type="radio" name="report_type" value="counter"/>счетчикам</label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <hr/>
                            </td>
                        </tr>
                        <tr>
                            <td>Выберите контоллера:</td>
                            <td>
                                <select name="contrId">
                                    <option value="">Все контроллеры</option>
                                    <?php
                                    $result = sql_in('select * from controllers');
                                    foreach ($result as $row) {
                                        echo '<option value="' . $row['id'] . '">' . $row['fio'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Выберите улицу:</td>
                            <td>
                                <select name="ulId">
                                    <option value="">Все улицы</option>
                                    <?php
                                    $result = sql_in('select * from ul');
                                    foreach ($result as $row) {
                                        echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <?php
                        break;
                }
                ?>
                <tr>
                    <td>Выберите дату:</td>
                    <td><input type="month" name="date" required value="<?= $last_report_date ?>"><input type="hidden"
                                                                                                         name="type"
                                                                                                         value="<?php echo __getParam('type') ?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Найти"></td>
                </tr>
            </table>
        </form>
        <a href="/">Назад</a>
        <h3>Последние отчеты : </h3>
        <?php
        $result = __get('last_reports');
        foreach ($result as $row) {
            echo '<a href="' . $row['url'] . '">' . $row['text'] . '</a><br>';
        }
        ?>
    </div>
</div>
