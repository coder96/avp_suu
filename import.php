<?php
/** PHPExcel_IOFactory */
session_start();

require 'app_load.php';

include 'inc/phpExcel/Classes/PHPExcel.php';
$abonAction = new AbonentActions();

$inputFileName = 'a.xls';
echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);


echo '<hr />';

if (!is_array($_SESSION['inserted_street'])){
    $_SESSION['inserted_street'] = [];
}
$ct = $objPHPExcel->getSheetCount();
$names = $objPHPExcel->getSheetNames();
$i = $ok = $err = 0;
while ($i < $ct){
    if (in_array($i,$_SESSION['inserted_street'])){
        cc('continue '.$i);
        $i++;
        continue;
    }
    $_SESSION['inserted_street'][] = $i;
    $ulId = $i + 1;
    try {
        cc('<b>'.$names[$i].'</b>');
        $objPHPExcel->setActiveSheetIndex($i);
        $sheet = $objPHPExcel->getActiveSheet();
        $array = $sheet->toArray('');
        array_shift($array);
        /*
        ?>
        <table border="1" cellspacing="0">
            <tr>
                <th>ФИО</th>
                <th>шалы улуш</th>
                <th>прочие улуш</th>
                <th>шалы аренда</th>
                <th>прочие аренда</th>
                <th>долг за 2018</th>
                <th>оплата</th>
            </tr>
            <?php
        */
            foreach ($array as $item) {
                $ls = don_get_insert_ls();
                if ($ls < 10000) {
                    $ls = 10000;
                }
                $fio = $item[1];
                if (empty($fio)){
                    break;
                }

                $shaaly = (float)$item[3];
                $shaalyA = (float)$item[5];
                $procie = (float)$item[4];
                $procieA = (float)$item[6];
                $ekinId = $ekinNum = $ekinGa = [];
                if ($shaaly){
                    $ekinId[] = 11;
                    $ekinNum[] = 1;
                    $ekinGa[] = $shaaly;
                }
                if ($shaalyA){
                    $ekinId[] = 12;
                    $ekinNum[] = 1;
                    $ekinGa[] = $shaalyA;
                }
                if ($procie){
                    $ekinId[] = 13;
                    $ekinNum[] = 1;
                    $ekinGa[] = $procie;
                }
                if ($procieA){
                    $ekinId[] = 14;
                    $ekinNum[] = 1;
                    $ekinGa[] = $procieA;
                }

                $dolg = (int)$item[7];
                $oplata = (int)$item[8];
                Params::setParam('ul', $ulId);
                Params::setParam('ls', $ls);
                Params::setParam('fio', $fio);
                Params::setParam('balance', $dolg - $oplata);
                Params::setParam('b_counter', 0);

                Params::setParam('ekin_id', $ekinId);
                Params::setParam('ekin_num', $ekinNum);
                Params::setParam('ekin_ga', $ekinGa);

                $abonAction->prepareData();
                $message = $insert = '';
                try {
                    $insert = $abonAction->insert();
                } catch (Exception $ex){
                    $message = $ex->getMessage();
                    cc($message);
                    exit;
                }
                if ($insert){
                    $ok++;
                } else {
                    $err++;
                }
//                cc('res',$ok, $err);
//                exit;
//                echo ("<tr><td>$fio</td><td>$shaaly</td><td>$procie</td><td>$shaalyA</td><td>$procieA</td><td>$dolg</td><td>$oplata</td>");
            }
            /*
            ?>
        </table>
        <?php
        break;
            */
    } catch (Exception $ex) {

    }
//    cc('')
    $i++;
    break;
}
//$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

//cc($sheetData);